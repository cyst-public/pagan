# PAGAN
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Parametrized Attack GenerAtioN 

A `Scenario` and `Terrain` generation engine.

## Parametrization
Parametrization is done via configuration files, that are then specified on the command line. There are three necessary parametrizatoion primitives:

  - Personas: a directory containing configuration files describing the users of the target infrastructure. As an example, see the `.\profiles\` subfolder.
  - Attacker goal: Example `goal.json`.
  - Topology parameters: Example `topo.json`.

The syntax of the config files is defined via `json schemas` in the folder `.\schemas\`.


## Usage

  1. Ensure that the database is up and running with its API (see `pagan-rs`).
  2. Create the poetry environment (if not yet up) with `poetry install`
  3. Run `poetry run python pagan\main.py`

  4. Wait for completion. the results will be written to the `.\output\` folder.
 
