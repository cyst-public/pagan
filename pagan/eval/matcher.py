from typing import Self, Callable, Dict, Tuple, Set
from abc import ABC, abstractmethod

import networkx

NodeMatchSet = Set[Tuple[int, int]]
EdgeMatchSet = Set[Tuple[Tuple[int, int, int], Tuple[int, int, int]]]


class ScenarioMatcher(ABC):
    ROOT = 0

    def __init__(self, generated: networkx.MultiDiGraph, manual: networkx.MultiDiGraph) -> Self:
        self._generated = generated
        self._manual = manual
        self._edge_match_f = lambda _e1, _e2: True
        self._node_match_f = lambda _n1, _n2: True
        self._stop_condition_f = lambda _n: False
        self._edge_match_store: EdgeMatchSet = set()
        self._node_match_store: NodeMatchSet = set()

    def register_edge_match(self, edge_match: Callable[[Dict, Dict], bool]) -> Self:
        self._edge_match_f = edge_match
        return self

    def register_node_match(self, node_match: Callable[[Dict, Dict], bool]) -> Self:
        self._node_match_f = node_match
        return self

    def register_stop_condition(self, stop_condition: Callable[[Dict], bool]) -> Self:
        self._stop_condition_f = stop_condition
        return self

    @abstractmethod
    def match(self) -> bool:
        pass

    @property
    def matchings(self) -> Tuple[NodeMatchSet, EdgeMatchSet]:
        return self._node_match_store, self._edge_match_store


class PathMatcher(ScenarioMatcher):
    def match(self) -> bool:
        return self._match(self.ROOT, self.ROOT)

    def _match(self, generated_node_handle: int, manual_node_handle: int) -> bool:
        generated_node_data = self._generated.nodes[generated_node_handle]
        manual_node_data = self._manual.nodes[manual_node_handle]
        if not self._node_match_f(generated_node_data, manual_node_data):
            return False
        self._node_match_store.add((generated_node_handle, manual_node_handle))
        if self._stop_condition_f(generated_node_data) or self._stop_condition_f(manual_node_data):
            return True
        res = False
        # for u2, v2, k2, data2 in self._manual.edges(nbunch=manual_node_handle, data=True):
        for edge in self._manual.edges(nbunch=manual_node_handle, data=True):
            for u1, v1, k1, data1 in self._generated.edges(nbunch=generated_node_handle, data=True):
                if self._edge_match_f(data1, data2):
                    self._edge_match_store.add(((u1, v1, k1), (u2, v2, k2)))
                    res = res or self._match(v1, v2)
        return res
