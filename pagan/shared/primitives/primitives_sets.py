from dataclasses import dataclass
from typing import Type, FrozenSet, Self
from adt import Case
from chainingiterator import Chi

from pagan.shared.primitives.types import (
    AccountHandle,
    ActionHandle,
    DeviceHandle,
    EnablerHandle,
    ServiceHandle,
)
from pagan.shared.primitives.set_option import SetOption


@dataclass
class ServiceAndVulnerability:
    service: ServiceHandle
    enabler: EnablerHandle

    def __hash__(self):
        return hash((self.service, self.enabler))


class ServiceSetOption(SetOption[ServiceHandle]):
    NONE: Case
    ALL: Case
    SOME: Case[FrozenSet[ServiceHandle]]

    def inner_type(self) -> Type:
        return ServiceHandle

    def get_service_set(self) -> Self:
        return self


class ServiceVulnerabilitySetOption(SetOption[ServiceAndVulnerability]):
    NONE: Case
    ALL: Case
    SOME: Case[FrozenSet[ServiceAndVulnerability]]

    def inner_type(self) -> Type:
        return ServiceAndVulnerability

    def get_service_set(self) -> ServiceSetOption:
        return self.match(
            all=lambda: ServiceSetOption.ALL(),
            none=lambda: ServiceSetOption.NONE(),
            some=lambda inner: ServiceSetOption.SOME(Chi(inner).map(lambda e: e.service).collect(frozenset)),
        )


class ActionSetOption(SetOption[ActionHandle]):
    NONE: Case
    ALL: Case
    SOME: Case[FrozenSet[ActionHandle]]

    def inner_type(self) -> Type:
        return ActionHandle


class DeviceSetOption(SetOption[DeviceHandle]):
    NONE: Case
    ALL: Case
    SOME: Case[FrozenSet[DeviceHandle]]

    def inner_type(self) -> Type:
        return DeviceHandle


class AccountSetOption(SetOption[AccountHandle]):
    NONE: Case
    ALL: Case
    SOME: Case[FrozenSet[AccountHandle]]

    def inner_type(self) -> Type:
        return AccountHandle


class EnablerSetOption(SetOption[AccountHandle]):
    NONE: Case
    ALL: Case
    SOME: Case[FrozenSet[EnablerHandle]]

    def inner_type(self) -> Type:
        return EnablerHandle
