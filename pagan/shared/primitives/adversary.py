from dataclasses import dataclass


@dataclass
class Adversary:
    allowed_online_guess_complexity: int
    allowed_cracking_complexity: int
