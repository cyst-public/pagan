from typing import Dict, Optional, Set, Iterator, List

from pagan_rs.pagan_rs import PrivilegeLevel
from pydantic.dataclasses import dataclass as PydanticDataclass
from pydantic import ConfigDict
from dataclasses import field
from uuid import uuid4, UUID
from serde import serialize, field

from pagan.shared.primitives.capabilities import Capability
from pagan.shared.primitives.credentials import Credential
from pagan.shared.primitives.types import DeviceHandle, AccountHandle, PersonHandle, CapabilityType


def serialize_caps(obj: Dict[DeviceHandle, Set[Capability]]) -> Dict[DeviceHandle, List[Capability]]:
    return dict(map(lambda item: (item[0], list(item[1])), obj.items()))


@serialize
@PydanticDataclass(frozen=True, config=ConfigDict(arbitrary_types_allowed=True))
class InfraUserAccount:
    owned_by: Optional[PersonHandle]
    nickname: str
    credential: Optional[Credential]
    capabilities: Dict[DeviceHandle, Set[Capability]] = field(serializer=serialize_caps)
    sid: UUID = field(init=False, default_factory=uuid4)
    domain_account: bool = field(default=False)

    def finalize_credential(self, handle: AccountHandle):
        if self.credential is not None:
            self.credential.account = handle

    def get_related_accounts(self, people: "PersonStore") -> Iterator[AccountHandle]:
        if self.owned_by is None:
            return iter([])
        yield from people.get(self.owned_by).get_accounts()

    def get_privilege_level(self, device: DeviceHandle) -> PrivilegeLevel:
        if (caps := self.capabilities[device]) is not None:
            for cap in caps:
                if cap.to == CapabilityType.EXECUTE_CODE:
                    return cap.privilege_limit

        return PrivilegeLevel.NONE
