from pagan_rs.pagan_rs import Service


# from dataclasses import dataclass, field
# from enum import Enum, auto, IntFlag
# from typing import FrozenSet, Generator, Self, Set, List, Union
# from semver import VersionInfo

# # from sin_backend.sin_backend import FullServiceViewForPagan, ActionApplicability

# from pagan.shared.exceptions import ProgramLogicError
# from pagan.shared.primitives.types import ActionGuard, ActionHandle, PortNum, PortRange
# from pagan.shared.primitives.capabilities import CapabilityType, PrivilegeLevel


# class ServiceTag(Enum):
#     OS_COMPONENT = auto()
#     BROWSER = auto()
#     WEB_SERVICE = auto()
#     DOMAIN_MANAGEMENT = auto()
#     PASSWORD_MANAGER = auto()
#     LOCAL_SERVICE = auto()
#     # TODO: more


# ExecImpact = FrozenSet[CapabilityType]


# @dataclass(frozen=True)
# class ExecutableDescription:
#     dbref: str
#     name: str
#     version: VersionInfo
#     accessible_to: PrivilegeLevel
#     tags: List[ServiceTag] = field(default_factory=list)
#     autoelevate: bool = field(default=False)  # suid/uac
#     impact: ExecImpact = field(
#         default_factory=frozenset
#     )  # set is good, as it orders the elements, so when we type it to tuple, we can do eq matching

#     def get_effective_privilege(self) -> PrivilegeLevel:
#         if self.accessible_to == PrivilegeLevel.USER and self.autoelevate:
#             return PrivilegeLevel.ADMINISTRATOR
#         if self.accessible_to == PrivilegeLevel.ADMINISTRATOR and self.autoelevate:
#             return PrivilegeLevel.LOCAL_SYSTEM
#         return self.accessible_to

#     def compute_process_integrity_level(
#         self, user_code_exec_priv: PrivilegeLevel
#     ) -> PrivilegeLevel:
#         if self.autoelevate:
#             return self.get_effective_privilege()
#         return min(user_code_exec_priv, self.get_effective_privilege())


# class ProcessIntegrityLevel(IntFlag):
#     LOW = PrivilegeLevel.SERVICE
#     MEDIUM = PrivilegeLevel.USER
#     HIGH = PrivilegeLevel.ADMINISTRATOR
#     SYSTEM = PrivilegeLevel.LOCAL_SYSTEM
#     PROTECTED = 100  # for future use


# @dataclass(frozen=True)
# class ProcessInfo:
#     executable: ExecutableDescription
#     integrity: ProcessIntegrityLevel
#     ports_held: List[PortNum | PortRange]

#     def __post_init__(self):
#         if self.integrity < self.executable.accessible_to:
#             raise ProgramLogicError(
#                 "Process integrity cannot be lower than execute access of the underlying executable."
#                 " Such a state requires an Enabler to describe it."
#             )


# @dataclass
# class Service:
#     inner: ExecutableDescription | ProcessInfo
#     applicable_actions: Set[ActionHandle] = field(default_factory=set)

#     @staticmethod
#     def is_process(specimen: Self) -> bool:
#         return isinstance(specimen.inner, ProcessInfo)

#     @staticmethod
#     def is_executable(specimen: Self) -> bool:
#         return isinstance(specimen.inner, ExecutableDescription)

#     def get_effective_privilege(self) -> Union[PrivilegeLevel, ProcessIntegrityLevel]:
#         return (
#             self.inner.get_effective_privilege()
#             if isinstance(self.inner, ExecutableDescription)
#             else self.inner.integrity
#         )

#     def get_access_privilege(self) -> Union[PrivilegeLevel, ProcessIntegrityLevel]:
#         return (
#             self.inner.accessible_to
#             if isinstance(self.inner, ExecutableDescription)
#             else self.inner.integrity
#         )

#     def get_applicable_actions(self) -> Set[ActionHandle]:
#         return self.applicable_actions

#     def get_impacts(self) -> ExecImpact:
#         return (
#             self.inner.impact
#             if isinstance(self.inner, ExecutableDescription)
#             else self.inner.executable.impact
#         )

#     # @classmethod
#     # def from_model(
#     #     cls, model: FullServiceViewForPagan, uses: List[ActionApplicability], mock: bool = False
#     # ) -> Generator[Self, None, None]:
#     #     if mock:
#     #         from pagan.mock.services_mock_data import MOCK_DATA

#     #         datasource = MOCK_DATA
#     #     else:
#     #         raise NotImplementedError("additional data collection for services is still missing")
#     #         # TODO connect actual KB
#     #     yield cls()
#     #     yield cls()
