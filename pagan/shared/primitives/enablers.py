from dataclasses import dataclass
from enum import IntFlag, Enum, auto, StrEnum
from typing import Set, Union

from pagan_rs.pagan_rs import EnablerType
from serde import serialize, field

from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.primitives_sets import ActionSetOption
from pagan.shared.primitives.predicate_trait import Predicate
from pagan.shared.primitives.primitives_sets import ServiceSetOption
from pagan.shared.primitives.set_option import SetOption
from pagan.shared.primitives.types import ActionHandle, EnablerImpact


@serialize
class EnablerLocality(IntFlag):
    LOCAL = 1 << 0
    REMOTE = 1 << 1
    BOTH = (1 << 0) | (1 << 1)


# @serialize
# class EnablerType(StrEnum):
#     ANY = "any"
#     MISCONFIGURATION = "misconfiguration"
#     VULNERABILITY = "vulnerability"
#     NEGLIGENCE = "negligence"
#     MISC = "misc"


@serialize
@dataclass(frozen=True)
class ProtoEnabler:
    locality: EnablerLocality
    impact: Union[Predicate, Set[EnablerImpact]]

    def __post_init__(self):
        if not isinstance(self.impact, Predicate):
            raise ProgramLogicError("Impact in ProtoEnabler must be a predicate.")


def serialize_enablertype(obj: EnablerType) -> str:
    if obj == EnablerType.Cve:
        return "CVE"
    elif obj == EnablerType.Misconfiguration:
        return "Misconfiguration"
    elif obj == EnablerType.Cwe:
        return "Cwe"
    elif obj == EnablerType.UserError:
        return "UserError"


@serialize(serializer=SetOption.serialize_setOption_includers)
@dataclass(frozen=True)
class Enabler(ProtoEnabler):
    description: str  # TODO: expand
    services: ServiceSetOption
    actions: ActionSetOption
    type: EnablerType = field(serializer=serialize_enablertype)

    def __post_init__(self):
        if not isinstance(self.impact, set):
            raise ProgramLogicError("Enabler must enumerate its impact categories.")

    def matches_proto(self, other: ProtoEnabler) -> bool:
        if type(other) == type(self):
            raise ProgramLogicError("Enabler::matches_proto argument must be a pure ProtoEnabler.")
        if not self.locality & other.locality:
            return False
        return other.impact.eval(enablers=self.impact)  # types are checked in `__post_init__`

    def usable_for(self, action: ActionHandle, template: ProtoEnabler) -> bool:
        return action in self.actions.as_iterator_notall() and self.matches_proto(template)
