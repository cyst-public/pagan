from abc import ABC
from enum import Enum
from typing import Iterator, Type, Iterable, Union

from serde import serialize

from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.capabilities import CapabilityTemplate
from pagan.shared.primitives.enablers import EnablerImpact
from pagan.shared.primitives.predicate_trait import Predicate
from pagan.shared.primitives.tokens import TokenType, Token
from pagan.shared.utils import NonZeroArgsMethod, VarArgTypesUniformlyOneOf
from chainingiterator import Chi


@serialize
class PredicateImpl(Predicate, ABC):
    @NonZeroArgsMethod
    @VarArgTypesUniformlyOneOf(TokenType, CapabilityTemplate, Predicate, EnablerImpact)
    def __init__(self, typ: Type, *args):
        self._elems = list(args)
        self._type: Type = typ

    def __iter__(self) -> Iterator:
        if self._type != Predicate:
            yield from self._elems
        else:
            for pred in self._elems:
                yield from pred

    def __contains__(self, elem: Union[Token, CapabilityTemplate, EnablerImpact]) -> bool:
        if self._type == Predicate:
            return Chi(self._elems).any(lambda pred: elem in pred)

        if not isinstance(elem, self._type):
            return False
        return elem in self._elems


class Empty(Predicate):
    def __init__(self):
        return

    def eval(
        self,
        tokens: Iterable[Token] = (),
        caps: Iterable[CapabilityTemplate] = (),
        enablers: Iterable[EnablerImpact] = (),
    ) -> bool:
        return True

    def __contains__(self, elem: Union[Token, CapabilityTemplate, EnablerImpact]) -> bool:
        return False

    def __iter__(self):
        return iter(list())


@serialize
class Any(PredicateImpl):
    def eval(
        self,
        tokens: Iterable[Token] = (),
        caps: Iterable[CapabilityTemplate] = (),
        enablers: Iterable[EnablerImpact] = (),
    ) -> bool:
        if self._type == TokenType:
            for token in self._elems:
                if token in map(lambda t: t.type, tokens):
                    return True
            return False
        elif self._type == CapabilityTemplate:
            for cap in self._elems:
                for acap in caps:
                    if cap.matching(acap):
                        return True
            return False
        elif self._type == EnablerImpact:
            for enabler in self._elems:
                if enabler in enablers:
                    return True
            return False
        elif self._type == Predicate:
            for elem in self._elems:
                if elem.eval(tokens, caps):
                    return True
            return False
        raise ProgramLogicError("Predicate with wrong type of elements")


@serialize
class All(PredicateImpl):
    def eval(
        self,
        tokens: Iterable[Token] = (),
        caps: Iterable[CapabilityTemplate] = (),
        enablers: Iterable[EnablerImpact] = (),
    ):
        if self._type == TokenType:
            for token in self._elems:
                if token not in map(lambda t: t.type, tokens):
                    return False
            return True
        elif self._type == CapabilityTemplate:
            for cap in self._elems:
                present = False
                for acap in caps:
                    if cap.matching(acap):
                        present = True
                if not present:
                    return False
            return True
        elif self._type == EnablerImpact:
            for enabler in self._elems:
                if enabler not in enablers:
                    return False
            return True
        elif self._type == Predicate:
            for elem in self._elems:
                if not elem.eval(tokens, caps):
                    return False
            return True
        raise ProgramLogicError("Predicate with wrong type of elements")


@serialize
class Not(PredicateImpl):
    def __init__(self, *args):
        if len(args) != 1:
            raise ProgramLogicError("Not predicate can not have multiple values inside.")
        super().__init__(*args)

    def eval(
        self,
        tokens: Iterable[Token] = (),
        caps: Iterable[CapabilityTemplate] = (),
        enablers: Iterable[EnablerImpact] = (),
    ) -> bool:
        if self._type == Predicate:
            return not self._elems[0].eval(tokens, caps, enablers)
        elif self._type == TokenType:
            return self._elems[0] not in map(lambda t: t.type, tokens)
        elif self._type == CapabilityTemplate:
            return self._elems[0] not in caps
        elif self._type == EnablerImpact:
            return self._elems[0] not in enablers
        else:
            raise ProgramLogicError("Predicate with wrong type of elements")


@serialize
class NoneOf(PredicateImpl):
    def __init__(self, *args):
        self._alias = Not(Any(*args))

    def eval(
        self,
        tokens: Iterable[Token] = (),
        caps: Iterable[CapabilityTemplate] = (),
        enablers: Iterable[EnablerImpact] = (),
    ) -> bool:
        return self._alias.eval(tokens, caps, enablers)


@serialize
class NotJust(PredicateImpl):
    @NonZeroArgsMethod
    @VarArgTypesUniformlyOneOf(TokenType, CapabilityTemplate, Predicate, EnablerImpact)
    def __init__(self, typ: Type, *args):
        if not isinstance(args[0], Enum):
            raise ProgramLogicError("NotJust must have elements of type inheriting from Enum")
        targets = []
        for option in typ:  # pray to thee lord, that this works as intended
            if option not in args:
                targets.append(option)
        self._alias = Any(*targets)

    def eval(
        self,
        tokens: Iterable[Token] = (),
        caps: Iterable[CapabilityTemplate] = (),
        enablers: Iterable[EnablerImpact] = (),
    ) -> bool:
        return self._alias.eval(tokens, caps, enablers)
