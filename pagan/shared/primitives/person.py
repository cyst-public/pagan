from pydantic.dataclasses import dataclass as PydanticDataclass
from pydantic import Field
from typing import Iterator, List, Set
from enum import StrEnum, auto
from pagan.shared.primitives.types import AccountHandle
from pagan.shared.primitives.credentials import Credential


class ITProficiency(StrEnum):
    BEGINNER = auto()
    COMPETENT = auto()
    EXPERT = auto()


class ITRole(StrEnum):
    BASIC_USER = auto()
    VIP_USER = auto()
    ADVANCED_USER = auto()
    RESTRICTED_SERVER_ADMIN = auto()
    RESTRICTED_STATION_ADMIN = auto()
    SERVER_ADMIN = auto()
    STATION_ADMIN = auto()
    DOMAIN_ADMIN = auto()
    ICS_ADMIN = auto()  # accounts on ICS master
    DEVELOPER = auto()  # standard user + user on some servers (web, db, services)


@PydanticDataclass
class Persona:
    proficiency: ITProficiency
    personal_traits: List[str]
    likes: List[str]
    dislikes: List[str]


@PydanticDataclass
class Person:
    name: str
    age: int
    role: str
    persona: Persona
    it_roles: List[ITRole] = Field(default_factory=lambda: [ITRole.BASIC_USER])
    store_handle: None | int = Field(exclude=True, init_var=False, default=None)
    controlled_accounts_handles: Set[AccountHandle] = Field(default_factory=set, exclude=True)
    controlled_accounts: List = Field(default_factory=list, exclude=True)
    credentials: List[Credential] = Field(default_factory=list, exclude=True)

    def add_account(self, account: "InfraUserAccount", handle: AccountHandle) -> None:
        self.controlled_accounts_handles.add(handle)
        self.controlled_accounts.append(account)

    def add_credential(self, credential: Credential):
        self.credentials.append(credential)

    def get_credentials(self) -> Iterator[Credential]:
        yield from self.credentials
        for acc in self.controlled_accounts:
            yield acc.credential

    def get_accounts(self) -> Iterator[AccountHandle]:
        yield from self.controlled_accounts_handles
