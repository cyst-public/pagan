from pydantic.dataclasses import dataclass
from pydantic import Field
from enum import IntEnum, auto, StrEnum
from typing import Any, Optional, Self

from serde import serialize

from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.types import (
    CapabilityType,
    DeviceHandle,
    AccountHandle,
    EnumProvidingSchemeLegend,
)


class LimitType(StrEnum):
    MINIMUM = auto()
    MAXIMUM = auto()
    EXACT = auto()


class PrivilegeLevel(IntEnum, EnumProvidingSchemeLegend):
    NONE = 0
    SERVICE = 1
    USER = 2
    ADMINISTRATOR = 3
    LOCAL_SYSTEM = 4


@serialize
@dataclass(frozen=True, unsafe_hash=True)
class CapabilityTemplate:
    to: CapabilityType
    privilege_limit: PrivilegeLevel
    limit_type: LimitType
    on_device: Optional[DeviceHandle] = Field(default=None)

    def matching(self, other: Self) -> bool:
        if self.to != other.to:
            return False
        if self.on_device is not None and other.on_device is not None and self.on_device != other.on_device:
            return False
        if self.limit_type == LimitType.EXACT:
            return self.privilege_limit == other.privilege_limit
        if self.limit_type == LimitType.MINIMUM:
            return self.privilege_limit <= other.privilege_limit
        return self.privilege_limit >= other.privilege_limit


@serialize
@dataclass(frozen=True, unsafe_hash=True)
class Capability(CapabilityTemplate):
    limit_type: LimitType = Field(init_var=False, default=LimitType.EXACT, exclude=True)

    def matching(self, other: "CapabilityTemplate") -> bool:
        raise ProgramLogicError("matching() method should only be called on a pure template.")


@serialize
@dataclass(frozen=True)
class AttackerCapability(Capability):
    as_user: Optional[AccountHandle] = Field(default=None)

    def __post_init__(self):
        if self.as_user is None:
            raise ProgramLogicError("AttackerCapability without connection to a user.")

    def __hash__(self) -> int:
        return hash(
            (self.to, self.privilege_limit, self.on_device)
        )  # limit is always EXACT, user distinction not part of MVP

    def __eq__(self, other: Any) -> bool:
        if not isinstance(other, self.__class__):
            raise ProgramLogicError(f"Comparison `eq` not available for AttackerCapability and {type(other)}")
        return (
            self.to == other.to and self.privilege_limit == other.privilege_limit and self.on_device == other.on_device
        )  # limit is always EXACT, user distinction not part of MVP
