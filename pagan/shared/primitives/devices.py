import random
import serde

from typing import Iterable, Iterator, List, Self, Set
from enum import Enum, auto, StrEnum
from pydantic.dataclasses import dataclass as PydanticDataclass
from chainingiterator import Chi
from serde import serialize, field


from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.accounts import InfraUserAccount
from pagan.shared.primitives.credentials import Credential, CredentialType
from pagan.shared.primitives.primitives_sets import (
    ServiceSetOption,
    EnablerSetOption,
    ServiceVulnerabilitySetOption,
    AccountSetOption,
)
from pagan.shared.primitives.set_option import SetOption
from pagan.shared.primitives.tokens import TokenType
from pagan.shared.primitives.types import AccountHandle, OrdinalId, PortNum, PortRange


class OperatingSystem(StrEnum):
    """
    To help automatically assign the right default services and OS components
    """

    WINDOWS_SERVER = auto()
    WINDOWS_DESKTOP = auto()
    NIX_SERVER = auto()
    NIX_DESKTOP = auto()
    SPECIAL_NETOWRK = auto()  # e.g. network appliance OS (Cisco OS, ...)
    SPECIAL_EMBEDDED = auto()
    MOCK = auto()

    @classmethod
    def random_server(cls) -> Self:
        return cls.WINDOWS_SERVER if random.getrandbits(1) else cls.NIX_SERVER

    @classmethod
    def random_desktop(cls) -> Self:
        return cls.WINDOWS_DESKTOP if random.getrandbits(1) else cls.NIX_DESKTOP

    def is_microsoft(self):
        return self == OperatingSystem.WINDOWS_SERVER or OperatingSystem.WINDOWS_DESKTOP

    def is_nix(self):
        return not self.is_microsoft()


class DeviceRole(StrEnum):
    """
    To help assign additional software common for the role.
    """

    WORKSTATION = auto()
    MAIL_SERVER = auto()
    WEB_SERVER = auto()
    DOMAIN_CONTROLLER = auto()
    NETWORK_SHARE = auto()
    BACKUP_SERVER = auto()
    DATABASE = auto()
    OPERATIONAL_TECHNOLOGY = auto()
    NETWORK_APPLIANCE = auto()
    REMOTE_ACCESS_GATEWAY = auto()
    APP_HOSTING = auto()


class SegmentType(StrEnum):
    INTERNAL = auto()
    DMZ = auto()
    ATTACKER = auto()
    PARTNER = auto()
    INTERNET = auto()
    ICS = auto()


@serialize(serializer=SetOption.serialize_setOption_includers)
@PydanticDataclass
class Device:
    name: str
    vlan: str
    vlan_num: int
    roles: List[DeviceRole]
    os: OperatingSystem
    segment_info: SegmentType
    system_account: AccountHandle | None = field(default=None)
    service_account: AccountHandle | None = field(default=None)
    services: ServiceSetOption = field(default=ServiceSetOption.NONE())
    prohibited_services: ServiceSetOption = field(
        default=ServiceSetOption.NONE(), skip=True
    )  # TODO: decide based on OS, role
    compulsory_services: ServiceSetOption = field(default=ServiceSetOption.NONE())  # TODO: decide based on OS, role
    open_ports: Set[PortNum | PortRange] = serde.field(default_factory=set, serializer=lambda s: list(s))
    local_accounts: Set[AccountHandle] = serde.field(default_factory=set, serializer=lambda s: list(s))
    domain_accounts: Set[AccountHandle] = serde.field(default_factory=set, serializer=lambda s: list(s))
    cached_credentials: Set[Credential] = serde.field(
        default_factory=set, serializer=lambda s: list(s)
    )  # it would be hella complicated to keep this in the lsass service
    virtualized: bool = field(default=False)
    usb_access: bool = field(default=True)
    store_ref: int | None = field(init=False, default=None)
    enablers: ServiceVulnerabilitySetOption = field(default=ServiceVulnerabilitySetOption.NONE())

    def get_segment_token_type(self) -> TokenType:
        if self.segment_info == SegmentType.INTERNAL:
            return TokenType.ACCESS_INTERNAL_NETWORK
        if self.segment_info == SegmentType.DMZ:
            return TokenType.ACCESS_DMZ_SEGMENT
        return TokenType.NONE

    def get_random_account(self) -> AccountHandle:
        return random.choice(
            Chi(self.local_accounts).chain(iter(self.domain_accounts)).collect(list)
        )  # TODO: implement a clever random choice in chainingiterator

    def get_random_user_account(self) -> AccountHandle:
        return random.choice(
            Chi(self.local_accounts)
            .chain(iter(self.domain_accounts))
            .filter(lambda acc_id: acc_id != self.system_account and acc_id != self.service_account)
            .collect(list)
        )

    def get_random_admin_account(self) -> AccountHandle:
        raise NotImplementedError

    def get_local_os_credentials(self, account_store: "AccountStore") -> Iterable[Credential]:
        for account in self.local_accounts:
            for credential in account_store.get(account).credentials:
                if credential.device == self.store_ref and credential.type == CredentialType.OS:
                    yield credential

    def get_user_accounts(self) -> AccountSetOption:
        accs = (
            Chi(self.local_accounts)
            .chain(iter(self.domain_accounts))
            .filter(lambda acc_id: acc_id != self.system_account and acc_id != self.service_account)
            .collect(set)
        )
        if accs:
            return AccountSetOption.SOME(accs)
        return AccountSetOption.NONE

    def get_active_os_account_credential(
        self, account_store: "AccountStore", account_handle: AccountHandle
    ) -> Credential | None:
        try:
            return (
                Chi(account_store.get(account_handle).credentials)
                .filter(lambda cred: cred.device == self.store_ref and cred.type == CredentialType.OS)
                .nth(0)
                .unseal()
            )
        except IndexError:
            return None

    def get_associated_accounts(self, store: "AccountStore") -> Iterator[AccountHandle]:
        for account in self.local_accounts:
            yield from store.get(account).get_related_accounts()
        for account in self.domain_accounts:
            yield from store.get(account).get_related_accounts()

    def set_store_ref(self, ref: OrdinalId) -> None:
        self.store_ref = ref

    def add_system_account(self, handle: AccountHandle):
        if self.system_account is None:
            self.system_account = handle
            self.local_accounts.add(handle)
        else:
            raise ProgramLogicError("can not set system account when one is already set")

    def add_services_account(self, handle: AccountHandle):
        if self.service_account is None:
            self.service_account = handle
            self.local_accounts.add(handle)
        else:
            raise ProgramLogicError("can not set service account when one is already set")

    def add_account(self, handle: AccountHandle, account: InfraUserAccount):
        if account.domain_account:
            self.domain_accounts.add(handle)
        else:
            self.local_accounts.add(handle)
