import dataclasses
from enum import IntFlag, auto
from typing import Dict, List, NewType, NamedTuple, Optional

from serde import serialize

from pagan.shared.primitives.primitives_sets import ServiceSetOption
from pagan.shared.primitives.set_option import SetOption
from pagan.shared.primitives.types import DeviceHandle, AccessType

CombinedToken = NewType("CombinedToken", int)  # to enforce proper use by type safety via mypy


class TokenLocationSpecifier(IntFlag):
    UNIVERSAL = auto()
    DEVICE_BOUND = auto()
    SERVICE_BOUND = auto()
    MARGINAL = auto()


class TokenType(IntFlag):
    NONE = 1 << 0

    # @Auth
    CREDENTIAL = 1 << 1
    CREDENTIAL_2FA_TOKEN = 1 << 2

    # @Privileges
    CAPABILITY = 1 << 3
    # just to represent that some capability is required, if in action output -> inherit some caps from some user
    CAPABILITY_ALL = 1 << 3 | 1 << 4
    # only for the `granted` branch, means we inherit all user caps,
    # the value is engineered so that it matches CAPABILITY when checking if granted

    # @Impact helpers
    DATA = 1 << 5  # meaning data collected dynamically
    IMPACT_DENIAL_OF_SERVICE = 1 << 6

    # @Access between devices
    ACCESS_VIA_NETWORK = 1 << 9
    ACCESS_VIA_SHARED_FILESYS = 1 << 10
    ACCESS_VIA_REMOVABLE_MEDIA = 1 << 11

    # @Session to a device
    SESSION = 1 << 12  # possible 2 way channel between attacker and target system, any action giving a session
    # - basically the same as ACE capability, except this can be "taken" by network defenses

    # @External Artifacts
    PRE_ATTACK_BACKDOOR = 1 << 13  # For supply-chain compromise and hardware additions.
    EXTERNAL_PHISHING_MAIL = 1 << 14  # if given, phishing is an option in the scenario

    # @Internal Artifacts -- created by the attacker when already inside the system
    TAINTED_SHARED_CONTENT = 1 << 15
    INFECTED_REMOVABLE_MEDIA = 1 << 16
    INTERNAL_PHISHING_MAIL = 1 << 17
    MALICIOUS_DOMAIN_CONFIGURATION_OBJECT = 1 << 18

    # @Other
    PHYSICAL_ACCESS = 1 << 19  # The attacker is able to smuggle stuff into the infrastructures physical location

    # @Negatives -- tokens mainly used for the usagescheme's except branch
    ACCESS_INTERNAL_NETWORK = 1 << 20  # aka being in the intra already, so we wont use pure initial access actions
    ACCESS_DMZ_SEGMENT = 1 << 21
    CONTAINERIZED = 1 << 22  # in VM, container this is the default, but can be set for other devices
    NO_USB_ACCESS = 1 << 23  # device has no usb access (containers for example)
    ONGOING = 1 << 24  # given by the init action it prohibits its further use

    # @Scenario control
    NO_MFA_ALLOWED = 1 << 25

    # TODO: extend with more

    # @util methods
    @staticmethod
    def combine(*tokens: "TokenType") -> CombinedToken:
        accumulator = 0
        for elem in tokens:
            accumulator |= elem
        return CombinedToken(accumulator)

    @staticmethod
    def decompose(token: CombinedToken) -> List["TokenType"]:
        # TODO: this misses PRIVILEGE_ALL -> might not be a problem tho
        test_value = 1
        result = list()
        while test_value <= token:
            if test_value & token == test_value:
                result.append(TokenType(test_value))
            test_value <<= 1
        return result

    def present_in(self, combined: CombinedToken) -> bool:
        return self & combined == self


_token_location: Dict[TokenType, TokenLocationSpecifier] = {
    TokenType.NONE: TokenLocationSpecifier.UNIVERSAL,
    TokenType.CREDENTIAL: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.CREDENTIAL_2FA_TOKEN: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.CAPABILITY: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.CAPABILITY_ALL: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.DATA: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.IMPACT_DENIAL_OF_SERVICE: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.ACCESS_VIA_NETWORK: TokenLocationSpecifier.MARGINAL,  # we are giving this based on network rules
    TokenType.ACCESS_VIA_SHARED_FILESYS: TokenLocationSpecifier.MARGINAL,  # we are giving this based on network rules
    TokenType.ACCESS_VIA_REMOVABLE_MEDIA: TokenLocationSpecifier.MARGINAL,  # we are giving this based on network rules
    TokenType.SESSION: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.PRE_ATTACK_BACKDOOR: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.EXTERNAL_PHISHING_MAIL: TokenLocationSpecifier.UNIVERSAL,
    TokenType.TAINTED_SHARED_CONTENT: TokenLocationSpecifier.UNIVERSAL,
    TokenType.INFECTED_REMOVABLE_MEDIA: TokenLocationSpecifier.UNIVERSAL,
    TokenType.INTERNAL_PHISHING_MAIL: TokenLocationSpecifier.UNIVERSAL,
    TokenType.MALICIOUS_DOMAIN_CONFIGURATION_OBJECT: TokenLocationSpecifier.UNIVERSAL,
    TokenType.PHYSICAL_ACCESS: TokenLocationSpecifier.UNIVERSAL,
    TokenType.ACCESS_INTERNAL_NETWORK: TokenLocationSpecifier.UNIVERSAL,
    TokenType.ACCESS_DMZ_SEGMENT: TokenLocationSpecifier.UNIVERSAL,
    TokenType.CONTAINERIZED: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.NO_USB_ACCESS: TokenLocationSpecifier.DEVICE_BOUND,
    TokenType.ONGOING: TokenLocationSpecifier.UNIVERSAL,
}


@serialize(serializer=SetOption.serialize_setOption_includers)
@dataclasses.dataclass
class Token:
    type: TokenType
    device: Optional[DeviceHandle]  # device ID
    services: ServiceSetOption = ServiceSetOption.ALL()  # different only for ACCESS type tokens

    @staticmethod
    def get_target_location_specifier(typ: TokenType) -> TokenLocationSpecifier:
        return _token_location[typ]

    def __hash__(self):
        return hash((self.type, self.device, self.services))
