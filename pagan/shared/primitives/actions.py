# from pydantic.dataclasses import dataclass as PydanticDataclass # -- for the future
from dataclasses import dataclass
from enum import Enum, auto
from typing import Optional, List, Iterable, Tuple
from adt import adt, Case

from serde import serialize, field

from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.capabilities import CapabilityTemplate, PrivilegeLevel
from pagan.shared.primitives.credentials import CredentialType
from pagan.shared.primitives.enablers import ProtoEnabler
from pagan.shared.primitives.predicate_trait import Predicate
from pagan.shared.primitives.tokens import CombinedToken, TokenType, Token


@adt
class PrivilegeOrigin:
    SERVICE: Case
    CREDENTIAL: Case
    RANDOM_USER: Case  # TODO: RECONSIDER - this is a weird one, basically says whoever runs a malicious file
    CREATED_USER: Case
    INHERIT_USER: Case  # only for domain login
    ELEVATE_SPLIT_TOKEN: Case
    ADVERSARY_CHOICE: Case  # for GPOs - TODO: RECONSIDER - for MVP always "system"
    FIXED: Case[PrivilegeLevel]
    ANONYMOUS_LOGIN: Case


class CredentialTarget(Enum):
    OS_ALL_LOCAL = auto()  # all the account info from the local database
    OS_CACHED = (
        auto()
    )  # the cached credentials including logged on domain users -> must be well simulated (win = 3 cached creds at once)
    OS_ACTIVE_ACCOUNT = auto()  # a user fills in a prompt
    SERVICE_SELF = auto()
    SERVICE_SELF_ALL = auto()
    SERVICE_OTHER = auto()
    MIMIC = auto()  # input also needs cred, so inherit specifics for this (2fa)
    MIMIC_USER = auto()
    MIMIC_ALL = auto()
    ANY_ACTUAL_USER = auto()  # limits to creds owned by the active user account
    ANY_PRESENT_USERS = auto()  # limits to creds owned by someone with an account/acces to a system
    NONE = auto()


class CapabilitySpecifier(Enum):
    NONE = auto()
    SERVICE = auto()
    ENABLER = auto()
    ACTION = auto()


@dataclass
class UsageScheme:
    given: CombinedToken
    grant: CombinedToken
    required_enabler: Optional[ProtoEnabler]
    required_capabilities: Predicate
    provided_capabilities: List[CapabilityTemplate] = field(default_factory=list)
    except_if: Optional[Predicate] = field(default=None)
    # negative prerequisite, such tokens should not be in the satchel
    # there are two possible uses, one is to eliminate chains of similar actions, the second is defenders in the future
    privilege_origin: PrivilegeOrigin = field(default=PrivilegeOrigin.FIXED(PrivilegeLevel.NONE))
    capability_specifier: CapabilitySpecifier = field(default=CapabilitySpecifier.NONE)
    credential_target: CredentialTarget = field(default=CredentialTarget.NONE)
    credential_type: CredentialType = field(default=CredentialType.UNSPEC)

    def __post_init__(self):
        if TokenType.CREDENTIAL.present_in(self.grant) and self.credential_target == CredentialTarget.NONE:
            raise ProgramLogicError("Usage granting credential must specify its target.")
        if TokenType.CREDENTIAL_2FA_TOKEN.present_in(self.grant) and self.credential_target == CredentialTarget.NONE:
            raise ProgramLogicError("Usage granting credential must specify its target.")
        if TokenType.CAPABILITY.present_in(self.grant) and self.privilege_origin == PrivilegeOrigin.FIXED(
            PrivilegeLevel.NONE
        ):
            raise ProgramLogicError("Usage granting privileges must specify where to inherit capabilities from.")
        if (
            TokenType.CAPABILITY.present_in(self.grant)
            and not TokenType.CAPABILITY_ALL.present_in(self.grant)
            and self.capability_specifier == CapabilitySpecifier.NONE
        ):
            raise ProgramLogicError("Usage granting capabilities must specify which artifact specifies the capability")
        if self.capability_specifier == CapabilitySpecifier.ENABLER and self.required_enabler is None:
            raise ProgramLogicError("Action must have a required enabler if capability is specified by an enabler.")


@serialize
@dataclass
class ActionDescription:
    name: str
    uses: List[UsageScheme] = field(skip=True)
    aliases: List[str] = field(default_factory=list)
    remote: bool = field(
        default=False
    )  # atm needed to make distinction between action with PrivilegeOrigin.SERVICE == ADMIN/USER
    # TODO: possibly more stuff

    def get_applicable_uses(self, tokens: Iterable[Token]) -> List[Tuple[int, UsageScheme]]:
        """
        Get all usage schemes, where the provided tokens include all tokens encoded in the usages "given" clause.
        @tokens: List of tokens available to attacker
        @assumption: only tokens available for the specific device are contained in 'tokens'
        """
        result = list()
        combined_tokens = TokenType.combine(*map(lambda t: t.type, tokens))
        for idx, usage in enumerate(self.uses):
            if usage.except_if is not None and usage.except_if.eval(tokens, []):
                continue
            if usage.given & combined_tokens == usage.given:
                result.append((idx, usage))

        return result
