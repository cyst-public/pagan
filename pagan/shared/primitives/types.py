from dataclasses import dataclass
from enum import Enum, IntFlag, StrEnum, auto
from typing import Any, Callable, Iterator, List
from pydantic import GetJsonSchemaHandler
from pydantic.dataclasses import dataclass as PydanticDataclass
from pydantic.json_schema import JsonSchemaValue
from pydantic_core import CoreSchema
from typing_extensions import Annotated
from pydantic.functional_validators import AfterValidator


def check_positive_and_neg_one(input: int) -> int:
    assert input >= -1
    return input


OrdinalId = Annotated[int, AfterValidator(check_positive_and_neg_one)]
PersonHandle = OrdinalId
AccountHandle = OrdinalId
ServiceHandle = OrdinalId
EnablerHandle = OrdinalId
DeviceHandle = OrdinalId
ActionHandle = OrdinalId
NodeHandle = OrdinalId
PersonHandle = OrdinalId
CredentialHandle = OrdinalId
EdgeKey = int  # for nx.MultiGraph edges, which are 3-tuples (from, to , key) key is just an int based on the docs


def check_port_range(input: int) -> int:
    assert input >= 1 and input <= 65535, "port number must be in range 1 - 65535"
    return input


PortNum = Annotated[int, AfterValidator(check_port_range)]


@PydanticDataclass
class PortRange:
    lower_bound: PortNum
    upper_bound: PortNum

    def matches(self, port: PortNum) -> bool:
        return port >= self.lower_bound and port <= self.upper_bound


class AccessType(StrEnum):
    NETWORK = auto()
    MEDIA = auto()
    SHARED_FILESYS = auto()


class EnumProvidingSchemeLegend(Enum):
    @classmethod
    def __get_pydantic_json_schema__(cls, core_schema: CoreSchema, handler: GetJsonSchemaHandler) -> JsonSchemaValue:
        description = "An enumeration. Legend: "
        for item in cls:
            description += f"| {item.name} -> {item.value} |"
        json_schema = handler(core_schema)
        json_schema["description"] = description

        return json_schema


class CapabilityType(StrEnum):
    ACCESS_FILESYS_GENERIC = auto()
    ACCESS_FILESYS_ELEVATED = auto()  # e.g access to /root/, %WINDIR%, ...
    ACCESS_FILESYS_SHARED = auto()  # shares (SMB, FTP ... )

    FILESYS_READ = auto()
    FILESYS_WRITE = auto()

    ACCESS_NETWORK_DOMAIN = (
        auto()
    )  # this privilege is not present for local services in Windows systems, so they are not able to auth in the domain
    # TODO: an authed exploit action usage without creds but this - but only for very specific services -> so maybe a separate action

    ACCOUNT_MANIPULATION_USER_ADD = auto()
    ACCOUNT_MANIPULATION_USER_EDIT = auto()
    ACCOUNT_MANIPULATION_USER_DELETE = auto()

    PERSIST_LOG_OUT = auto()  # guest users do not have this, their files / registries are cleaned upon logout

    EXECUTE_CODE = auto()  # includes commands, scheduled tasks, interacting with the registry

    SHUTDOWN = auto()
    DOMAIN_LOGIN = auto()

    PERSISTENCE = auto()

    IMPERSONATE = auto()  # e.g. send mails in the users name, etc.

    CONTROL_DOMAIN = auto()
    CONFIG_LOCAL_MACHINE = auto()  # e.g tamper with HKLM registry, global $ENV
    CONFIG_LOCAL_USER = auto()  # e.g. tamper with HKCU registry, user $ENV

    SE_DEBUG = auto()
    SE_LOAD_DRIVER = auto()
    SE_CREATE_SECURITY_TOKEN = auto()
    SE_TOKEN_IMPERSONATE = auto()
    UAC_ELEVATE = auto()

    # TODO: maybe more


class EnablerImpact(IntFlag):
    DATA_DISCLOSURE = 1 << 0
    DATA_TAMPERING = 1 << 1
    CREDENTIAL_LEAK = 1 << 2
    AUTH_MANIPULATION = 1 << 3
    AVAILABILITY_VIOLATION = 1 << 4
    CHANGE_MACHINE_CONFIGURATION = 1 << 5
    ARBITRARY_CODE_EXECUTION = (1 << 0) | (1 << 1) | (1 << 2) | (1 << 3) | (1 << 4) | (1 << 5)
    ALLOW = 1 << 6

    def present_in(self, other: int):
        return self & other == self

    def to_capability_types(self) -> Iterator[CapabilityType]:
        match self:
            case EnablerImpact.DATA_DISCLOSURE:
                yield CapabilityType.FILESYS_READ
                yield CapabilityType.ACCESS_FILESYS_GENERIC
                yield CapabilityType.ACCESS_FILESYS_ELEVATED
            case EnablerImpact.DATA_TAMPERING:
                yield CapabilityType.FILESYS_WRITE
                yield CapabilityType.ACCESS_FILESYS_GENERIC
                yield CapabilityType.ACCESS_FILESYS_ELEVATED
            case EnablerImpact.AUTH_MANIPULATION:
                yield CapabilityType.ACCOUNT_MANIPULATION_USER_EDIT
                yield CapabilityType.ACCOUNT_MANIPULATION_USER_ADD
                yield CapabilityType.ACCOUNT_MANIPULATION_USER_DELETE
            case EnablerImpact.CHANGE_MACHINE_CONFIGURATION:
                yield CapabilityType.CONFIG_LOCAL_MACHINE
            case EnablerImpact.ARBITRARY_CODE_EXECUTION:
                yield CapabilityType.EXECUTE_CODE
                yield CapabilityType.FILESYS_WRITE
                yield CapabilityType.FILESYS_READ
                yield CapabilityType.ACCESS_FILESYS_GENERIC
                yield CapabilityType.ACCESS_FILESYS_ELEVATED
                yield CapabilityType.CONFIG_LOCAL_MACHINE
                yield CapabilityType.CONFIG_LOCAL_USER
                yield CapabilityType.ACCOUNT_MANIPULATION_USER_DELETE
                yield CapabilityType.ACCOUNT_MANIPULATION_USER_ADD
                yield CapabilityType.ACCOUNT_MANIPULATION_USER_EDIT
                yield CapabilityType.SHUTDOWN


@dataclass(frozen=True)
class ActionGuard:
    actions: List[ActionHandle]
    guard: Callable[[Any], bool]
