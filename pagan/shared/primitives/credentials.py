import dataclasses
import random
from dataclasses import field
from pydantic import Field, ConfigDict
from pydantic.dataclasses import dataclass as PydanticDataclass
from enum import IntEnum
from typing import Self, List, Set
from abc import ABC, abstractmethod
from uuid import UUID, uuid4
from math import inf
from serde import serialize

from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.primitives_sets import (
    AccountSetOption,
    DeviceSetOption,
    ServiceSetOption,
)
from pagan.shared.primitives.set_option import SetOption
from pagan.shared.primitives.types import (
    AccountHandle,
    DeviceHandle,
    EnumProvidingSchemeLegend,
)


class CredentialType(IntEnum, EnumProvidingSchemeLegend):
    # IntFlag has stupid iteration just over multiples of two
    OS = 1
    MFA_PASSABLE = 2
    MFA_SECURE = 4
    APP_PRIMARY = 8
    MFA = MFA_PASSABLE | MFA_SECURE
    ANY_PRIMARY = OS | APP_PRIMARY
    APP_ANY_PASSABLE = APP_PRIMARY | MFA_PASSABLE
    APP_ANY = APP_PRIMARY | MFA
    UNSPEC = OS | MFA | APP_PRIMARY | ANY_PRIMARY  # TODO: we might want to make a group with only passable MFA

    def matches(self, other: Self) -> bool:
        return self & other != 0


@dataclasses.dataclass()
class ImplementsCredentialTemplateCheck(ABC):
    @abstractmethod
    def fits_template(self, other: Self) -> bool:
        pass

    @abstractmethod
    def get_account(self, limit_to: AccountSetOption = AccountSetOption.ALL()) -> AccountHandle:
        pass


@dataclasses.dataclass()
class WildcardCredential(ImplementsCredentialTemplateCheck):
    """
    Used as templates for filtering Credentials or as Wildcards that
    can be applied on multiple occasions.
    Notes:
    - The type is matched based on `CredentialType.matches()`.
    - Services should be checked for non-empty intersection.

    @attributes:
    - binding: represents whether the wildcard credential binds its generator to the
                consumer (used with CredentialTarget.SERVICE_SELF*)
    """

    accounts: AccountSetOption = field(default=AccountSetOption.ALL())
    devices: DeviceSetOption = field(default=DeviceSetOption.ALL())
    services: ServiceSetOption = field(default=ServiceSetOption.ALL())
    type: CredentialType = field(default=CredentialType.UNSPEC)
    binding: bool = field(default=False, hash=False, compare=False)
    id: UUID = field(default_factory=uuid4, hash=False, compare=False)

    def applicable_as(self, credential: "BaseCredential") -> bool:
        return credential.fits_template(self)

    def fits_template(self, other: Self) -> bool:
        if self.accounts.intersection(other.accounts).is_none():
            return False
        if self.devices.intersection(other.devices).is_none():
            return False
        if self.services.intersection(other.services).is_none():
            return False
        if not self.type.matches(other.type):
            return False
        return True

    def __hash__(self):
        return hash((self.accounts, self.devices, self.services, self.type, self.binding))

    def __eq__(self, other):
        return (
            self.accounts == other.accounts
            and self.devices == other.devices
            and self.services == other.services
            and self.binding == other.binding
        )

    def get_account(self, limit_to: AccountSetOption = AccountSetOption.ALL()) -> AccountHandle:
        if self.accounts.is_some():
            try:
                return random.choice(list(self.accounts.intersection(limit_to).as_iterator_notall()))
            except IndexError:
                raise ProgramLogicError("account choice from wildcard credential impossible")
        raise ProgramLogicError("account choice from wildcard credential impossible")


@serialize(serializer=SetOption.serialize_setOption_includers)
@PydanticDataclass(config=ConfigDict(arbitrary_types_allowed=True))
class BaseCredential(ImplementsCredentialTemplateCheck):
    devices: DeviceSetOption
    type: CredentialType
    services: ServiceSetOption
    account: AccountHandle = Field(exclude=True)

    def fits_template(self, template: WildcardCredential) -> bool:
        if self.account not in template.accounts:
            return False
        if self.devices.intersection(template.devices).is_none():
            return False
        if not self.type.matches(template.type):
            return False
        if self.services.intersection(template.services).is_none():
            return False
        return True

    def __hash__(self):
        return hash((self.devices, self.type, self.services, self.account))

    def get_account(self, limit_to: AccountSetOption = AccountSetOption.ALL()) -> AccountHandle:
        return self.account


@serialize(serializer=SetOption.serialize_setOption_includers)
@PydanticDataclass(config=ConfigDict(arbitrary_types_allowed=True))
class Credential(BaseCredential):
    online_guess_complexity: float = Field(default=inf, allow_inf_nan=True)
    unseal_complexity: float = field(default=0)
    batch: None | UUID = field(default=None)  # for binding primary and MFA creds together

    # TODO: EXPAND

    def is_sealed(self) -> bool:
        return self.unseal_complexity > 0

    def __post_init__(self) -> None:
        if self.account is None:
            raise ProgramLogicError("Credential must be bound to a user")
        if self.devices == DeviceSetOption.NONE():
            raise ProgramLogicError("Credential must be bound to a device")
        if self.type is None:
            raise ProgramLogicError("Credential must have a type")
        if self.services is None:
            raise ProgramLogicError("Credential must be bound to services")
        if self.unseal_complexity < 0 or self.unseal_complexity > 100:
            raise ProgramLogicError("Invalid unsealing complexity for credential")
        if self.type == CredentialType.UNSPEC:
            raise ProgramLogicError(
                "Credential type can not be unspecified, UNSPEC can only be used in requirement-templates."
            )

    def unseal(self) -> Self:
        return (
            Credential(
                account=self.account,
                device=self.devices,
                services=self.services,
                unseal_complexity=0,
                online_guess_complexity=self.online_guess_complexity,
                type=self.type,
            )
            if self.is_sealed()
            else self
        )

    def __hash__(self):
        return hash(
            (
                self.devices,
                self.type,
                self.services,
                self.account,
                self.online_guess_complexity,
                self.unseal_complexity,
                self.batch,
            )
        )
