import random
from typing import (
    Any,
    Callable,
    FrozenSet,
    Iterator,
    Generic,
    Type,
    TypeVar,
    Iterable,
    Self,
    Optional,
    Set,
    List,
)
from adt import adt, Case
from abc import abstractmethod, ABC

from pydantic import GetJsonSchemaHandler
from pydantic_core import CoreSchema
from pydantic_core.core_schema import (
    set_schema,
    json_or_python_schema,
    union_schema,
    is_instance_schema,
    plain_serializer_function_ser_schema,
    IntSchema,
)
from pydantic.json_schema import JsonSchemaValue
from serde import SerdeSkip, serialize

from pagan.shared.exceptions import ProgramLogicError


# type:ignore


@adt
class SetOptionLen:
    """Thank you python for not having int maxes"""

    INFINITY: Case  # meaning includes every possible element
    INTEGER: Case[int]

    def __ge__(self, other):
        def check_second(first: "SetOptionLen", second: "SetOptionLen"):
            return self.__class__.match(
                second,
                infinity=lambda: False,
                integer=lambda _: (first.integer() >= second.integer()),
            )

        return self.match(infinity=lambda: True, integer=lambda _: check_second(self, other))

    def __lt__(self, other):
        return not self.__ge__(other)

    def __le__(self, other):
        def check_second(first: "SetOptionLen", second: "SetOptionLen"):
            return self.__class__.match(
                second,
                infinity=lambda: True,
                integer=lambda _: (first.integer() <= second.integer()),
            )

        return self.match(infinity=lambda: False, integer=lambda _: check_second(self, other))

    def __gt__(self, other):
        return not self.__le__(other)


SetElement = TypeVar("SetElement")


@adt
class SetOption(ABC, Generic[SetElement]):
    NONE: Case
    ALL: Case
    SOME: Case[FrozenSet[SetElement]]

    @classmethod
    def __get_pydantic_json_schema__(cls, core_schema: CoreSchema, handler: GetJsonSchemaHandler) -> JsonSchemaValue:
        json_schema = handler(core_schema)
        json_schema["type"] = "array"
        json_schema["items"] = "integer"
        json_schema["uniqueItems"] = True

        return json_schema

    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: Callable[[Any], CoreSchema],
    ) -> CoreSchema:
        def inner_raise() -> None:
            raise ProgramLogicError("Config files shall not use the `ALL` option")

        from_set_schema = set_schema()

        return json_or_python_schema(
            json_schema=from_set_schema,
            python_schema=union_schema([is_instance_schema(SetOption), from_set_schema]),
            serialization=plain_serializer_function_ser_schema(
                lambda instance: instance.match(
                    none=lambda: set(),
                    all=lambda: inner_raise(),
                    some=lambda inner: inner,
                )
            ),
        )

    @classmethod
    def _intersection(cls, first: Self, second: Self) -> Self:
        def _check_other(first_: Self, second_: Self) -> Self:
            return cls.match(
                second_,
                none=lambda: cls.NONE(),
                all=lambda: first_,
                some=lambda s: cls.SOME(new_set) if (new_set := (first_.some().intersection(s))) else cls.NONE(),
            )

        return cls.match(
            first,
            none=lambda: cls.NONE(),
            all=lambda: second,
            some=lambda _: _check_other(first, second),
        )

    @classmethod
    def _union(cls, first: Self, second: Self) -> Self:
        def _check_other(first_: Self, second_: Self) -> Self:
            return cls.match(
                second_,
                none=lambda: first_,
                all=lambda: cls.ALL(),
                some=lambda s: cls.SOME(first_.some().union(s)),
            )

        return cls.match(
            first,
            none=lambda: second,
            all=lambda: cls.ALL(),
            some=lambda _: _check_other(first, second),
        )

    @classmethod
    def _except(
        cls,
        first: Self,
        second: Self,
        case_all: Iterable[SetElement],
    ) -> Self:
        def raise_ex(ex: Exception):
            raise ex

        def _check_other_some(first_: Self, second_: Self) -> Self:
            return cls.match(
                second_,
                none=lambda: first_,
                all=lambda: cls.NONE(),
                some=lambda s: cls.SOME(first_.some().difference(s)),
            )

        def _check_other_all(first_: Self, second_: Self) -> Self:
            return cls.match(
                second_,
                none=lambda: first_,
                all=lambda: cls.NONE(),
                some=lambda other: cls.SOME(inner) if (inner := set(case_all).difference(other)) else cls.NONE(),
            )

        return cls.match(
            first,
            none=lambda: cls.NONE(),
            all=lambda: _check_other_all(first, second),
            some=lambda _: _check_other_some(first, second),
        )

    def intersection(self, other: Self) -> Self:
        return self.__class__._intersection(self, other)

    def union(self, other: Self) -> Self:
        return self.__class__._union(self, other)

    def except_of(self, other: Self, case_all: Iterable[SetElement]) -> Self:
        return self.__class__._except(self, other, case_all)

    def size(self) -> SetOptionLen:
        return self.match(
            none=lambda: SetOptionLen.INTEGER(0),
            all=lambda: SetOptionLen.INFINITY(),
            some=lambda s: SetOptionLen.INTEGER(len(s)),
        )

    def is_all(self) -> bool:
        return self.match(
            none=lambda: False,
            all=lambda: True,
            some=lambda _: False,
        )

    def is_none(self) -> bool:
        return self.match(
            none=lambda: True,
            all=lambda: False,
            some=lambda _: False,
        )

    def is_some(self) -> bool:
        return self.match(
            none=lambda: False,
            all=lambda: False,
            some=lambda _: True,
        )

    def as_iterator(self, case_all: Iterable[SetElement]) -> Iterator[SetElement]:
        return iter(self.match(none=lambda: set(), all=lambda: case_all, some=lambda inner: inner))

    def as_iterator_notall(self) -> Iterator[SetElement]:
        def invalid():
            raise RuntimeError(
                "Can not create iterator from the ALL case without hints. Use `as_iterable`, which supports hints."
            )

        target = self.match(none=lambda: set(), all=lambda: invalid(), some=lambda inner: inner)
        return iter(target)

    def __contains__(self, elem: SetElement) -> bool:
        return self.match(none=lambda: False, all=lambda: True, some=lambda s: elem in s)

    def has(self, elem: SetElement) -> bool:
        return self.__contains__(elem)

    @abstractmethod
    def inner_type(self) -> Type:
        pass

    def retain(self, f: Callable) -> Self:
        def lambda_raise():
            raise RuntimeError("retain can not be applied to SetOption.ALL")

        return self.match(none=lambda: self, all=lambda_raise, some=lambda s: self.SOME(set(filter(f, s))))

    @staticmethod
    def serialize_setOption_includers(typ: Any, o: Any):
        if isinstance(o, SetOption):
            return o.match(none=lambda: "none", all=lambda: "all", some=lambda s: list(s))
        else:
            raise SerdeSkip

    def __hash__(self):
        return self.match(
            none=lambda: hash(str(self)), all=lambda: hash(str(self)), some=lambda inner: hash(frozenset(inner))
        )

    def random_choice(self, option_all: Optional[List[SetElement]] = None) -> Optional[SetElement]:
        return self.match(
            none=lambda: None,
            all=lambda: random.choice(option_all) if option_all is not None else None,
            some=lambda inner: random.choice(list(inner)),
        )
