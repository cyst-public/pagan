from abc import ABC, abstractmethod
from typing import Iterable, Union, Iterator

from pagan.shared.primitives.capabilities import CapabilityTemplate
from pagan.shared.primitives.tokens import Token


class Predicate(ABC):
    @abstractmethod
    def eval(
        self,
        tokens: Iterable[Token] = (),
        caps: Iterable[CapabilityTemplate] = (),
        enablers: Iterable["EnablerImpact"] = (),
    ) -> bool:
        pass

    @abstractmethod
    def __contains__(self, elem: Union[Token, CapabilityTemplate, "EnablerImpact"]) -> bool:
        pass

    @abstractmethod
    def __iter__(self) -> Iterator:
        pass
