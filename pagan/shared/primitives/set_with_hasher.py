from typing import (
    AbstractSet,
    Iterable,
    Hashable,
    TypeVar,
    Generic,
    Sized,
    Never,
    Dict,
    List,
    Callable,
)
from abc import ABC
from dataclasses import dataclass, field


T = TypeVar("T", bound=Hashable)


@dataclass
class SetWithHasherAndEq(ABC, AbstractSet, Iterable, Sized, Generic[T]):
    hasher: Callable[[T], int]
    eq_func: Callable[[T, T], bool]
    items: Dict[int, List[T]] = field(default_factory=dict)


class FrozenSetWithHasherAndEq(SetWithHasherAndEq, Hashable):
    pass


class MutableSetWithHasherAndEq(SetWithHasherAndEq):
    pass
