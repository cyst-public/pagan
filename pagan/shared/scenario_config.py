from dataclasses import dataclass
from typing import Iterable, Set, Tuple, Optional

from pagan.generation.mock.applicability_mock import ApplicabilityMatrix
from pagan.shared.stores.accounts import AccountStore
from pagan.shared.stores.actions import ActionStore
from pagan.shared.stores.credentials import CredentialStore
from pagan.shared.stores.enabler import EnablerStore
from pagan.shared.stores.people import PersonStore
from pagan.shared.stores.services import ServiceStore

from pagan.generation.preparation.topology import Topology
from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.adversary import Adversary
from pagan.shared.primitives.types import (
    DeviceHandle,
    OrdinalId,
    AccessType,
)
from pagan.shared.primitives.predicate_trait import Predicate
from pagan.shared.primitives.credentials import Credential
from pagan.shared.primitives.capabilities import CapabilityTemplate
from pagan.shared.primitives.tokens import Token


@dataclass(frozen=True)
class Goal:
    device: OrdinalId
    action: OrdinalId
    specifier: Predicate

    def conforming(
        self,
        action: OrdinalId,
        device: OrdinalId,
        tokens: Set[Token],
        capabilities: Set[CapabilityTemplate],
    ) -> bool:
        if action != self.action:
            return False
        if device != self.device:
            return False
        return self.specifier.eval(tokens=tokens, caps=capabilities)


@dataclass
class ScenarioConfig:
    adversary: Adversary
    # @Stores
    service_store: ServiceStore
    enabler_store: EnablerStore
    user_account_store: AccountStore
    action_store: ActionStore
    credential_store: CredentialStore
    people_store: PersonStore

    topology: Topology

    # @Starting tokens
    available_tokens: Set[Token]
    available_credentials: Set[Credential]

    # @Matrices
    action_service_applicability: ApplicabilityMatrix

    goal: Goal

    max_devices_touched: int
    max_action_count: int

    def get_accesses_from(
        self, device: DeviceHandle, types: Optional[Iterable[AccessType]] = None
    ) -> Iterable[Tuple[DeviceHandle, AccessType]]:
        if (accesses := self.topology.accesses.elems.get(device, None)) is None:
            raise ProgramLogicError("Querying non existing device handle.")
        if types is None:
            return accesses
        return filter(lambda accessinfo: accessinfo[1] in types, accesses)
