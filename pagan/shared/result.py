from dataclasses import dataclass, field
from typing import List
from serde import serde

from pagan.shared.primitives import NodeID


@serde
@dataclass
class GToken:
    type: str
    target: str = field(default="None")


@serde
@dataclass
class GNode:
    nid: NodeID
    device: str
    vlan: str
    action: str
    service: str
    exploit: List[str]


@serde
@dataclass
class GEdge:
    from_nid: NodeID
    to_nid: NodeID
    tokens: List[GToken] = field(default_factory=list)


@serde
@dataclass
class MappingResult:
    nodes: List[GNode] = field(default_factory=list)
    edges: List[GEdge] = field(default_factory=list)
