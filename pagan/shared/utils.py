import random
from typing import Iterable, Callable, TypeVar, Optional, Any, Type


from pagan.shared.exceptions import ProgramLogicError

T = TypeVar("T")


def _foldl(
    accumulator: T,
    func: Callable,
    container: Iterable[T],
    stop_condition: Optional[T] = None,
) -> Any:
    if stop_condition is None:
        for elem in container:
            accumulator = func(accumulator, elem)
        return accumulator
    # else
    for elem in container:
        accumulator = func(accumulator, elem)
        if accumulator == stop_condition:
            break
    return accumulator


# any
def foldl_or(container: Iterable[bool]) -> bool:
    return _foldl(
        accumulator=False,
        func=lambda acc, e: acc or e,
        container=container,
        stop_condition=True,
    )


def foldl_max(container: Iterable[int]) -> int:  # actually uint if it existed
    return _foldl(
        accumulator=-1,
        func=lambda acc, e: acc if acc > e else e,
        container=container,
    )


# all
def foldl_and(container: Iterable[bool]) -> bool:
    return _foldl(
        accumulator=True,
        func=lambda acc, e: acc and e,
        container=container,
        stop_condition=False,
    )


def iter_check_equal(container: Iterable[T], eq: Callable) -> bool:
    iterator = iter(container)
    try:
        base = next(iterator)
    except StopIteration:
        return True
    for elem in iterator:
        if not eq(base, elem):
            return False
    return True


def NonZeroArgsMethod(func: Callable) -> Callable:
    def wrapper(this, *args):
        if len(args) > 0:
            return func(this, *args)
        raise ProgramLogicError(f"Method {func} expects at least one arg in *args.")

    return wrapper


class VarArgTypesUniformlyOneOf:
    @NonZeroArgsMethod
    def __init__(self, *types: Type) -> None:
        if not isinstance(types[0], type):
            raise ProgramLogicError("Decorator `VarArgTypesUniformlyOneOf` can not be used with type arguments")
        self.types = list(types)

    def __call__(self, func: Callable) -> Any:
        def comparator(a: Any, b: Any, t: Type) -> bool:
            return isinstance(a, t) == isinstance(b, t)

        def wrapper(this, *args):
            if len(args) <= 0:
                raise ProgramLogicError("Decorator `VarArgTypesUniformlyOneOf` must be wrapped in `NonZeroArgsMethod`")
            try:
                ft = next(filter(lambda t: isinstance(args[0], t), self.types))
            except StopIteration:
                raise ProgramLogicError(f"args[0] ({args[0]}) is of invalid type, valid options are {self.types}")
            if iter_check_equal(args, lambda a, b: comparator(a, b, ft)):
                return func(this, ft, *args)
            raise ProgramLogicError("Uniform *args expected, different types given.")

        return wrapper


def coin_toss(percentage: int) -> bool:
    return random.randint(0, 100) < percentage
