from typing import Iterable, List, Tuple

from pagan.shared.primitives.actions import ActionDescription, UsageScheme
from pagan.shared.primitives.capabilities import AttackerCapability
from pagan.shared.primitives.tokens import Token
from pagan.shared.primitives.types import ActionHandle, OrdinalId
from pagan.shared.stores.base import GenericStore
from pagan.shared.capability_computation import CapabilityComputationEngine

from chainingiterator import Chi


class ActionStore(GenericStore[ActionDescription, ActionHandle]):
    def get_applicable(
        self, tokens: Iterable[Token], capabilities: Iterable[AttackerCapability]
    ) -> List[Tuple[OrdinalId, int, UsageScheme]]:
        """
        Get all oids of actions that can be used with the provided tokens, combined with all their valid usage schemes.
        :param tokens: provided tokens
        :param capabilities: the capabilities of the attacker when querying actions (should already be filtered to
        include only global and specific device caps)
        :return: List of tuples (oid, usagescheme)
        """
        result = []
        for oid, action in self._items.items():
            for idx, scheme in action.get_applicable_uses(tokens):
                result.append((oid, idx, scheme))

        return (
            Chi(result)
            .filter(
                lambda triplet: CapabilityComputationEngine.is_applicable_scheme(triplet[2], capabilities),
            )
            .collect(list)
        )  # type: ignore
