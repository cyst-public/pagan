from pagan.shared.primitives.credentials import Credential
from pagan.shared.primitives.types import CredentialHandle
from pagan.shared.stores.base import GenericStore


CredentialStore = GenericStore[Credential, CredentialHandle]
