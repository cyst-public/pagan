import dataclasses
from typing import Dict

import requests

from pagan.shared.exceptions import StoreException, StoreInconsistency
from pagan.shared.primitives.types import OrdinalId, ServiceHandle
from pagan.shared.stores.base import GenericStore

from pagan_rs.pagan_rs import Service

SERVICES_STREAM = "http://localhost:8080/services_all_for_pagan"


@dataclasses.dataclass
class ServiceStore(GenericStore[Service, ServiceHandle]):
    name_map: Dict[str, OrdinalId] = dataclasses.field(default_factory=dict)

    def add(self, item: Service) -> OrdinalId:
        if self._blocked:
            raise StoreException(handle=-1, typ=StoreInconsistency.STORE_BLOCKED)
        self._add(item.id(), item)
        self.name_map[item.name()] = item.id()
        return item.id()

    def get_by_name(self, name: str) -> OrdinalId:
        return self.name_map[name]


async def load_service_store_from_kb() -> ServiceStore:
    store = ServiceStore()
    payload = requests.get(SERVICES_STREAM, stream=True)
    for line in map(lambda jsonline: jsonline.decode("utf-8"), payload.iter_lines(decode_unicode=True)):
        service = Service.from_json(line)
        store.add(service)
    return store
