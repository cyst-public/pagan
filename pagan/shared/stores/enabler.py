from collections import defaultdict
from dataclasses import dataclass, field
from typing import Dict, Generator, Set, Tuple
from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.enablers import Enabler, ProtoEnabler
from pagan.shared.primitives.types import ActionHandle, EnablerHandle, ServiceHandle
from pagan.shared.stores.actions import ActionStore
from pagan.shared.stores.base import GenericStore, ItemHandle
from pagan.shared.stores.services import ServiceStore


@dataclass
class EnablerStore(GenericStore[Enabler, EnablerHandle]):
    _action_store_ref: ActionStore
    _service_store_ref: ServiceStore
    _service_index: Dict[ServiceHandle, Set[EnablerHandle]] = field(init=False)
    _action_index: Dict[ActionHandle, Set[EnablerHandle]] = field(init=False)

    def __post_init__(self) -> None:
        self._service_index = defaultdict(set)
        self._action_index = defaultdict(set)

    def get_conforming(
        self, template: ProtoEnabler, service: ServiceHandle
    ) -> Generator[Tuple[EnablerHandle, Enabler], None, None]:
        for handle, instance in self._items.items():
            if instance.services.has(service) and instance.matches_proto(template):
                yield handle, instance

    def add(self, item: Enabler) -> ItemHandle:
        handle = super().add(item)
        for service_handle in item.services.as_iterator(self._service_store_ref.handles):
            self._service_index[service_handle].add(handle)
        for action_handle in item.actions.as_iterator(self._action_store_ref.handles):
            self._action_index[action_handle].add(handle)
        return handle

    def get_handles_for_action(self, action: ActionHandle) -> Set[EnablerHandle]:
        if (enablers := self._action_index.get(action, None)) is not None:
            return enablers
        return set()

    def get_handles_for_service(self, service: ServiceHandle) -> Set[EnablerHandle]:
        if (enablers := self._service_index.get(service, None)) is not None:
            return enablers
        return set()
        # raise ProgramLogicError(f"Non-existing service handle given {service}")
