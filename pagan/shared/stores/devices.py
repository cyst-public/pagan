from pagan.shared.primitives.devices import Device
from pagan.shared.primitives.types import DeviceHandle
from pagan.shared.stores.base import GenericStore
from pydantic.dataclasses import dataclass as PydanticDataclass


class DeviceStore(GenericStore[Device, DeviceHandle]):
    def add(self, item: Device) -> DeviceHandle:
        retval = super().add(item)
        item.set_store_ref(retval)
        return retval
