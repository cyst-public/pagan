from pagan.shared.primitives.person import Person
from pagan.shared.primitives.types import PersonHandle
from pagan.shared.stores.base import GenericStore


PersonStore = GenericStore[Person, PersonHandle]
