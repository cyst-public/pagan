from dataclasses import dataclass, field
from typing import Dict, Tuple
from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.accounts import InfraUserAccount
from pagan.shared.primitives.types import AccountHandle
from serde import serialize

from pagan.shared.stores.base import GenericStore


@serialize
@dataclass
class AccountStore(GenericStore[InfraUserAccount, AccountHandle]):
    _uac_elevation_pairs: Dict[AccountHandle, AccountHandle] = field(init=False, default_factory=dict)

    def add_uac_elevation_pair(self, base: AccountHandle, elevated: AccountHandle) -> None:
        self._uac_elevation_pairs[base] = elevated

    def uac_elevate(self, account: AccountHandle) -> Tuple[AccountHandle, bool]:
        return (elev, True) if (elev := self._uac_elevation_pairs.get(account, None)) is not None else (account, False)

    def query_next_handle(self) -> AccountHandle:
        if self._blocked:
            raise ProgramLogicError("Store is locked")
        return self._next_handle

    def get_related_accounts(self, handle: AccountHandle):
        pass

    def __hash__(self):
        return hash((super(self), list(self._uac_elevation_pairs.items())))
