from dataclasses import field, dataclass
from typing import Callable, Dict, Generator, Generic, Set, TypeVar

from serde import serialize

from pagan.shared.exceptions import StoreException, StoreInconsistency
from pagan.shared.primitives.accounts import InfraUserAccount
from pagan.shared.primitives.actions import ActionDescription
from pagan.shared.primitives.credentials import Credential
from pagan.shared.primitives.devices import Device
from pagan.shared.primitives.enablers import Enabler
from pagan.shared.primitives.person import Person
from pagan.shared.primitives.services import Service
from pagan.shared.primitives.types import (
    AccountHandle,
    ActionHandle,
    DeviceHandle,
    EnablerHandle,
    PersonHandle,
    ServiceHandle,
)


StoreItem = TypeVar(
    "StoreItem",
    Service,
    ActionDescription,
    Enabler,
    InfraUserAccount,
    Device,
    Credential,
    Person,
)


ItemHandle = TypeVar(
    "ItemHandle",
    ServiceHandle,
    ActionHandle,
    EnablerHandle,
    AccountHandle,
    DeviceHandle,
    PersonHandle,
)


@serialize
@dataclass
class GenericStore(Generic[StoreItem, ItemHandle]):
    _next_handle: ItemHandle = field(init=False, default=0)  # type: ignore
    _blocked: bool = field(default=False, init=False)
    _items: Dict[ItemHandle, StoreItem] = field(init=False, default_factory=dict)

    @property
    def handles(self) -> Set[ItemHandle]:
        return set(self._items.keys())

    def add(self, item: StoreItem) -> ItemHandle:
        if item is None:
            return -1
        if self._blocked:
            raise StoreException(handle=-1, typ=StoreInconsistency.STORE_BLOCKED)
        handle = self._next_handle
        self._add(handle, item)
        self._next_handle += 1
        return handle

    def _add(self, handle: ItemHandle, item: StoreItem) -> None:
        if self._items.get(handle, None) is not None:
            raise StoreException(handle=handle, typ=StoreInconsistency.ALREADY_PRESENT_OID)
        self._items[handle] = item

    def get(self, handle: ItemHandle) -> StoreItem:
        if (item := self._items.get(handle, None)) is None:
            raise StoreException(handle=handle, typ=StoreInconsistency.NON_EXISTING_OID)
        return item

    def get_handle_for(self, item: StoreItem) -> ItemHandle:
        for key, val in self._items.items():
            if val == item:
                return key
        raise StoreException(handle=-1, typ=StoreInconsistency.NON_EXISTING_VALUE)

    def get_handles_with(self, fn: Callable) -> Generator[ItemHandle, None, None]:
        for key, val in self._items.items():
            if fn(val):
                yield key

    def get_all_with(self, fn: Callable) -> Generator[StoreItem, None, None]:
        for _, val in self._items.items():
            if fn(val):
                yield val

    def cardinality(self) -> int:
        return len(self._items)

    def block(self):
        self._blocked = True

    def __hash__(self):
        return hash(list(self._items.items()))
