from enum import Enum, auto


class StoreInconsistency(Enum):
    ALREADY_PRESENT_OID = auto()
    NON_EXISTING_OID = auto()
    NON_EXISTING_VALUE = auto()
    STORE_BLOCKED = auto()


class StoreException(Exception):
    def __init__(self, handle: int, typ: StoreInconsistency):
        super().__init__(f"Problem with item with oid: {handle}, type: {str(typ)}")


class SolverException(Exception):
    def __init__(self, reason: str):
        super().__init__(reason)


class ProgramLogicError(Exception):
    def __init__(self, reason: str):
        super().__init__(reason)


class AttackGraphException(Exception):
    def __init__(self, reason: str):
        super().__init__(reason)


class ImpossibleScenarioPathError(Exception):
    def __init__(self, reason: str):
        super().__init__(reason)
