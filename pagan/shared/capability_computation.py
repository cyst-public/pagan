from typing import Iterator, Optional, List, Iterable

from pagan.shared.primitives.actions import UsageScheme
from pagan.shared.primitives.capabilities import (
    CapabilityType,
    PrivilegeLevel,
    Capability,
    AttackerCapability,
    CapabilityTemplate,
    LimitType,
)
from pagan.shared.primitives.types import DeviceHandle, AccountHandle
from pagan.shared.primitives.accounts import InfraUserAccount


class CapabilityComputationEngine:
    def __init__(self):
        pass

    @staticmethod
    def compute_user_capability(
        to: CapabilityType,
        level: PrivilegeLevel,
        device: DeviceHandle,
    ) -> Capability:
        return Capability(
            to=to,
            privilege_limit=level,
            on_device=(device if device != -1 else None),
        )

    @staticmethod
    def compute_attacker_capability(
        user_id: AccountHandle,
        user_cap: Capability,
    ) -> AttackerCapability:
        return AttackerCapability(
            to=user_cap.to,
            privilege_limit=user_cap.privilege_limit,
            on_device=user_cap.on_device,
            as_user=user_id,
        )

    @staticmethod
    def all_inherited_from_user(
        user_id: AccountHandle,
        user: InfraUserAccount,
        device: DeviceHandle,
    ) -> Iterator[AttackerCapability]:
        eligible_caps = user.capabilities.get(device, set())
        for cap in eligible_caps:
            yield CapabilityComputationEngine.compute_attacker_capability(user_id, cap)

    @staticmethod
    def inherit_specific_from_user(
        user_id: AccountHandle,
        user: InfraUserAccount,
        device: DeviceHandle,
        capability_type: CapabilityType,
    ) -> AttackerCapability:
        raise NotImplementedError

    @staticmethod
    def is_applicable_scheme(scheme: UsageScheme, capabilities: Iterable[AttackerCapability]) -> bool:
        return scheme.required_capabilities.eval(caps=capabilities)
        # check if we find at least one conforming attacker capability to all required caps
        # return all(
        #     map(
        #         lambda req: any(
        #             map(
        #                 lambda atk: CapabilityComputationEngine.conforming(atk, req, None),
        #                 capabilities,
        #             )
        #         ),
        #         scheme.required_capabilities,
        #     )
        # )
