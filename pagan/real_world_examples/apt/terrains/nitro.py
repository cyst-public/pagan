from pagan_rs.pagan_rs import EnablerType

from pagan.generation.mock.applicability_mock import ApplicabilityMatrix, ApplicabilityEntry
from pagan.generation.preparation.topology import Topology, AccessRule, FwAction, RuleDirection, PortSet
from pagan.generation.preparation.topology.topology_definitions import (
    AccessesMapAndRules,
    DeviceHandlesView,
    StaticWRKSegment,
)
from pagan.shared.primitives.accounts import InfraUserAccount
from pagan.shared.primitives.adversary import Adversary
from pagan.shared.primitives.credentials import Credential, CredentialType
from pagan.shared.primitives.devices import Device, OperatingSystem, SegmentType, DeviceRole
from pagan.shared.primitives.enablers import Enabler, EnablerLocality
from pagan.shared.primitives.predicate_implementations import All
from pagan.shared.primitives.primitives_sets import ActionSetOption, ServiceSetOption, DeviceSetOption
from pagan.shared.primitives.tokens import Token, TokenType
from pagan.shared.primitives.types import AccessType, EnablerImpact, CapabilityType
from pagan.shared.scenario_config import Goal, ScenarioConfig
from pagan.shared.stores.credentials import CredentialStore
from pagan.shared.stores.people import PersonStore
from pagan.shared.stores.services import ServiceStore, load_service_store_from_kb
from pagan.shared.stores.devices import DeviceStore
from pagan.shared.stores.enabler import EnablerStore
from pagan.adversary import QuiverStore, Quiver
from pagan.shared.stores.accounts import AccountStore
from pagan.shared.primitives.capabilities import Capability, LimitType, PrivilegeLevel
from pagan.generation.preparation.accounts.account_generation import (
    BASIC_USER_CAPS_LOCAL,
    ADVANCED_USER_CAPS,
    RESTRICTED_ADMIN_CAPS,
    FULL_ADMIN_CAPS,
    SYSTEM_CAPS,
    SERVICE_CAPS,
)


async def get_config():
    service_store = await load_service_store_from_kb()
    device_store = DeviceStore()
    untrusted_internet = device_store.add(
        Device(
            name="untrusted internet",
            vlan="internet",
            vlan_num=0,
            os=OperatingSystem.MOCK,
            roles=[],
            segment_info=SegmentType.INTERNET,
        )
    )

    device_a_handle = device_store.add(
        (
            device_a := Device(
                name="device a",
                vlan="internal",
                vlan_num=1,
                os=OperatingSystem.WINDOWS_DESKTOP,
                roles=[DeviceRole.WORKSTATION],
                segment_info=SegmentType.INTERNAL,
            )
        )
    )

    device_b_handle = device_store.add(
        (
            device_b := Device(
                name="device b",
                vlan="internal",
                vlan_num=1,
                os=OperatingSystem.WINDOWS_DESKTOP,
                roles=[DeviceRole.WORKSTATION],
                segment_info=SegmentType.INTERNAL,
            )
        )
    )
    accesses = AccessesMapAndRules()
    accesses.add_bidirectional(
        untrusted_internet,
        device_a_handle,
        AccessType.NETWORK,
        AccessRule(
            action=FwAction.ALLOW,
            direction=RuleDirection.BIDIRECTIONAL,
            source_handle=untrusted_internet,
            source_ports=PortSet.EMAIL_CLIENT_SERVER,
            destination_handle=device_a_handle,
            destination_ports=PortSet.EMAIL_CLIENT_SERVER,
        ),
    )
    accesses.add_bidirectional(
        device_a_handle,
        device_b_handle,
        AccessType.NETWORK,
        AccessRule(
            action=FwAction.ALLOW,
            direction=RuleDirection.ESTABLISHED,
            source_handle=device_a_handle,
            source_ports=PortSet.DYNAMIC_PORTS,
            destination_handle=device_b_handle,
            destination_ports=[22],
        ),
    )

    handles = DeviceHandlesView(
        dmz=None,
        srv=None,
        wrk=[StaticWRKSegment(admin_pcs=[], workstations=[device_a_handle, device_b_handle])],
        ics=None,
        third_party=None,
        attacker=untrusted_internet,
        untrusted_internet=untrusted_internet,
        trusted_internet=untrusted_internet,
    )

    enabler_store = EnablerStore(_action_store_ref=QuiverStore, _service_store_ref=service_store)
    lsass_dump_enabler = enabler_store.add(
        Enabler(
            locality=EnablerLocality.LOCAL,
            impact={EnablerImpact.ALLOW},
            description="Old windows versions allow lsass dump by an Administrator",
            type=EnablerType.Cwe,
            actions=ActionSetOption.SOME({Quiver.credential_access.dump_cached_creds}),
            services=ServiceSetOption.SOME({service_store.get_by_name("lsass")}),
        )
    )

    phishing_enabler = enabler_store.add(
        Enabler(
            locality=EnablerLocality.BOTH,
            impact={EnablerImpact.ALLOW},
            description="Weak phish filtering",
            type=EnablerType.Cwe,
            actions=ActionSetOption.SOME({Quiver.movement.phishing_with_malware}),
            services=ServiceSetOption.SOME({service_store.get_by_name("outlook")}),
        )
    )

    account_store = AccountStore()
    credential_store = CredentialStore()

    host_A_admin_cred = Credential(
        devices=DeviceSetOption.SOME({device_a_handle}),
        type=CredentialType.OS,
        services=ServiceSetOption.SOME({service_store.get_by_name("ssh")}),
        account=-1,
        online_guess_complexity=75,
        unseal_complexity=50,
    )

    host_A_admin_acc = InfraUserAccount(
        owned_by=None,
        nickname="adminA",
        credential=host_A_admin_cred,
        capabilities={
            device_a_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=device_a_handle,
                )
                for cap_type in BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS + FULL_ADMIN_CAPS
            ]
        },
        domain_account=False,
    )

    host_A_admin_cred_id = credential_store.add(host_A_admin_cred)
    host_A_admin_acc_id = account_store.add(host_A_admin_acc)
    host_A_admin_acc.finalize_credential(host_A_admin_acc_id)

    host_A_user_cred = Credential(
        devices=DeviceSetOption.SOME({device_a_handle}),
        type=CredentialType.OS,
        services=ServiceSetOption.SOME({service_store.get_by_name("ssh")}),
        account=-1,
        online_guess_complexity=75,
        unseal_complexity=50,
    )

    host_A_user_acc = InfraUserAccount(
        owned_by=None,
        nickname="userA",
        credential=host_A_user_cred,
        capabilities={
            device_a_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.USER,
                    limit_type=LimitType.EXACT,
                    on_device=device_a_handle,
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
            ]
        },
        domain_account=False,
    )

    host_A_user_cred_id = credential_store.add(host_A_user_cred)
    host_A_user_acc_id = account_store.add(host_A_user_acc)
    host_A_user_acc.finalize_credential(host_A_user_acc_id)

    host_B_user_cred = Credential(
        devices=DeviceSetOption.SOME({device_b_handle}),
        type=CredentialType.OS,
        services=ServiceSetOption.SOME({service_store.get_by_name("ssh")}),
        account=-1,
        online_guess_complexity=75,
        unseal_complexity=50,
    )

    host_B_user_acc = InfraUserAccount(
        owned_by=None,
        nickname="userB",
        credential=host_B_user_cred,
        capabilities={
            device_b_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.USER,
                    limit_type=LimitType.EXACT,
                    on_device=device_b_handle,
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
            ]
        },
        domain_account=False,
    )

    host_B_user_cred_id = credential_store.add(host_B_user_cred)
    host_B_user_acc_id = account_store.add(host_B_user_acc)
    host_B_user_acc.finalize_credential(host_B_user_acc_id)

    host_A_system_acc = InfraUserAccount(
        owned_by=None,
        nickname="NT AUTHORITY\SYSTEM@A",
        credential=None,
        capabilities={
            device_a_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
                    limit_type=LimitType.EXACT,
                    on_device=device_a_handle,
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
                + SYSTEM_CAPS
            ]
        },
        domain_account=False,
    )

    host_A_system_acc_id = account_store.add(host_A_system_acc)

    host_A_service_acc = InfraUserAccount(
        owned_by=None,
        nickname="NT AUTHORITY\SERVICE@A",
        credential=None,
        capabilities={
            device_a_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.SERVICE,
                    limit_type=LimitType.EXACT,
                    on_device=device_a_handle,
                )
                for cap_type in SERVICE_CAPS
            ]
        },
        domain_account=False,
    )

    host_A_service_acc_id = account_store.add(host_A_service_acc)

    host_B_system_acc = InfraUserAccount(
        owned_by=None,
        nickname="NT AUTHORITY\SYSTEM@B",
        credential=None,
        capabilities={
            device_a_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
                    limit_type=LimitType.EXACT,
                    on_device=device_b_handle,
                )
                for cap_type in BASIC_USER_CAPS_LOCAL
                + ADVANCED_USER_CAPS
                + RESTRICTED_ADMIN_CAPS
                + FULL_ADMIN_CAPS
                + SYSTEM_CAPS
            ]
        },
        domain_account=False,
    )

    host_B_system_acc_id = account_store.add(host_B_system_acc)

    host_B_service_acc = InfraUserAccount(
        owned_by=None,
        nickname="NT AUTHORITY\SERVICE@B",
        credential=None,
        capabilities={
            device_a_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.SERVICE,
                    limit_type=LimitType.EXACT,
                    on_device=device_b_handle,
                )
                for cap_type in SERVICE_CAPS
            ]
        },
        domain_account=False,
    )

    host_B_service_acc_id = account_store.add(host_B_service_acc)

    host_AB_admin_cred = Credential(
        devices=DeviceSetOption.SOME({device_a_handle, device_b_handle}),
        type=CredentialType.OS,
        services=ServiceSetOption.SOME({service_store.get_by_name("ssh")}),
        account=-1,
        online_guess_complexity=75,
        unseal_complexity=50,
    )

    host_AB_admin_acc = InfraUserAccount(
        owned_by=None,
        nickname="adminAB",
        credential=host_AB_admin_cred,
        capabilities={
            device_a_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=device_a_handle,
                )
                for cap_type in BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS + FULL_ADMIN_CAPS
            ],
            device_b_handle: [
                Capability(
                    to=cap_type,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=device_b_handle,
                )
                for cap_type in BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS + FULL_ADMIN_CAPS
            ],
        },
        domain_account=True,
    )

    host_AB_admin_cred_id = credential_store.add(host_AB_admin_cred)
    host_AB_admin_acc_id = account_store.add(host_AB_admin_acc)
    host_AB_admin_acc.finalize_credential(host_AB_admin_acc_id)

    device_a.add_system_account(host_A_system_acc_id)
    device_a.add_services_account(host_A_service_acc_id)
    device_a.add_account(host_A_admin_acc_id, host_A_admin_acc)
    device_a.add_account(host_AB_admin_acc_id, host_AB_admin_acc)
    device_b.add_system_account(host_B_system_acc_id)
    device_b.add_services_account(host_B_service_acc_id)
    device_b.add_account(host_AB_admin_acc_id, host_AB_admin_acc)

    goal = Goal(
        device=device_b_handle,
        action=Quiver.impact.exfiltrate_data,
        specifier=All(TokenType.DATA),
    )

    topology = Topology(store=device_store, handles=handles, accesses=accesses)

    config = ScenarioConfig(
        adversary=Adversary(allowed_cracking_complexity=45, allowed_online_guess_complexity=50),
        # @Stores
        service_store=service_store,
        enabler_store=enabler_store,
        user_account_store=account_store,
        action_store=QuiverStore,
        credential_store=credential_store,
        people_store=PersonStore(),
        topology=topology,
        # @Starting tokens
        available_tokens={
            Token(type=TokenType.EXTERNAL_PHISHING_MAIL, device=None),
            Token(type=TokenType.ACCESS_VIA_NETWORK, device=device_a_handle),
        },
        available_credentials=set(),
        # @Matrices
        action_service_applicability=ApplicabilityMatrix(
            entries=[
                ApplicabilityEntry(
                    service=service_store.get_by_name("outlook"),
                    action=Quiver.movement.phishing_with_malware,
                    enabler=None,
                ),
                ApplicabilityEntry(
                    service=service_store.get_by_name("powershell"), action=Quiver.persistence.backdoor, enabler=None
                ),
                ApplicabilityEntry(
                    service=service_store.get_by_name("powershell"), action=Quiver.persistence.backdoor, enabler=None
                ),
                ApplicabilityEntry(
                    service=service_store.get_by_name("powershell"), action=Quiver.discovery, enabler=None
                ),
                ApplicabilityEntry(
                    service=service_store.get_by_name("powershell"), action=Quiver.discovery, enabler=None
                ),
                ApplicabilityEntry(
                    service=service_store.get_by_name("lsass"),
                    action=Quiver.credential_access.dump_cached_creds,
                    enabler=lsass_dump_enabler,
                ),
                ApplicabilityEntry(
                    service=service_store.get_by_name("ssh"), action=Quiver.movement.use_known_credentials, enabler=None
                ),
                ApplicabilityEntry(
                    service=service_store.get_by_name("zip"), action=Quiver.impact.collect_data, enabler=None
                ),
                ApplicabilityEntry(
                    service=service_store.get_by_name("powershell"), action=Quiver.impact.exfiltrate_data, enabler=None
                ),
            ]
        ),
        goal=goal,
        max_devices_touched=4,
        max_action_count=15,
    )

    return config
