from typing import Dict, Callable
from pagan.real_world_examples.apt.scenarios import (
    carbanak as carbanak_s,
    darkseoul as darkseoul_s,
    nitro as nitro_s,
    sandworm as sandworm_s,
    stuxnet as stuxnet_s,
)
from pagan.real_world_examples.apt.terrains import (
    carbanak as carbanak_cfg,
    nitro as nitro_cfg,
    darkseoul as darkseoul_cfg,
    stuxnet as stuxnet_cfg,
    sandworm as sandworm_cfg,
)

historic_scenarios: Dict[str, Callable] = {
    "Carbanak": carbanak_s.create_carbanak_graph,
    "DarkSeoul": darkseoul_s.create_darkseoul_graph,
    "Nitro": nitro_s.create_nitro_graph,
    "Sandworm": sandworm_s.create_sandworm_graph,
    "Stuxnet": stuxnet_s.create_stuxnet_graph,
}

historic_configs: Dict[str, Callable] = {
    "Carbanak": carbanak_cfg.get_config,
    "DarkSeoul": darkseoul_cfg.get_config,
    "Nitro": nitro_cfg.get_config,
    "Sandworm": sandworm_cfg.get_config,
    "Stuxnet": stuxnet_cfg.get_config,
}
