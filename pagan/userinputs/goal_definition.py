from typing import List
from pydantic.dataclasses import dataclass as PydanticDataclass
from enum import StrEnum
from pagan.shared.primitives.capabilities import PrivilegeLevel

from pagan.shared.primitives.types import ActionHandle, CapabilityType


class TargetType(StrEnum):
    DOMAIN_CONTROLLER = "domain_controller"
    DATABASE = "database"
    SHARES_SERVER = "shares_server"
    WORKSTATION = "workstation"
    ADMINISTRATOR_WORKSTATION = "administrator_workstation"
    ICS = "ics"


@PydanticDataclass
class CapabilityDefinition:
    target: TargetType
    to: CapabilityType
    privilege: PrivilegeLevel


@PydanticDataclass
class GoalDescription:
    target: TargetType
    action: ActionHandle
    required_capabilities: List[CapabilityDefinition]
    max_devices_on_path: int
