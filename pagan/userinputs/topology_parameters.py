from typing import Annotated, Dict, List

from pydantic.dataclasses import dataclass as PydanticDataclass
from pydantic import model_validator, AfterValidator
from enum import StrEnum, auto


def check_percentage(value: int) -> int:
    assert value >= 0 and value <= 100
    return value


Percentage = Annotated[int, AfterValidator(check_percentage)]


class IsolationLevel(StrEnum):
    STRICT = auto()
    MODERATE = auto()
    LOOSE = auto()


@PydanticDataclass
class TopologyParameters:
    # DMZ
    dmz: bool
    dmz_mail: bool
    dmz_pubserv: bool
    dmz_vpn: bool

    # SRV
    srv: bool
    srv_dc: bool
    srv_dc_count: int
    srv_shares: bool
    srv_services: bool
    srv_db: bool

    # WRK
    work_segments: bool
    work_segment_count: int
    work_segment_populations: List[int]
    work_segment_admin_stations: List[int]
    nomad_culture: Percentage  # give what proportion of workstations doe not have a constant user
    domain_controlled: bool
    admin_password_sanity: int

    # ICS
    ics: bool
    ics_controller_count: int
    ics_airgap: bool
    ics_jumphost: bool

    # 3rd party
    third_party: bool

    # misc
    intra_segment_isolation: IsolationLevel
    inter_segment_isolation: IsolationLevel

    @model_validator(mode="after")
    def check_list_lengths(self):
        work_segment_count = self.work_segment_count
        if (
            len(self.work_segment_populations) != work_segment_count
            or len(self.work_segment_admin_stations) != work_segment_count
        ):
            ValueError("Invalid length of workstation populations or admin machines")

        if self.ics_airgap and self.ics_jumphost:
            ValueError("Airgapped ics can not have a jumphost")

        if self.admin_password_sanity < 0 or self.admin_password_sanity > 3:
            ValueError("Admin password sanity must be either 1 - LOW, 2 - MEDIUM, 3-HIGH.")
        return self

    def as_dict(self) -> Dict:
        return self.__dict__
