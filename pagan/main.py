import asyncio
import time

import aiofiles
import aiofiles.os
import os
import concurrent.futures as ConFutures
import orjson
import requests
import networkx as nx


from sys import stderr, stdout
from pydantic import TypeAdapter
from argparse import ArgumentParser, Namespace
from typing import Any, Dict, List, Tuple
from colorama import Fore, Style, init as colorama_init
from functools import partial as PartialFunction
from serde.json import to_json

from pagan.adversary import QuiverStore, CrippleQuiverStore
from pagan.eval.matcher import PathMatcher
from pagan.generation.mock.applicability_mock import get_applicabilities
from pagan.generation.preparation.credentials import generate_credentials
from pagan.generation.scenario.scenario_automaton import (
    ScenarioStateMachine,
    node_match_wrap,
    edge_match_wrap,
    stop_condition,
)
from pagan.generation.scenario.scenario_specifier import ScenarioSpecifier
from pagan.generation.scenario.scenariobuilder import ScenarioBuilder
from pagan.real_world_examples import historic_configs, historic_scenarios
from pagan.shared.primitives.adversary import Adversary
from pagan.shared.primitives.person import Person
from pagan.shared.primitives.set_option import SetOption
from pagan.shared.primitives.tokens import Token, TokenType
from pagan.shared.primitives.types import AccessType, DeviceHandle, NodeHandle
from pagan.shared.scenario_config import Goal, ScenarioConfig
from pagan.generation.preparation.topology import (
    DeviceHandlesView,
    Topology,
)
from pagan.shared.stores.actions import ActionStore
from pagan.shared.stores.credentials import CredentialStore
from pagan.shared.stores.enabler import EnablerStore
from pagan.shared.stores.people import PersonStore
from pagan.shared.stores.accounts import AccountStore
from pagan.generation.preparation.goal import (
    choose_target_device,
    compute_goal_specifics,
    validate_target_action,
)
from pagan.shared.stores.services import ServiceStore, load_service_store_from_kb, SERVICES_STREAM
from pagan.userinputs.goal_definition import GoalDescription
from pagan.userinputs.topology_parameters import TopologyParameters


from pagan_rs.pagan_rs import Service, create_py_query

from pagan.generation.preparation.accounts.account_generation import generate_infra_accounts
from pagan.generation.mock.enablers_mock import generate_enablers


def create_argparser() -> ArgumentParser:
    argparser = ArgumentParser(prog="PAGAN", usage="python ./pagan/main.py -h {schemas, generate} ...")
    subparsers = argparser.add_subparsers(title="actions")

    schema_parser = subparsers.add_parser(name="schemas", help="Generate JSON schemas for inputs.")
    schema_parser.add_argument(
        "--which",
        required=True,
        type=str,
        choices=["user_profiles", "goals", "topology_params", "all"],
    )
    schema_parser.add_argument(
        "--store",
        help="Path to a folder where schemas should be stored. Defaults to `$pwd.path/schemas`.",
        default="./schemas",
        type=str,
    )
    schema_parser.set_defaults(func=schema_entry)

    generation_parser = subparsers.add_parser("generate", help="Generate scenarios and terrains.")
    generation_parser.add_argument(
        "--profiles",
        required=True,
        help="Path to a directory with JSON files of profile definitions.",
    )
    generation_parser.add_argument(
        "--topology_params",
        required=True,
        help="Path to file with topology parameter definitions.",
    )
    generation_parser.add_argument("--goal", required=True, help="Path to file with goal definitions.")
    generation_parser.set_defaults(func=generate_entry)

    historic_parser = subparsers.add_parser("historic", help="Inform about historic APT scenarios.")
    historic_subparsers = historic_parser.add_subparsers(title="actions")
    historic_show = historic_subparsers.add_parser("show", help="Show a historic scenario.")
    historic_show.add_argument(
        "--name", help="Name of the scenario. Use list to obtain available.", type=str, required=True
    )
    historic_show.set_defaults(func=history_show_entry)

    historic_list = historic_subparsers.add_parser("list", help="List available scenarios")
    historic_list.set_defaults(func=history_list_entry)

    async def wrap_help(_sink1):
        argparser.print_usage(stdout)

    argparser.set_defaults(func=wrap_help)

    return argparser


async def generate_topology(parameters_path: str) -> Topology:
    async with aiofiles.open(parameters_path, "r") as parameters_file:
        topology_cfg = await parameters_file.read()
        cfg = orjson.loads(topology_cfg)
        topology_params = TopologyParameters(**cfg)

    return Topology.from_parameters(topology_params)


async def load_profile(file_path: os.DirEntry) -> Person | None:
    async with aiofiles.open(file_path, "r") as profiles_file:
        profile_cfg = await profiles_file.read()
        cfg = orjson.loads(profile_cfg)
        return Person(**cfg)


async def load_profiles(profiles_path: str) -> PersonStore:
    if not await aiofiles.os.path.isdir(s=profiles_path):
        raise RuntimeError("Profiles path must point to a directory")
    profile_tasks = []
    for filepath in await aiofiles.os.scandir(profiles_path):
        profile_tasks.append(load_profile(filepath))
    people = await asyncio.gather(*profile_tasks, return_exceptions=True)  # TODO: check if this handles exceptions well
    person_store = PersonStore()
    for person in people:
        person_store.add(person)
    return person_store


def wrap_load_profiles(args: Namespace) -> PersonStore:
    return asyncio.run(load_profiles(profiles_path=args.profiles), debug=True)


async def load_goal(goals_path: str) -> GoalDescription:
    async with aiofiles.open(goals_path, "r") as goal_file:
        goal_cfg = await goal_file.read()
        cfg = orjson.loads(goal_cfg)
        return GoalDescription(**cfg)


def wrap_load_goal(args: Namespace) -> GoalDescription:
    return asyncio.run(load_goal(goals_path=args.goal), debug=True)


def specify_goals(
    goal_description: GoalDescription,
    devices: DeviceHandlesView,
    accesses: Dict[DeviceHandle, List[Tuple[DeviceHandle, AccessType]]],
    topology_cardinality: int,
) -> Goal:
    target_device = choose_target_device(goal_description.target, devices, accesses, topology_cardinality)
    target_action = validate_target_action(goal_description.action)

    specifiers = compute_goal_specifics(goal_description, target_device, devices)

    return Goal(device=target_device, action=target_action, specifier=specifiers)


async def generate_infra_accounts_wrap(
    topology: Topology, population: PersonStore, services: ServiceStore
) -> Tuple[AccountStore, CredentialStore]:
    return generate_infra_accounts(topology, population, services)


async def generate_service_credentials(
    population: PersonStore, credential_store: CredentialStore, service_store: ServiceStore
) -> None:
    generate_credentials(population, service_store, credential_store)


def compile_config(
    service_store: ServiceStore,
    account_store: AccountStore,
    action_store: ActionStore,
    credential_store: CredentialStore,
    people: PersonStore,
    topology: Topology,
    max_devices: int,
    goal: Goal,
) -> ScenarioConfig:
    auto_accesses = set(
        Token(type=TokenType.ACCESS_VIA_NETWORK, device=device)
        for device, typ in topology.accesses.elems[topology.handles.untrusted_internet]
        if typ == AccessType.NETWORK
    )
    enablers, handles = generate_enablers(service_store)
    return ScenarioConfig(
        adversary=Adversary(allowed_cracking_complexity=75, allowed_online_guess_complexity=60),
        # @Stores
        service_store=service_store,
        enabler_store=enablers,
        user_account_store=account_store,
        action_store=action_store,
        credential_store=credential_store,
        people_store=people,
        topology=topology,
        # @Starting tokens
        available_tokens=auto_accesses.union({Token(type=TokenType.NO_MFA_ALLOWED, device=None)}),
        available_credentials=set(),
        # @Matrices
        action_service_applicability=get_applicabilities(service_store, handles),
        goal=goal,
        max_devices_touched=max_devices,
        max_action_count=int((max_devices * 4) * 1.35),
        # max_action_count=10,
    )


def generate_scenario(
    config: ScenarioConfig,
) -> Tuple[ScenarioStateMachine, List[List[NodeHandle]]]:
    builder = ScenarioBuilder(configuration=config)
    builder.build()
    specifier = ScenarioSpecifier(scenario=builder, topology=config.topology)
    specifier.do_specify()
    return builder.automaton, builder.paths


async def write_profile_scheme(args: Namespace) -> None:
    async with aiofiles.open(file=f"{args.store}/person_schema.json", mode="wb") as schemafile:
        await schemafile.write(
            orjson.dumps(
                TypeAdapter(Person).json_schema(),
                option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
            )
        )
        print(
            "Saved profiles schema to" + Fore.GREEN + f"{args.store}/person_schema.json" + Style.RESET_ALL,
        )


async def write_goal_scheme(args: Namespace) -> None:
    async with aiofiles.open(file=f"{args.store}/goal_schema.json", mode="wb") as schemafile:
        await schemafile.write(
            orjson.dumps(
                TypeAdapter(GoalDescription).json_schema(),
                option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
            )
        )
        print(
            "Saved goals schema to" + Fore.GREEN + f"{args.store}/goal_schema.json" + Style.RESET_ALL,
        )


async def write_topology_schema(args: Namespace) -> None:
    async with aiofiles.open(file=f"{args.store}/topology_param_schema.json", mode="wb") as schemafile:
        await schemafile.write(
            orjson.dumps(
                TypeAdapter(TopologyParameters).json_schema(),
                option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
            )
        )
        print(
            "Saved topology params schema to"
            + Fore.GREEN
            + f"{args.store}/topology_param_schema.json"
            + Style.RESET_ALL,
        )


async def schema_entry(args: Namespace):
    choice = args.which
    await aiofiles.os.makedirs(name=args.store, exist_ok=True)
    async with asyncio.TaskGroup() as tg:
        if choice == "user_profiles" or choice == "all":
            tg.create_task(write_profile_scheme(args))
        if choice == "goals" or choice == "all":
            tg.create_task(write_goal_scheme(args))
        if choice == "topology_params" or choice == "all":
            tg.create_task(write_topology_schema(args))


def default_serializer(obj: Any) -> Any:
    if isinstance(obj, set):
        return list(obj)

    raise TypeError


async def generate_accounts_and_credentials(
    topology: Topology, people: PersonStore, services: ServiceStore
) -> Tuple[AccountStore, CredentialStore]:
    account_store, credential_store = await generate_infra_accounts_wrap(topology, people, services)
    await generate_service_credentials(people, credential_store, services)

    return account_store, credential_store


async def serialize_all(
    topology: Topology,
    scenario: ScenarioStateMachine,
    paths: List[List[NodeHandle]],
    credential_store: CredentialStore,
    account_store: AccountStore,
    enabler_store: EnablerStore,
    actions: ActionStore,
) -> None:
    await aiofiles.os.makedirs("./output/", exist_ok=True)
    topo_job = write_topology(topology)
    scenario_job = write_scenario(scenario, paths)
    services_job = write_services()
    accounts_job = write_accounts(account_store)
    credentials_job = write_credentials(credential_store)
    enablers_job = write_enablers(enabler_store)
    actions_job = write_actions(actions)

    await asyncio.gather(topo_job, scenario_job, services_job, accounts_job, credentials_job, enablers_job, actions_job)


def default_serializer(obj):
    if isinstance(obj, set):
        return list(obj)
    if isinstance(obj, frozenset):
        return list(obj)
    if isinstance(obj, SetOption):
        return obj.match(none=lambda: "none", all=lambda: "all", some=lambda s: list(s))
    raise TypeError


async def write_topology(topology: Topology) -> None:
    async with aiofiles.open("./output/topology.json", "wb") as devices_file:
        await devices_file.write(
            to_json(topology, option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2).encode(),
        )


async def write_scenario(scenario: ScenarioStateMachine, paths: List[List[NodeHandle]]) -> None:
    async with aiofiles.open("./output/scenario.json", "wb") as scenario_file:
        await scenario_file.write(
            orjson.dumps(
                nx.node_link_data(scenario.graph),
                option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
                default=default_serializer,
            )
        )

    async with aiofiles.open("./output/paths.txt", "wb") as paths_file:
        await paths_file.write(
            orjson.dumps(
                paths,
                option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
            )
        )


async def write_services() -> None:
    async with aiofiles.open("./output/services.jsonl", "wb") as services_file:
        payload = requests.get(SERVICES_STREAM, stream=True)
        await services_file.write(payload.content)
        # for line in map(lambda jsonline: jsonline.decode("utf-8"), payload.iter_lines(decode_unicode=True)):
        #     await services_file.write(line.)


async def write_accounts(accounts: AccountStore) -> None:
    async with aiofiles.open("./output/accounts.json", "wb") as accounts_file:
        await accounts_file.write(
            to_json(
                accounts,
                option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
            ).encode()
        )


async def write_credentials(credentials: CredentialStore) -> None:
    async with aiofiles.open("./output/credentials.json", "wb") as credentials_file:
        await credentials_file.write(
            to_json(
                credentials,
                option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
            ).encode()
        )


async def write_actions(actions: ActionStore) -> None:
    async with aiofiles.open("./output/actions.json", "wb") as actions_file:
        await actions_file.write(
            to_json(
                actions,
                option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
            ).encode()
        )


async def write_enablers(enablers: EnablerStore) -> None:
    async with aiofiles.open("./output/enablers.json", "wb") as enablers_file:
        await enablers_file.write(
            to_json(enablers, option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2, default=default_serializer).encode()
        )


async def generate_entry(args: Namespace):
    start_time = time.time_ns()
    db_engine = await create_py_query()
    async_loop = asyncio.get_running_loop()
    with ConFutures.ProcessPoolExecutor() as MPExecutor:
        services_task = load_service_store_from_kb()

        profiles_task = async_loop.run_in_executor(
            executor=MPExecutor, func=PartialFunction(wrap_load_profiles, args=args)
        )
        goals_task = async_loop.run_in_executor(executor=MPExecutor, func=PartialFunction(wrap_load_goal, args=args))
        topology_task = generate_topology(parameters_path=args.topology_params)

        people, goal_description, services, topology = await asyncio.gather(
            profiles_task, goals_task, services_task, topology_task, return_exceptions=True
        )

        goal_specification_task = async_loop.run_in_executor(
            executor=MPExecutor,
            func=PartialFunction(
                specify_goals,
                goal_description=goal_description,
                devices=topology.handles,
                accesses=topology.accesses.elems,
                topology_cardinality=topology.store.cardinality(),
            ),
        )

        account_job = generate_accounts_and_credentials(topology, people, services)

        goal = await goal_specification_task
        # path_builder = PathTree(goal.device)
        # path_builder.build(
        #     accesses=topology.accesses.elems,
        #     max_depth=goal_description.max_devices_on_path,
        #     start_device=topology.handles.untrusted_internet,
        # )
        account_store, credential_store = await account_job

    topology.finalize(services)

    config = compile_config(
        services,
        account_store,
        CrippleQuiverStore,
        credential_store,
        people,
        topology,
        goal_description.max_devices_on_path,
        goal,
    )
    actions = CrippleQuiverStore

    scenario, paths = generate_scenario(config)
    await serialize_all(topology, scenario, paths, credential_store, account_store, config.enabler_store, actions)
    end_time = time.time_ns()
    print(f"took {end_time - start_time} nanoseconds")

    # scenario, accepting_states, edge_keys = generate_scenario(config)
    # print(accepting_states)
    # goal_node = accepting_states[0]
    #
    # chosen_path = random.choice(list(nx.all_shortest_paths(scenario.graph, scenario.ROOT, goal_node)))
    # path_iter_forward = iter(chosen_path)
    # next(path_iter_forward)
    # for u, v in zip(chosen_path, path_iter_forward):
    #     print(scenario.graph.nodes[u]["state"])
    #     print("---------")
    #     for k in edge_keys[(u, v)]:
    #         print(scenario.graph.edges[u, v, k]["transition_guard"])
    #     print("-------->")
    # print(scenario.graph.nodes[chosen_path[-1]]["state"])

    # print(scenario)

    # counter = 0
    # for path in path_builder.paths:
    #     print(path)
    #     counter += 1

    # print(counter)

    # topo_json = to_json(
    #     topology,
    #     option=orjson.OPT_NON_STR_KEYS | orjson.OPT_INDENT_2,
    #     default=default_serializer,
    # )

    # with open("topo_skeleton.json", "w") as jsonfile:
    #     jsonfile.write(topo_json)

    # for device in topology.store.handles:
    #     print(
    #         f"{device}: {topology.store.get(device).name}@{topology.store.get(device).vlan}({topology.store.get(device).vlan_num})"
    #     )
    # import matplotlib.pyplot as plt
    #
    # #
    # colors = set()
    # while len(colors) < topology.store.cardinality() + 1:
    #     colors.add("#" + "".join([random.choice("0123456789ABCDEF") for j in range(6)]))
    # #
    # # # access_graph = graphify_accesses(topology.accesses.elems, topology.store)
    # graph = scenario.graph
    # l = nx.kamada_kawai_layout(graph)
    # # edge_labels = nx.draw_networkx_edge_labels(graph, l)
    # node_colors = [list(colors)[i] for i in map(lambda node: node[1]["state"].device, graph.nodes(data=True))]
    # nx.draw(graph, pos=l, with_labels=True, node_color=node_colors)
    # #
    # plt.show()

    return


async def history_list_entry(args: Namespace):
    from pagan.real_world_examples import historic_scenarios

    print("Historic APTs:")
    for scenario_name in historic_scenarios.keys():
        print("\t" + scenario_name)


async def history_show_entry(args: Namespace):
    campaign_name = args.name
    config_f = historic_configs.get(campaign_name, None)
    scenario_f = historic_scenarios.get(campaign_name, None)

    if config_f is None or scenario_f is None:
        print("Invalid historic scenario")
        exit(1)

    config = await config_f()
    manual_scenario = scenario_f(config)
    print(manual_scenario.graph)
    scenario, paths = generate_scenario(config)

    print(scenario.graph)
    print(manual_scenario.graph)
    await serialize_all(
        config.topology,
        scenario,
        paths,
        config.credential_store,
        config.user_account_store,
        config.enabler_store,
        QuiverStore,
    )

    # # this does not work, manually I can find the APT scenario, but this is slackin'
    # # we should probably just try a 'directed' graph search for the path
    # gm = nx.isomorphism.MultiDiGraphMatcher(
    #     scenario.graph, manual_scenario.graph, node_match=node_match_wrap, edge_match=edge_match_wrap
    # )
    # print(list(gm.subgraph_isomorphisms_iter()))

    return


if __name__ == "__main__":
    __spec__ = None  # this is here for pdb
    colorama_init()
    argparser = create_argparser()
    args = argparser.parse_args()
    try:
        asyncio.run(args.func(args))
    except Exception as e:
        print(
            Fore.RED
            + "Houston, we have bad news:\n"
            + Style.RESET_ALL
            + str(e)
            + Fore.RED
            + "\nExiting."
            + Style.RESET_ALL,
            file=stderr,
        )
        raise e
