from dataclasses import dataclass, field

from pagan.shared.primitives.actions import (
    ActionDescription,
    CapabilitySpecifier,
    UsageScheme,
    PrivilegeOrigin,
    CredentialTarget,
)
from pagan.shared.primitives.capabilities import (
    CapabilityTemplate,
    PrivilegeLevel,
    LimitType,
    CapabilityType,
)
from pagan.shared.primitives.credentials import CredentialType
from pagan.shared.primitives.enablers import (
    ProtoEnabler,
    EnablerLocality,
    EnablerImpact,
)
from pagan.shared.stores.actions import ActionStore
from pagan.shared.primitives.predicate_implementations import (
    Any,
    All,
    Empty,
    Not,
    NotJust,
)
from pagan.shared.primitives.tokens import TokenType
from pagan.shared.primitives.types import ActionHandle

"""
This file contains our generator action framework. The actions are described as predicates, depending on the
attackers skills & capabilities and also creating those. The scheme of representing actions is described
@ActionDescription, here we will comment on the actions themselves.
For our actions, attributing to a phase of an attack is not necessary, however for clarity, we applied a similar
grouping such as MITRE ATT&CK. (But please dont take this division strictly.)
"""


_PaganActionStore = ActionStore()

""" <Discovery>

Actions:
    - discover reachable nodes: an attacker has to use this action to discover other network
      connected devices. Without using this, an attacker is only able to execute actions on the actual machine.
      The attacker requires a session and code execution rights.
    ! other types of accesses are automatically inherited once a session is acquired with the device
"""
_discover_network_reachable_nodes = _PaganActionStore.add(
    ActionDescription(
        name="Discover accessible devices and services",
        aliases=["T1595", "T1592", "T1590"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.ACCESS_VIA_NETWORK),
                required_enabler=None,
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.SERVICE,
                        limit_type=LimitType.MINIMUM,
                    )
                ),
            )
        ],
    )
)
""" </Discovery> """


"""  <Initial Compromise>

    Actions:
        - drop infected media: describes an attacker physically placing an infected media in the infrastructure,
          the attacker only gains an attack artifact, which still needs to be used (a separate action). This action
          should not be used once the attacker is present in the internal network.
        - exploit application: find a running application open* to the network and exploit it,
          the attacker gains privileges of a user in the system (based on service configuration - runas, owner, suid)
          *(if public - this is initial compromise, if internal - lateral movement)
        - exploit authenticated application: similar to the previous, only that the application has its own
          authentication mechanism and can be exploited only after verification
        - drive-by compromise: the victim visits a compromised site and gets infected, the attacker gains a session and
          privileges of the user under who the *browser runs *(rarely other services)
        - phishing with malware: either external (initial compromise) or internal (lateral movement), this action
          represents a malicious email being executed and providing the attacker with a session and user capabilities
        - supply chain compromise: the attacker uses a backdoor originating from third party components
        - valid accounts: an attacker leverages known credentials and badly configured remote access
"""
# _infiltrate_infected_media = _PaganActionStore.add(
#     ActionDescription(
#         name="Drop infected removable media",
#         aliases=[],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.PHYSICAL_ACCESS),
#                 grant=TokenType.combine(TokenType.INFECTED_REMOVABLE_MEDIA),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.ACCESS_INTERNAL_NETWORK),
#             ),
#         ],
#     )
# )

_exploit_open_application = _PaganActionStore.add(
    ActionDescription(
        remote=True,
        name="Exploit Application",
        aliases=[
            "T1190",
        ],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK),
                grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.REMOTE,
                    impact=All(EnablerImpact.ARBITRARY_CODE_EXECUTION),
                ),
                required_capabilities=Empty(),
                privilege_origin=PrivilegeOrigin.SERVICE(),
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK),
                grant=TokenType.combine(TokenType.CAPABILITY),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.REMOTE,
                    impact=All(
                        Not(EnablerImpact.ARBITRARY_CODE_EXECUTION),
                        NotJust(EnablerImpact.ALLOW),
                    ),
                ),
                required_capabilities=Empty(),
                privilege_origin=PrivilegeOrigin.SERVICE(),
                capability_specifier=CapabilitySpecifier.ENABLER,
            ),
        ],
    )
)

_exploit_authed_application = _PaganActionStore.add(
    ActionDescription(
        remote=True,
        name="Exploit Application with auth",
        aliases=[
            "T1190",
        ],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK, TokenType.CREDENTIAL),
                grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.REMOTE,
                    impact=All(EnablerImpact.ARBITRARY_CODE_EXECUTION),
                ),
                required_capabilities=Empty(),
                privilege_origin=PrivilegeOrigin.SERVICE(),
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK, TokenType.CREDENTIAL),
                grant=TokenType.combine(TokenType.CAPABILITY),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.REMOTE,
                    impact=All(
                        Not(EnablerImpact.ARBITRARY_CODE_EXECUTION),
                        NotJust(EnablerImpact.ALLOW),
                    ),
                ),
                required_capabilities=Empty(),
                privilege_origin=PrivilegeOrigin.SERVICE(),
                capability_specifier=CapabilitySpecifier.ENABLER,
            ),
        ],
    )
)

# _drive_by_compromise = _PaganActionStore.add(
#     ActionDescription(
#         remote=True,
#         name="Drive-by Compromise",
#         aliases=[
#             "T1189",
#         ],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.NONE),
#                 grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.REMOTE,
#                     impact=All(EnablerImpact.ARBITRARY_CODE_EXECUTION),
#                 ),
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.ACCESS_INTERNAL_NETWORK),
#                 privilege_origin=PrivilegeOrigin.SERVICE(),
#                 capability_specifier=CapabilitySpecifier.ENABLER,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.NONE),
#                 grant=TokenType.combine(TokenType.CAPABILITY),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.REMOTE,
#                     impact=All(
#                         Not(EnablerImpact.ARBITRARY_CODE_EXECUTION),
#                         NotJust(EnablerImpact.ALLOW),
#                     ),
#                 ),
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.ACCESS_INTERNAL_NETWORK),
#                 privilege_origin=PrivilegeOrigin.SERVICE(),
#                 capability_specifier=CapabilitySpecifier.ENABLER,
#             ),
#         ],
#     )
# )

_phishing_with_malware = _PaganActionStore.add(
    ActionDescription(
        name="Phishing with malware",
        aliases=["T1566"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.EXTERNAL_PHISHING_MAIL),
                grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
                required_enabler=ProtoEnabler(locality=EnablerLocality.BOTH, impact=Any(EnablerImpact.ALLOW)),
                required_capabilities=Empty(),
                except_if=Any(TokenType.ACCESS_INTERNAL_NETWORK, TokenType.ACCESS_DMZ_SEGMENT),
                privilege_origin=PrivilegeOrigin.RANDOM_USER(),
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.INTERNAL_PHISHING_MAIL),
                grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
                required_enabler=ProtoEnabler(locality=EnablerLocality.BOTH, impact=Any(EnablerImpact.ALLOW)),
                required_capabilities=Empty(),
                privilege_origin=PrivilegeOrigin.RANDOM_USER(),
            ),
        ],
    )
)

# _supply_chain_compromise = _PaganActionStore.add(
#     ActionDescription(
#         remote=True,
#         name="Supply chain compromise",
#         aliases=["T1200", "T1090", "T1195"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.PRE_ATTACK_BACKDOOR),
#                 grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.ACCESS_INTERNAL_NETWORK),
#                 privilege_origin=PrivilegeOrigin.SERVICE(),
#             )
#         ],
#     )
# )


_valid_accounts = _PaganActionStore.add(
    ActionDescription(
        name="Valid Accounts",
        aliases=["T1133", "T1078", "T1021"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.CREDENTIAL, TokenType.ACCESS_VIA_NETWORK),
                # credential is OS credential -> this action will be only allowed on services like ssh, rdp, ...
                grant=TokenType.combine(TokenType.CAPABILITY_ALL, TokenType.SESSION),
                required_enabler=None,
                required_capabilities=Empty(),
                privilege_origin=PrivilegeOrigin.CREDENTIAL(),
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.CREDENTIAL, TokenType.PHYSICAL_ACCESS),
                # credential is OS credential -> this action will be only allowed on services like ssh, rdp, ...
                grant=TokenType.combine(TokenType.CAPABILITY_ALL, TokenType.SESSION),
                required_enabler=None,
                required_capabilities=Empty(),
                privilege_origin=PrivilegeOrigin.CREDENTIAL(),
            ),
            UsageScheme(
                given=TokenType.combine(
                    TokenType.CREDENTIAL,
                    TokenType.CREDENTIAL_2FA_TOKEN,
                    TokenType.ACCESS_VIA_NETWORK,
                ),
                grant=TokenType.combine(TokenType.CAPABILITY_ALL, TokenType.SESSION),
                required_enabler=None,
                required_capabilities=Empty(),
                privilege_origin=PrivilegeOrigin.CREDENTIAL(),
                except_if=Any(TokenType.NO_MFA_ALLOWED),
            ),
            UsageScheme(  # for example anonymous smb
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK),
                grant=TokenType.combine(TokenType.CAPABILITY, TokenType.SESSION),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.REMOTE,
                    impact=All(NotJust(EnablerImpact.ALLOW), All(EnablerImpact.ALLOW, EnablerImpact.DATA_DISCLOSURE)),
                ),
                required_capabilities=Empty(),
                capability_specifier=CapabilitySpecifier.ENABLER,
                privilege_origin=PrivilegeOrigin.ANONYMOUS_LOGIN(),
            ),
            # we need to find a way to represent if a user needs 2FA
        ],
    )
)
"""  </Initial Compromise> """


"""  <Persistence>
 These actions are at the moment side-goals for the attacker, as the provided `PERSIST` capability is never
 required for another action as input. (might change in the furture).

 Actions:
    - persist with backdoor: the attacker schedules / injects processes that survive reboots
    - persist with user: an attacker creates a user account so he can pivot back to this machine in the future
"""
# _persist_plant_backdoor = _PaganActionStore.add(
#     ActionDescription(
#         name="Plant backdoor for persistence",
#         aliases=["T1053", "T1547", "T1037"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSIST_LOG_OUT,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 privilege_origin=PrivilegeOrigin.INHERIT_USER(),
#                 capability_specifier=CapabilitySpecifier.ACTION,
#             )
#         ],
#     )
# )

# _persist_create_account = _PaganActionStore.add(
#     ActionDescription(
#         name="Plant user for persistence",
#         aliases=[
#             "T1098",
#         ],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(
#                     TokenType.CAPABILITY_ALL
#                 ),  # we immediately get the privs of the created account
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCOUNT_MANIPULATION_USER_ADD,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 privilege_origin=PrivilegeOrigin.CREATED_USER(),
#             )
#         ],
#     )
# )
""" </Persistence> """


""" <Privilege escalation> """
"""_privilege_escalation = _PaganActionStore.add(
    ActionDescription(
        name="Privilege escalation",
        aliases=[],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CAPABILITY),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.LOCAL, impact=All(EnablerImpact.ALLOW)
                ),
                # privesc template, actually here we would want not just exploits but vulns, misconfigurations, etc.
                # the actual technique will also depend on the input and output privileges
                required_capabilities=[
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.SERVICE,
                        limit_type=LimitType.MINIMUM,
                    )
                ],
                privilege_origin=PrivilegeOrigin.SERVICE,
            )
        ],
    )
)"""

_privesc_suid = _PaganActionStore.add(
    ActionDescription(
        name="Abuse Elevation Control Mechanism: Setuid and Setgid",
        aliases=["T1548.001"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CAPABILITY),
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.SERVICE,
                        limit_type=LimitType.MINIMUM,
                    )
                ),
                required_enabler=ProtoEnabler(locality=EnablerLocality.LOCAL, impact=All(EnablerImpact.ALLOW)),
                privilege_origin=PrivilegeOrigin.SERVICE(),
                capability_specifier=CapabilitySpecifier.SERVICE,
            )
        ],
    )
)

# _privesc_uac_bypass_com = _PaganActionStore.add(
#     ActionDescription(
#         name="Abuse Elevation Control Mechanism: Bypass User Account Control",
#         aliases=["T1548.002"],
#         uses=[
#             # usages are based on the https://github.com/hfiref0x/UACME techniques (click "Keys" in readme)
#             # case A: application shimming requires admin priv to create shim, aka no effective privesc
#             # case B: Dll Hijack -> see component hijack on filesys
#             # case C: shell API - again depends on a high priv or autoElevating process to grab data/run
#             #  command from user controlled registry/env -> see component hijack path
#             # case D: Exposed elevated COM interface - the only standalone
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.UAC_ELEVATE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.EXACT,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.EXACT,
#                     ),
#                 ),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL,
#                     impact=Any(
#                         EnablerImpact.ARBITRARY_CODE_EXECUTION,
#                         EnablerImpact.DATA_TAMPERING,
#                         EnablerImpact.CHANGE_MACHINE_CONFIGURATION,
#                     ),
#                 ),
#                 privilege_origin=PrivilegeOrigin.ELEVATE_SPLIT_TOKEN(),
#                 capability_specifier=CapabilitySpecifier.ENABLER,
#             ),
#         ],
#     )
# )

_privesc_tamper_sudoers = _PaganActionStore.add(
    ActionDescription(
        name="Abuse Elevation Control Mechanism: Sudoers",
        aliases=["T1548.003"],
        uses=[
            # Case A & B: user has sudo privileges without password prompt
            # or tty isolation is turned off and attacker is lucky
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CAPABILITY),
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.EXACT,
                    )
                ),
                privilege_origin=PrivilegeOrigin.ELEVATE_SPLIT_TOKEN(),
                capability_specifier=CapabilitySpecifier.ENABLER,
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.LOCAL,
                    impact=All(All(EnablerImpact.ALLOW), NotJust(EnablerImpact.ALLOW)),
                ),
            ),
            # Case C: attacker can tamper with the sudoers file
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CAPABILITY_ALL),
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.EXACT,
                    ),
                ),
                privilege_origin=PrivilegeOrigin.FIXED(PrivilegeLevel.LOCAL_SYSTEM),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.LOCAL,
                    impact=All(EnablerImpact.ALLOW),
                ),
            ),
        ],
    )
)

# _privesc_grab_security_token = _PaganActionStore.add(
#     ActionDescription(
#         name="Access Token Manipulation: Theft, Impersonation",
#         aliases=["T1134.001", "T1134.002", "T1134.003"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.EXACT,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.SE_CREATE_SECURITY_TOKEN,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.EXACT,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.SE_TOKEN_IMPERSONATE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.EXACT,
#                     ),
#                 ),
#                 required_enabler=None,
#                 privilege_origin=PrivilegeOrigin.FIXED(
#                     PrivilegeLevel.LOCAL_SYSTEM
#                 ),  # the one we steal from - basically SYSTEM, otherwise no reason doing it
#             ),
#             # once we differentiate logged-in and inactive users
#             # UsageScheme(
#             #     given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY, TokenType.CREDENTIAL),
#             #     grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#             #     required_capabilities=[
#             #         CapabilityTemplate(
#             #             to=CapabilityType.EXECUTE_CODE,
#             #             privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#             #             limit_type=LimitType.EXACT,
#             #         ),
#             #         CapabilityTemplate(
#             #             to=CapabilityType.SE_CREATE_SECURITY_TOKEN,
#             #             privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#             #             limit_type=LimitType.EXACT,
#             #         ),
#             #         CapabilityTemplate(
#             #             to=CapabilityType.SE_TOKEN_IMPERSONATE,
#             #             privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#             #             limit_type=LimitType.EXACT,
#             #         ),
#             #     ],
#             #     required_enabler=None,
#             #     privilege_origin=PrivilegeOrigin.CREDENTIAL,
#             # )
#         ],
#     )
# )

# _privesc_parent_pid_spoof = _PaganActionStore.add(
#     ActionDescription(
#         name="Access Token Manipulation: Parent PID Spoofing",
#         aliases=["T1134.004"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.EXACT,
#                     )
#                 ),
#                 required_enabler=None,
#                 privilege_origin=PrivilegeOrigin.FIXED(PrivilegeLevel.LOCAL_SYSTEM),  # parent
#             )
#         ],
#     )
# )

# _privesc_autoexec = _PaganActionStore.add(
#     ActionDescription(
#         name="Privileged Autoexecution",
#         aliases=["T1547"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=Any(
#                     Any(
#                         CapabilityTemplate(
#                             to=CapabilityType.CONFIG_LOCAL_MACHINE,
#                             privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                             limit_type=LimitType.MINIMUM,
#                         ),
#                         CapabilityTemplate(
#                             to=CapabilityType.EXECUTE_CODE,
#                             privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                             limit_type=LimitType.MINIMUM,
#                         ),
#                     ),
#                     All(
#                         CapabilityTemplate(
#                             to=CapabilityType.ACCESS_FILESYS_ELEVATED,
#                             privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                             limit_type=LimitType.MINIMUM,
#                         ),
#                         CapabilityTemplate(
#                             to=CapabilityType.FILESYS_WRITE,
#                             privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                             limit_type=LimitType.MINIMUM,
#                         ),
#                     ),
#                 ),
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 required_enabler=None,
#                 privilege_origin=PrivilegeOrigin.FIXED(PrivilegeLevel.LOCAL_SYSTEM),
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL, impact=All(EnablerImpact.ALLOW)
#                 ),
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 privilege_origin=PrivilegeOrigin.ADVERSARY_CHOICE(),
#             ),
#         ],
#     )
# )

_privesc_services = _PaganActionStore.add(
    ActionDescription(
        name="Create or Modify System Process",
        aliases=["T1543"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CAPABILITY_ALL),
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.CONFIG_LOCAL_MACHINE,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                    ),
                ),
                required_enabler=None,
                provided_capabilities=[
                    CapabilityTemplate(
                        to=CapabilityType.PERSISTENCE,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.MINIMUM,
                    )
                ],
                privilege_origin=PrivilegeOrigin.ADVERSARY_CHOICE(),
            ),
        ],
    )
)

# _privesc_accessibility_ifeo = _PaganActionStore.add(
#     ActionDescription(
#         name="Event Triggered Execution: Accessibility Features, Image File Execution Options Injection",
#         aliases=["T1546.012", "T1546.008"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.CONFIG_LOCAL_MACHINE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.EXACT,
#                     ),
#                 ),
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 required_enabler=None,
#                 privilege_origin=PrivilegeOrigin.FIXED(PrivilegeLevel.LOCAL_SYSTEM),
#             )
#         ],
#     )
# )

_privesc_exploit = _PaganActionStore.add(
    ActionDescription(
        name="Exploitation for Privilege Escalation",
        aliases=["T1068"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CAPABILITY),
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.SERVICE,
                        limit_type=LimitType.MINIMUM,
                    )
                ),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.LOCAL,
                    impact=Any(
                        EnablerImpact.ARBITRARY_CODE_EXECUTION,
                        EnablerImpact.DATA_TAMPERING,
                        EnablerImpact.CHANGE_MACHINE_CONFIGURATION,
                    ),
                ),
                privilege_origin=PrivilegeOrigin.SERVICE(),
                capability_specifier=CapabilitySpecifier.ENABLER,
            )
        ],
    )
)

# _privesc_byovd = _PaganActionStore.add(
#     ActionDescription(
#         name="Exploitation for Privilege Escalation - Bring Your Own Vulnerable Driver",
#         aliases=["T1068"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.SE_LOAD_DRIVER,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.EXACT,
#                     ),
#                 ),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL,
#                     impact=Any(
#                         EnablerImpact.ARBITRARY_CODE_EXECUTION,
#                         EnablerImpact.DATA_TAMPERING,
#                         EnablerImpact.CHANGE_MACHINE_CONFIGURATION,
#                     ),
#                 ),
#                 privilege_origin=PrivilegeOrigin.FIXED(PrivilegeLevel.LOCAL_SYSTEM),
#                 capability_specifier=CapabilitySpecifier.ENABLER,
#             )
#         ],
#     )
# )

# _privesc_process_injection = _PaganActionStore.add(
#     ActionDescription(
#         name="Process Injection",
#         aliases=["T1055", "CAPEC-640"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.SE_DEBUG,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 required_enabler=None,
#                 privilege_origin=PrivilegeOrigin.SERVICE(),
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL,
#                     impact=All(
#                         EnablerImpact.ARBITRARY_CODE_EXECUTION
#                     ),  # user who has privileged SE_DEBUG
#                 ),
#                 privilege_origin=PrivilegeOrigin.SERVICE(),
#             ),
#         ],
#     )
# )

# _privesc_component_hijack = _PaganActionStore.add(
#     ActionDescription(
#         name="Hijacking Components (Dlls, executables) on the filesystem, Unquoted component paths",
#         aliases=["T1574.001", "T1574.002", "T1574.008", "T1574.009", "T1574.005"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_ELEVATED,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_WRITE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 required_enabler=None,
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 privilege_origin=PrivilegeOrigin.FIXED(PrivilegeLevel.LOCAL_SYSTEM),
#             ),
#             UsageScheme(  # user has to wait for admin/system to launch the process -- admin also if no code exec
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MAXIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_WRITE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL,
#                     impact=All(EnablerImpact.ARBITRARY_CODE_EXECUTION),
#                     # user with write access to sys folders or unquoted path with a component under user control, non-absolute import paths,
#                     #  bad folder permissions
#                 ),
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 privilege_origin=PrivilegeOrigin.SERVICE(),
#             ),
#         ],
#     )
# )

# _privesc_search_order_tampering = _PaganActionStore.add(
#     ActionDescription(
#         name="Hijack component path or search order, AppCert and AppInit DLL spoofing",
#         aliases=[
#             "T1574.006",
#             "T1574.007",
#             "T1574.001",
#             "T1574.011",
#             "T1546.009",
#             "T1546.010",
#         ],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.CONFIG_LOCAL_MACHINE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_WRITE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 required_enabler=None,
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 privilege_origin=PrivilegeOrigin.FIXED(PrivilegeLevel.LOCAL_SYSTEM),
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CAPABILITY_ALL),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.CONFIG_LOCAL_USER,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.EXACT,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_WRITE,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL,
#                     impact=All(EnablerImpact.ARBITRARY_CODE_EXECUTION),
#                     # system proc/task taking path from user env/registry, bad env/registry acl
#                 ),
#                 provided_capabilities=[
#                     CapabilityTemplate(
#                         to=CapabilityType.PERSISTENCE,
#                         privilege_limit=PrivilegeLevel.SERVICE,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ],
#                 privilege_origin=PrivilegeOrigin.FIXED(PrivilegeLevel.LOCAL_SYSTEM),
#             ),
#         ],
#     )
# )
""" </Privilege escalation> """

""" <Credential access>

    Actions:
        - phishing: internal or external - internal needs mail creation action beforehand (see later)

            Note: Phishing should be allowed for services, where we expect that they will be mimiced, not for 
                services acting as the middle man (e.g. mail).

        - from OS vault: an adversary with local admin or higher privileges can dump clear or hashed credentials for
          users, even domain login from registries, the precise sub techniques is given by the attackers capability
          level
        - bruteforce: straightforward
        - forced authentication: the attacker mimics UAC or similar auth methods to harvest credentials
        - unsecured creds: anything stored in a cleartext file, etc.
        - input capture: aka keylogging
        - exploit service: a service has a security hole through which credentials can be leaked for users of that service
        - exploit 2fa: for TOTP and HOTP applications
        - steal 2fa: access users mail and steal it
"""
# _credential_access_phish = _PaganActionStore.add(
#     ActionDescription(
#         name="Credential phishing",
#         aliases=[],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.EXTERNAL_PHISHING_MAIL),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.ACCESS_INTERNAL_NETWORK),
#                 credential_target=CredentialTarget.SERVICE_SELF,
#                 credential_type=CredentialType.APP_PRIMARY,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.EXTERNAL_PHISHING_MAIL),
#                 grant=TokenType.combine(TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.ACCESS_INTERNAL_NETWORK, TokenType.NO_MFA_ALLOWED),
#                 credential_target=CredentialTarget.SERVICE_SELF,
#                 credential_type=CredentialType.APP_ANY_PASSABLE,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.INTERNAL_PHISHING_MAIL),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 credential_target=CredentialTarget.SERVICE_SELF,
#                 credential_type=CredentialType.APP_PRIMARY,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.INTERNAL_PHISHING_MAIL),
#                 grant=TokenType.combine(TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.NO_MFA_ALLOWED),
#                 credential_target=CredentialTarget.SERVICE_SELF,
#                 credential_type=CredentialType.APP_ANY_PASSABLE,
#             ),
#         ],
#     )
# )

# _credential_access_os_cached = _PaganActionStore.add(
#     ActionDescription(
#         name="Credential Access from LSASS",  # LSASS dumping
#         aliases=["T1555"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL, impact=All(EnablerImpact.ALLOW)
#                 ),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 credential_target=CredentialTarget.OS_CACHED,
#                 credential_type=CredentialType.OS,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#                 credential_target=CredentialTarget.OS_CACHED,
#                 credential_type=CredentialType.OS,
#             ),
#         ],
#     )
# )  # in reality, we get hashes; cracking them tho is a side action

# _credential_access_os_vault = _PaganActionStore.add(
#     ActionDescription(
#         name="Credential Access from OS vault",  # SAM db
#         aliases=["T1555"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=Any(
#                     CapabilityTemplate(
#                         to=CapabilityType.CONFIG_LOCAL_MACHINE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 credential_target=CredentialTarget.OS_ALL_LOCAL,
#                 credential_type=CredentialType.OS,
#             ),
#         ],
#     )
# )  # in reality, we get hashes; cracking them tho is a side action

# _credential_access_os_file = _PaganActionStore.add(
#     ActionDescription(
#         name="Credential Access from OS file",  # aka. reading /etc/shadow
#         aliases=["T1555"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL, impact=All(EnablerImpact.ALLOW)
#                 ),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_ELEVATED,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 credential_target=CredentialTarget.OS_ALL_LOCAL,
#                 credential_type=CredentialType.OS,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_ELEVATED,
#                         privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 credential_target=CredentialTarget.OS_ALL_LOCAL,
#                 credential_type=CredentialType.OS,
#             ),
#         ],
#     )
# )  # in reality, we get hashes; cracking them tho is a side action

_credential_access_bruteforce = _PaganActionStore.add(
    ActionDescription(
        name="Bruteforce passwords",
        aliases=["T1110"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK),
                grant=TokenType.combine(TokenType.CREDENTIAL),
                required_enabler=ProtoEnabler(locality=EnablerLocality.BOTH, impact=Any(EnablerImpact.ALLOW)),
                required_capabilities=Empty(),
                credential_target=CredentialTarget.SERVICE_SELF,
                credential_type=CredentialType.ANY_PRIMARY,
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK),
                grant=TokenType.combine(
                    TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN
                ),  # questionable if 2fa bruteforce is possible
                required_enabler=None,
                required_capabilities=Empty(),
                except_if=Any(TokenType.NO_MFA_ALLOWED),
                credential_target=CredentialTarget.SERVICE_SELF,
                credential_type=CredentialType.APP_ANY_PASSABLE,
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CREDENTIAL),
                required_enabler=None,  # in a way we could add enablers representing lockout, guess limitation settings
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.SERVICE,
                        limit_type=LimitType.MINIMUM,
                    )
                ),
                credential_target=CredentialTarget.SERVICE_SELF,
                credential_type=CredentialType.ANY_PRIMARY,
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN),
                required_enabler=None,
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.SERVICE,
                        limit_type=LimitType.MINIMUM,
                    )
                ),
                except_if=Any(TokenType.NO_MFA_ALLOWED),
                credential_target=CredentialTarget.SERVICE_SELF,
                credential_type=CredentialType.APP_ANY_PASSABLE,
            ),
            # nuh, leave these for now
            # UsageScheme(
            #     given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK, TokenType.CREDENTIAL),
            #     grant=TokenType.combine(TokenType.CREDENTIAL_2FA_TOKEN),
            #     required_enabler=None,  # or maybe allow -> e.g bad rate limitation/lockout
            #     required_capabilities=Empty(),
            #     except_if=Any(TokenType.NO_MFA_ALLOWED),
            #     credential_target=CredentialTarget.MIMIC_ALL,
            #     credential_type=CredentialType.MFA,
            # ),
            # UsageScheme(
            #     given=TokenType.combine(
            #         TokenType.SESSION, TokenType.CAPABILITY, TokenType.CREDENTIAL
            #     ),
            #     grant=TokenType.combine(TokenType.CREDENTIAL_2FA_TOKEN),
            #     required_enabler=None,  # in a way we could add enablers representing lockout, guess limitation settings
            #     required_capabilities=All(
            #         CapabilityTemplate(
            #             to=CapabilityType.EXECUTE_CODE,
            #             privilege_limit=PrivilegeLevel.SERVICE,
            #             limit_type=LimitType.MINIMUM,
            #         )
            #     ),
            #     except_if=Any(TokenType.NO_MFA_ALLOWED)
            #     credential_target=CredentialTarget.MIMIC_ALL,
            #     credential_type=CredentialType.MFA,
            # ),
        ],
    )
)

# _credential_access_forced_auth = _PaganActionStore.add(
#     ActionDescription(
#         name="Forced Auth",
#         aliases=["T1187"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 credential_target=CredentialTarget.OS_ACTIVE_ACCOUNT,
#                 credential_type=CredentialType.OS,
#             ),
#             # NO 2FA for os ATM
#             # UsageScheme(
#             #     given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#             #     grant=TokenType.combine(TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN),
#             #     required_enabler=None,
#             #     required_capabilities=All(
#             #         CapabilityTemplate(
#             #             to=CapabilityType.EXECUTE_CODE,
#             #             privilege_limit=PrivilegeLevel.USER,
#             #             limit_type=LimitType.MINIMUM,
#             #         )
#             #     ),
#             #     except_if=Any(TokenType.NO_MFA_ALLOWED)
#             #     credential_target=CredentialTarget.OS_ACTIVE_ACCOUNT,
#             # ),
#         ],
#     )
# )

# _credential_access_unsecured_creds = _PaganActionStore.add(
#     ActionDescription(
#         name="Unsecured creds",
#         aliases=["T1552"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.EXACT,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.EXACT,
#                     ),
#                 ),
#                 credential_target=CredentialTarget.ANY_ACTUAL_USER,
#                 credential_type=CredentialType.UNSPEC,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.EXACT,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.EXACT,
#                     ),
#                 ),
#                 except_if=Any(TokenType.NO_MFA_ALLOWED),
#                 credential_target=CredentialTarget.ANY_ACTUAL_USER,
#                 credential_type=CredentialType.UNSPEC,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 credential_target=CredentialTarget.ANY_PRESENT_USERS,
#                 credential_type=CredentialType.UNSPEC,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 except_if=Any(TokenType.NO_MFA_ALLOWED),
#                 credential_target=CredentialTarget.ANY_PRESENT_USERS,
#                 credential_type=CredentialType.UNSPEC,
#             ),
#         ],
#     )
# )

# _credential_access_input_capture = _PaganActionStore.add(
#     ActionDescription(
#         name="Input Capture",
#         aliases=["T1056"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#                 credential_target=CredentialTarget.ANY_ACTUAL_USER,
#                 credential_type=CredentialType.UNSPEC,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#                 except_if=Any(TokenType.NO_MFA_ALLOWED),
#                 credential_target=CredentialTarget.ANY_ACTUAL_USER,
#                 credential_type=CredentialType.UNSPEC,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#                 credential_target=CredentialTarget.ANY_PRESENT_USERS,
#                 credential_type=CredentialType.UNSPEC,
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL, TokenType.CREDENTIAL_2FA_TOKEN),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#                 except_if=Any(TokenType.NO_MFA_ALLOWED),
#                 credential_target=CredentialTarget.ANY_PRESENT_USERS,
#                 credential_type=CredentialType.UNSPEC,
#             ),
#         ],
#     )
# )

_credential_access_exploit_local_service = _PaganActionStore.add(
    ActionDescription(
        name="exploit local service for credentials",
        aliases=["T1212"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.CREDENTIAL),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.LOCAL,
                    impact=All(EnablerImpact.CREDENTIAL_LEAK),
                ),
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.MINIMUM,
                    )
                ),
                credential_target=CredentialTarget.ANY_ACTUAL_USER,  # aka exploiting password vault or so
                credential_type=CredentialType.UNSPEC,
            ),
        ],
    )
)

_credential_access_exploit_remote_service = _PaganActionStore.add(
    ActionDescription(
        remote=True,
        name="exploit remote service for credentials",
        aliases=["T1212"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK),
                grant=TokenType.combine(TokenType.CREDENTIAL),
                required_enabler=ProtoEnabler(
                    locality=EnablerLocality.REMOTE,
                    impact=All(EnablerImpact.CREDENTIAL_LEAK),
                ),
                required_capabilities=Empty(),
                credential_target=CredentialTarget.SERVICE_SELF_ALL,
                credential_type=CredentialType.ANY_PRIMARY,
            ),
        ],
    )
)

# _credential_access_exploit_2fa = _PaganActionStore.add(
#     ActionDescription(
#         name="exploit for 2FA credentials",
#         aliases=["T1212"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.CREDENTIAL_2FA_TOKEN),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.BOTH,
#                     impact=All(EnablerImpact.CREDENTIAL_LEAK),
#                 ),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#                 except_if=Any(TokenType.NO_MFA_ALLOWED),
#                 credential_target=CredentialTarget.SERVICE_OTHER,
#                 credential_type=CredentialType.MFA,
#             ),
#         ],
#     )
# )

# _credential_access_2fa_service_use = _PaganActionStore.add(
#     ActionDescription(
#         name="read 2FA credentials from authed service",
#         aliases=["T1111"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK, TokenType.CREDENTIAL),
#                 grant=TokenType.combine(TokenType.CREDENTIAL_2FA_TOKEN),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.NO_MFA_ALLOWED),
#                 credential_target=CredentialTarget.MIMIC_USER,
#                 credential_type=CredentialType.MFA,
#             ),
#         ],
#     )
# )
""" </Credential access> """


"""  <Lateral Movement>
 There is no division in MITRE ATT&CK, however most lateral movement actions have to phases:
 1. the attacker creates an artifact
 2. the artifact gets used
 In this framework we split the group to those two phases. This also helps with mapping the actions to devices, we will
 precisely see where the artifact is created and used in our graph representation.

    Preparation Actions:
        - taint shared content: the attacker maliciously modifies files/programs shared between different devices
        - infect removable media: the attacker executes code that infects removable media
        - create internal phishing mail (as client): the attacker impersonates a legitimatre user and crafts a mail
          in their name
        - create internal phishing mail as service: the attacker leverages an exploit in the mail service (exchange ...)
          to impersonate legitimate users
        - create domain configuration object: the attacker leverages domain configuration primitives to schedule
          execution on any device(domain)
Movement Actions:
        - For each prepared artifact type, a matching use of that artifact.
        - Escape to host: a special lateral movement action for escaping containers, only available if the device
          is marked as container (ISOLATED token), must exploit isolation holes
        - some of the initial access techniques  are also lateral movements, see hierarchy below (class Movement)
"""
# _taint_shared_content = _PaganActionStore.add(
#     ActionDescription(
#         name="Create Tainted shared content",
#         aliases=["T1080-A"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.TAINTED_SHARED_CONTENT),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_SHARED,  # access filesys shared,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_WRITE,  # access filesys shared,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#             ),
#         ],
#     )
# )

# _infect_removable_media = _PaganActionStore.add(
#     ActionDescription(
#         name="Infect removable media",
#         aliases=["T1091-A"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.INFECTED_REMOVABLE_MEDIA),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_WRITE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#                 except_if=Any(TokenType.NO_USB_ACCESS),
#             ),
#         ],
#     )
# )

_create_internal_phishing_mail = _PaganActionStore.add(
    ActionDescription(
        name="Create internal phishing mail",
        aliases=["T1534-A"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.INTERNAL_PHISHING_MAIL),
                required_enabler=None,
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.IMPERSONATE,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.MINIMUM,
                    )
                ),
            ),
        ],
    )
)

#  only applicable on the email server
_create_internal_phishing_mail_email_srv = _PaganActionStore.add(
    ActionDescription(
        name="Create internal phishing mail",
        aliases=["T1534-A"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.CAPABILITY, TokenType.SESSION),
                grant=TokenType.combine(TokenType.INTERNAL_PHISHING_MAIL),
                required_enabler=ProtoEnabler(locality=EnablerLocality.LOCAL, impact=All(EnablerImpact.ALLOW)),
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.SERVICE,
                        limit_type=LimitType.MINIMUM,
                    )
                ),
            ),
        ],
    )
)

# _create_domain_configuration_object = _PaganActionStore.add(
#     ActionDescription(
#         name="Create Domain Configuration Object",
#         aliases=[],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.MALICIOUS_DOMAIN_CONFIGURATION_OBJECT),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.CONTROL_DOMAIN,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#             )
#         ],
#     )
# )


# _move_via_infected_media = _PaganActionStore.add(
#     ActionDescription(
#         name="Infected media runs",
#         aliases=["T1091-B"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(
#                     TokenType.INFECTED_REMOVABLE_MEDIA,
#                     TokenType.ACCESS_VIA_REMOVABLE_MEDIA,
#                 ),
#                 grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.LOCAL,
#                     impact=All(EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION),
#                 ),
#                 required_capabilities=Empty(),
#                 except_if=Any(TokenType.NO_USB_ACCESS),
#                 privilege_origin=PrivilegeOrigin.RANDOM_USER(),
#             ),
#         ],
#     )
# )

# _move_with_tainted_content = _PaganActionStore.add(
#     ActionDescription(
#         name="Tainted shared content beacon",
#         aliases=["T1080-B"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(
#                     TokenType.TAINTED_SHARED_CONTENT,
#                     TokenType.ACCESS_VIA_SHARED_FILESYS,
#                 ),
#                 grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 privilege_origin=PrivilegeOrigin.RANDOM_USER(),
#             ),
#         ],
#     )
# )

# _move_with_domain_account = _PaganActionStore.add(
#     ActionDescription(
#         name="Lateral with domain account",
#         aliases=["T1078"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.CAPABILITY, TokenType.ACCESS_VIA_NETWORK),
#                 grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.DOMAIN_LOGIN,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#                 privilege_origin=PrivilegeOrigin.INHERIT_USER(),
#             )
#         ],
#     )
# )

# _move_with_domain_config_object = _PaganActionStore.add(
#     ActionDescription(
#         name="Lateral with Domain Configuration Object",
#         aliases=[],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.MALICIOUS_DOMAIN_CONFIGURATION_OBJECT),
#                 grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
#                 required_enabler=None,
#                 required_capabilities=Empty(),
#                 privilege_origin=PrivilegeOrigin.ADVERSARY_CHOICE(),
#             )
#         ],
#     )
# )

# _escape_to_host = _PaganActionStore.add(  # TODO check if tokens here work as you assumed at first
#     ActionDescription(
#         name="Escape to host",
#         aliases=["T1611"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(
#                     TokenType.ACCESS_VIA_NETWORK,
#                     TokenType.CAPABILITY,
#                     TokenType.CONTAINERIZED,
#                 ),
#                 grant=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY_ALL),
#                 required_enabler=ProtoEnabler(
#                     locality=EnablerLocality.REMOTE,
#                     impact=All(EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION),
#                 ),
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#                 privilege_origin=PrivilegeOrigin.SERVICE(),
#             )
#         ],
#     )
# )
"""  </Lateral Movement> """


"""  <Impact, Collection, Exfiltration>
 3 MITRE ATT&CK phases merged and cleaned.

    Actions:
        - data collection: the attacker gathers dynamically generated data, emails, screen capture, keylogging etc.
        - data exfiltration: the attacker steals either collected data or static data
        - data manipulation: ransomware, changing data, etc.
        - disk wipe: on partition level, high privileges needed
        - shutdown
        - stop service
        - remove user account
        - DoS/overwhelm: simply by sending too much data/requests
"""
# _data_collection = _PaganActionStore.add(
#     ActionDescription(
#         name="Data collection",
#         aliases=["T1005"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.DATA),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.DATA),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_ELEVATED,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.DATA),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_SHARED,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.EXECUTE_CODE,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#             ),
#         ],
#     )
# )

# _data_exfiltration = _PaganActionStore.add(
#     ActionDescription(
#         name="Data exfiltration",
#         aliases=["TA0010"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.NONE),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_GENERIC,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff - we dont give generic access to services
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff - we dont give generic access to services
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.NONE),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_ELEVATED,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#             ),
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.NONE),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_SHARED,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_READ,
#                         privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#             ),
#         ],
#     )
# )

_data_manipulation = _PaganActionStore.add(
    ActionDescription(
        name="Data Manipulation",
        aliases=["T1565", "T1845", "T1846"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.NONE),
                required_enabler=None,
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.ACCESS_FILESYS_GENERIC,
                        privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
                        limit_type=LimitType.MINIMUM,
                    ),
                    CapabilityTemplate(
                        to=CapabilityType.FILESYS_WRITE,
                        privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
                        limit_type=LimitType.MINIMUM,
                    ),
                ),
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.NONE),
                required_enabler=None,
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.ACCESS_FILESYS_ELEVATED,
                        privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
                        limit_type=LimitType.MINIMUM,
                    ),
                    CapabilityTemplate(
                        to=CapabilityType.FILESYS_WRITE,
                        privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
                        limit_type=LimitType.MINIMUM,
                    ),
                ),
            ),
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.NONE),
                required_enabler=None,
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.ACCESS_FILESYS_SHARED,  # Access filesys shared
                        privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
                        limit_type=LimitType.MINIMUM,
                    ),
                    CapabilityTemplate(
                        to=CapabilityType.FILESYS_WRITE,  # Access filesys shared
                        privilege_limit=PrivilegeLevel.USER,  # no reason doing this with lower stuff
                        limit_type=LimitType.MINIMUM,
                    ),
                ),
            ),
        ],
    )
)

# _disk_wipe = _PaganActionStore.add(
#     ActionDescription(
#         name="Wipe disk",
#         aliases=["T1561"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.IMPACT_DENIAL_OF_SERVICE),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCESS_FILESYS_ELEVATED,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                     CapabilityTemplate(
#                         to=CapabilityType.FILESYS_WRITE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     ),
#                 ),
#             )
#         ],
#     )
# )

# _shutdown = _PaganActionStore.add(
#     ActionDescription(
#         name="shutdown",
#         aliases=["T1529"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.IMPACT_DENIAL_OF_SERVICE),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.SHUTDOWN,
#                         privilege_limit=PrivilegeLevel.USER,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#             )
#         ],
#     )
# )


_stop_service = _PaganActionStore.add(
    ActionDescription(
        name="stop_service",
        aliases=["T1489"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
                grant=TokenType.combine(TokenType.IMPACT_DENIAL_OF_SERVICE),
                required_enabler=None,
                required_capabilities=All(
                    CapabilityTemplate(
                        to=CapabilityType.EXECUTE_CODE,
                        privilege_limit=PrivilegeLevel.USER,  # actually whoever runs/owns the process, given by goal
                        limit_type=LimitType.MINIMUM,
                    )
                ),
            )
        ],
    )
)

# _account_removal = _PaganActionStore.add(
#     ActionDescription(
#         name="remove account",
#         aliases=["T1531"],
#         uses=[
#             UsageScheme(
#                 given=TokenType.combine(TokenType.SESSION, TokenType.CAPABILITY),
#                 grant=TokenType.combine(TokenType.IMPACT_DENIAL_OF_SERVICE),
#                 required_enabler=None,
#                 required_capabilities=All(
#                     CapabilityTemplate(
#                         to=CapabilityType.ACCOUNT_MANIPULATION_USER_DELETE,
#                         privilege_limit=PrivilegeLevel.ADMINISTRATOR,
#                         limit_type=LimitType.MINIMUM,
#                     )
#                 ),
#             )
#         ],
#     )
# )

_overwhelm = _PaganActionStore.add(
    ActionDescription(
        remote=True,
        name="Overwhelm service for DoS",
        aliases=["T1498", "T1496"],
        uses=[
            UsageScheme(
                given=TokenType.combine(TokenType.ACCESS_VIA_NETWORK),
                grant=TokenType.combine(TokenType.IMPACT_DENIAL_OF_SERVICE),
                required_enabler=None,
                required_capabilities=Empty(),
            )
        ],
    )
)
"""  </Impact, Collection, Exfiltration> """

_PaganActionStore.block()  # so the store can not be changed if imported (disables `add()`)


"""
The following classes simply represent a hierarchy of the actions and provides handles to them in the Store object.
"""


@dataclass(frozen=True)
class MovementPreparation:
    # infiltrate_infected_media: ActionHandle = field(init=False, default=_infiltrate_infected_media)
    # infect_removable_media: ActionHandle = field(init=False, default=_infect_removable_media)
    # taint_shared_content: ActionHandle = field(init=False, default=_taint_shared_content)
    create_internal_phishing_mail_as_client: ActionHandle = field(init=False, default=_create_internal_phishing_mail)
    create_internal_phishing_mail: ActionHandle = field(init=False, default=_create_internal_phishing_mail_email_srv)
    # create_domain_configuration_object: ActionHandle = field(
    #     init=False, default=_create_domain_configuration_object
    # )


@dataclass(frozen=True)
class PureInitAccess:
    pass
    # drive_by_compromise: ActionHandle = field(
    #     init=False, default=_drive_by_compromise
    # )  # TODO: really - internal watering hole??
    # supply_chain_backdoor: ActionHandle = field(
    #     init=False, default=_supply_chain_compromise
    # )  # also covers HW additions


@dataclass(frozen=True)
class PureLateral:
    pass
    # with_tainted_content: ActionHandle = field(init=False, default=_move_with_tainted_content)
    # with_domain_account: ActionHandle = field(init=False, default=_move_with_domain_account)
    # with_domain_configuration_object: ActionHandle = field(
    #     init=False, default=_move_with_domain_config_object
    # )


@dataclass(frozen=True)
class Movement:
    pure_init_access: PureInitAccess = field(init=False, default=PureInitAccess())
    pure_lateral: PureLateral = field(init=False, default=PureLateral())
    exploit_open_application: ActionHandle = field(init=False, default=_exploit_open_application)
    exploit_authed_application: ActionHandle = field(init=False, default=_exploit_authed_application)
    phishing_with_malware: ActionHandle = field(init=False, default=_phishing_with_malware)
    use_known_credentials: ActionHandle = field(init=False, default=_valid_accounts)
    # use_infected_removable_media: ActionHandle = field(init=False, default=_move_via_infected_media)
    # escape_to_host: ActionHandle = field(init=False, default=_escape_to_host)


@dataclass(frozen=True)
class CredentialAccess:
    # phishing: ActionHandle = field(init=False, default=_credential_access_phish)
    # dump_os_vault: ActionHandle = field(init=False, default=_credential_access_os_vault)
    # read_os_file: ActionHandle = field(init=False, default=_credential_access_os_file)
    # dump_cached_creds: ActionHandle = field(init=False, default=_credential_access_os_cached)
    bruteforce: ActionHandle = field(init=False, default=_credential_access_bruteforce)
    # read_unsecure_store: ActionHandle = field(
    #     init=False, default=_credential_access_unsecured_creds
    # )
    # forced_auth: ActionHandle = field(init=False, default=_credential_access_forced_auth)
    # input_capture: ActionHandle = field(init=False, default=_credential_access_input_capture)
    exploit_local_service: ActionHandle = field(init=False, default=_credential_access_exploit_local_service)
    exploit_remote_service: ActionHandle = field(init=False, default=_credential_access_exploit_remote_service)
    # exploit_2fa: ActionHandle = field(init=False, default=_credential_access_exploit_2fa)
    # steal_2fa_known_creds: ActionHandle = field(
    #     init=False, default=_credential_access_2fa_service_use
    # )


@dataclass(frozen=True)
class Persistence:
    pass
    # backdoor: ActionHandle = field(init=False, default=_persist_plant_backdoor)
    # user: ActionHandle = field(init=False, default=_persist_create_account)


@dataclass(frozen=True)
class PrivilegeEscalation:
    suid_executables: ActionHandle = field(init=False, default=_privesc_suid)
    # elevated_com_interface: ActionHandle = field(init=False, default=_privesc_uac_bypass_com)
    tamper_sudoers: ActionHandle = field(init=False, default=_privesc_tamper_sudoers)
    # inject_process: ActionHandle = field(init=False, default=_privesc_process_injection)
    # impersonate_security_token: ActionHandle = field(
    #     init=False, default=_privesc_grab_security_token
    # )
    # spoof_ppid: ActionHandle = field(init=False, default=_privesc_parent_pid_spoof)
    # elevated_autoexec: ActionHandle = field(init=False, default=_privesc_autoexec)
    create_system_service: ActionHandle = field(init=False, default=_privesc_services)
    exploit: ActionHandle = field(init=False, default=_privesc_exploit)
    # byovd: ActionHandle = field(init=False, default=_privesc_byovd)
    # component_hijacking: ActionHandle = field(init=False, default=_privesc_component_hijack)
    # ifeo_and_accessibility_registry_editing: ActionHandle = field(
    #     init=False, default=_privesc_accessibility_ifeo
    # )
    # component_location_or_search_order_hijacking: ActionHandle = field(
    #     init=False, default=_privesc_search_order_tampering
    # )


@dataclass(frozen=True)
class Impact:
    # collect_data: ActionHandle = field(init=False, default=_data_collection)
    # exfiltrate_data: ActionHandle = field(init=False, default=_data_exfiltration)
    manipulate_data: ActionHandle = field(init=False, default=_data_manipulation)
    # wipe_disk: ActionHandle = field(init=False, default=_disk_wipe)
    # shutdown: ActionHandle = field(init=False, default=_shutdown)
    stop_service: ActionHandle = field(init=False, default=_stop_service)
    # remove_account: ActionHandle = field(init=False, default=_account_removal)
    dos: ActionHandle = field(init=False, default=_overwhelm)


# @dataclass(frozen=True)
# class Utils:
#     # backtrack: ActionHandle = field(init=False, default=_backtrack)
#     init: ActionHandle = field(init=False, default=_attack_start)


@dataclass(frozen=True)
class ActionKit:
    discover_reachable: ActionHandle = field(init=False, default=_discover_network_reachable_nodes)
    movement_preparation: MovementPreparation = field(init=False, default=MovementPreparation())
    movement: Movement = field(init=False, default=Movement())
    credential_access: CredentialAccess = field(init=False, default=CredentialAccess())
    persistence: Persistence = field(init=False, default=Persistence())
    privilege_escalation: PrivilegeEscalation = field(init=False, default=PrivilegeEscalation())
    impact: Impact = field(init=False, default=Impact())


AdversaryActionsKit = _PaganActionStore
AdversaryActionHandles = ActionKit()
