import random
import itertools
import dataclasses
import networkx as nx

from pydantic import RootModel, Field
from pydantic.dataclasses import dataclass as PydanticDataclass
from dataclasses import field, InitVar
from typing import DefaultDict, List, Dict, Optional, Self, Tuple
from collections import defaultdict
from serde import serialize

from pagan.shared.primitives.devices import Device, DeviceRole, OperatingSystem, SegmentType
from pagan.shared.primitives.primitives_sets import ServiceSetOption
from pagan.shared.stores.devices import DeviceStore
from pagan.shared.primitives.types import DeviceHandle, OrdinalId, AccessType
from pagan.generation.preparation.topology.constants import PortSet
from pagan.generation.preparation.topology.network_access_rules import AccessRule, FwAction, RuleDirection
from pagan.shared.stores.services import ServiceStore
from pagan.userinputs.topology_parameters import IsolationLevel, TopologyParameters
from functools import partial as PartialFunction


@serialize
@dataclasses.dataclass
class SegmentSRV:
    store: InitVar[DeviceStore]
    want_dc: InitVar[bool]
    dc_count: InitVar[int]
    want_shares: InitVar[bool]
    want_services: InitVar[bool]
    want_db: InitVar[bool]
    vlan_num: int
    dcs: List[DeviceHandle] = field(init=False, default_factory=list)
    db: DeviceHandle | None = field(init=False, default=None)
    shares: DeviceHandle | None = field(init=False, default=None)
    services: DeviceHandle | None = field(init=False, default=None)

    def __post_init__(
        self,
        store: DeviceStore,
        want_dc: bool,
        dc_count: int,
        want_shares: bool,
        want_services: bool,
        want_db: bool,
    ) -> None:
        if want_dc and dc_count > 0:
            for i in range(dc_count):
                dev = Device(
                    name=f"DC_{i}",
                    vlan="SRV",
                    vlan_num=self.vlan_num,
                    roles=[DeviceRole.DOMAIN_CONTROLLER, DeviceRole.NETWORK_SHARE],
                    os=OperatingSystem.WINDOWS_SERVER,
                    segment_info=SegmentType.INTERNAL,
                )
                self.dcs.append(store.add(dev))

        if want_shares:
            self.shares = store.add(
                Device(
                    name="SHARES",
                    vlan="SRV",
                    vlan_num=self.vlan_num,
                    roles=[DeviceRole.NETWORK_SHARE],
                    os=OperatingSystem.random_server(),
                    segment_info=SegmentType.INTERNAL,
                )
            )
        if want_db:
            self.db = store.add(
                Device(
                    name="DB",
                    vlan="SRV",
                    vlan_num=self.vlan_num,
                    roles=[DeviceRole.DATABASE],
                    os=OperatingSystem.random_server(),
                    segment_info=SegmentType.INTERNAL,
                )
            )
        if want_services:
            self.services = store.add(
                Device(
                    name="SERVICES",
                    vlan="SRV",
                    vlan_num=self.vlan_num,
                    roles=[DeviceRole.APP_HOSTING, DeviceRole.WEB_SERVER],
                    os=OperatingSystem.random_server(),
                    segment_info=SegmentType.INTERNAL,
                )
            )


@serialize
@dataclasses.dataclass
class SegmentDMZ:
    store: InitVar[DeviceStore]
    want_pubserv: InitVar[bool]
    want_email: InitVar[bool]
    want_vpn: InitVar[bool]
    vlan_num: int
    public_services: DeviceHandle | None = field(init=False, default=None)
    email: DeviceHandle | None = field(init=False, default=None)
    vpn: DeviceHandle | None = field(init=False, default=None)

    def __post_init__(self, store: DeviceStore, want_pubserv: bool, want_email: bool, want_vpn: bool) -> None:
        if want_pubserv:
            self.public_services = store.add(
                Device(
                    name="public services",
                    vlan="DMZ",
                    vlan_num=self.vlan_num,
                    roles=[DeviceRole.WEB_SERVER, DeviceRole.APP_HOSTING],
                    os=OperatingSystem.random_server(),
                    segment_info=SegmentType.DMZ,
                )
            )
        if want_email:
            self.email = store.add(
                Device(
                    name="EMAIL",
                    vlan="DMZ",
                    vlan_num=self.vlan_num,
                    roles=[DeviceRole.MAIL_SERVER],
                    os=OperatingSystem.random_server(),
                    segment_info=SegmentType.DMZ,
                )
            )

        if want_vpn:
            self.vpn = store.add(
                Device(
                    name="VPN",
                    vlan="DMZ",
                    vlan_num=self.vlan_num,
                    roles=[DeviceRole.REMOTE_ACCESS_GATEWAY],
                    os=OperatingSystem.random_server(),
                    segment_info=SegmentType.DMZ,
                )
            )


@dataclasses.dataclass
class StaticWRKSegment:
    admin_pcs: List[DeviceHandle]
    workstations: List[DeviceHandle]


@serialize
@dataclasses.dataclass
class SegmentWRK:
    store: InitVar[DeviceStore]
    pc_num: InitVar[int]
    admin_pc_count: InitVar[int]
    vlan_num: int
    admin_pcs: List[DeviceHandle] = field(init=False, default_factory=list)
    workstations: List[DeviceHandle] = field(init=False, default_factory=list)

    def __post_init__(self, store: DeviceStore, pc_num: int, admin_pc_count: int):
        for i in range(pc_num):
            if i < admin_pc_count:
                self.admin_pcs.append(
                    store.add(
                        Device(
                            name=f"ADMIN_PC{i}",
                            vlan=f"WRK{self.vlan_num}",
                            vlan_num=self.vlan_num,
                            roles=[DeviceRole.WORKSTATION],
                            os=OperatingSystem.random_desktop(),
                            segment_info=SegmentType.INTERNAL,
                        )
                    )
                )
            else:
                self.workstations.append(
                    store.add(
                        Device(
                            name=f"workstation_{i}",
                            vlan=f"WRK{self.vlan_num}",
                            vlan_num=self.vlan_num,
                            roles=[DeviceRole.WORKSTATION],
                            os=OperatingSystem.random_desktop(),
                            segment_info=SegmentType.INTERNAL,
                        )
                    )
                )


@serialize
@dataclasses.dataclass
class SegmentICS:
    store: InitVar[DeviceStore]
    controller_num: InitVar[int]
    vlan_num: int
    master: DeviceHandle | None = field(init=False, default=None)
    controllers: List[DeviceHandle] = field(init=False, default_factory=list)

    def __post_init__(self, store: DeviceStore, controller_num: int):
        self.master = store.add(
            Device(
                name="MASTER",
                vlan="ICS",
                vlan_num=self.vlan_num,
                roles=[DeviceRole.WORKSTATION],
                os=OperatingSystem.random_desktop(),
                segment_info=SegmentType.ICS,
            )
        )
        for i in range(controller_num):
            self.controllers.append(
                store.add(
                    Device(
                        name=f"controller_{i}",
                        vlan="ICS",
                        vlan_num=self.vlan_num,
                        roles=[DeviceRole.OPERATIONAL_TECHNOLOGY],
                        os=OperatingSystem.SPECIAL_EMBEDDED,
                        segment_info=SegmentType.ICS,
                    )
                )
            )


@serialize
@PydanticDataclass
class AccessesMapAndRules:
    _elems: DefaultDict[DeviceHandle, List[Tuple[DeviceHandle, AccessType]]] = Field(
        init_var=False, default_factory=PartialFunction(defaultdict, list)
    )
    _rules: List[AccessRule] = Field(init_var=False, default_factory=list)
    # list so we can make linear processing resembling actual firewall ACLs

    """ The following two methods handle additions to the access-map in a way so that None values are
    semi-silently ignored, so that logic creating the accesses does not need to handle whether the device was
    requested/added-to the infrastructure
    """

    def add_bidirectional(
        self,
        source: DeviceHandle | None,
        target: DeviceHandle | None,
        atype: AccessType,
        *networkrules: AccessRule,
    ) -> bool:
        if source is None or target is None:
            return False
        self._elems[source].append((target, atype))
        self._elems[target].append((source, atype))
        if atype == AccessType.NETWORK:
            self._rules.extend(
                networkrules
            )  # we might want to check here that the device handles in the rules do not differ in from the connection
        return True

    def add_onedirectional(
        self,
        source: DeviceHandle | None,
        target: DeviceHandle | None,
        atype: AccessType,
        *networkrules: AccessRule,
    ) -> bool:
        if source is None or target is None:
            return False
        self._elems[source].append((target, atype))
        if atype == AccessType.NETWORK:
            self._rules.extend(
                networkrules
            )  # we might want to check here that the device handles in the rules do not differ in from the connection
        return True

    @property
    def elems(self) -> Dict[DeviceHandle, List[Tuple[DeviceHandle, AccessType]]]:
        return self._elems

    @property
    def rules(self) -> List[AccessRule]:
        return self._rules

    def get_from(
        self, device: DeviceHandle, wanted: Optional[List[AccessType]] = None
    ) -> List[Tuple[DeviceHandle, AccessType]]:
        if wanted is None:
            return self.elems[device]
        return list(filter(lambda item: item[1] in wanted, self.elems[device]))


def graphify_accesses(
    elems: Dict[DeviceHandle, List[Tuple[DeviceHandle, AccessType]]],
    store: Optional[DeviceStore] = None,
) -> nx.DiGraph:
    graph = nx.DiGraph()
    if store is not None:
        for node in store.handles:
            graph.add_node(node, info=store.get(node))
    for node_u, conn in elems.items():
        for node_v, spec in conn:
            graph.add_edge(node_u, node_v, type=spec)

    return graph


@dataclasses.dataclass(frozen=True)
class DeviceHandlesView:
    dmz: SegmentDMZ | None
    srv: SegmentSRV | None
    wrk: List[SegmentWRK | StaticWRKSegment]
    ics: SegmentICS | None
    third_party: DeviceHandle | None
    attacker: DeviceHandle
    untrusted_internet: DeviceHandle
    trusted_internet: DeviceHandle


class _TopologyBuilder:
    def __init__(self):
        return

    def build_from_params(
        self,
        *,
        dmz: bool,
        dmz_mail: bool,
        dmz_pubserv: bool,
        dmz_vpn: bool,
        srv: bool,
        srv_dc: bool,
        srv_dc_count: int,
        srv_shares: bool,
        srv_services: bool,
        srv_db: bool,
        work_segments: bool,
        work_segment_count: int,
        work_segment_populations: List[int],
        work_segment_admin_stations: List[int],
        domain_controlled: bool,
        admin_password_sanity: int,
        ics: bool,
        ics_controller_count: int,
        ics_airgap: bool,
        third_party: bool,
        intra_segment_isolation: IsolationLevel,
        inter_segment_isolation: IsolationLevel,
        **kwargs,  # this definitely cannot be like this
    ) -> "Topology":
        self._store = DeviceStore()
        vlan_counter = 0
        self.domain_controlled = domain_controlled
        self.admin_password_sanity = admin_password_sanity

        self._dmz = None
        self._srv = None
        self._wrk: List[SegmentWRK] = list()
        self._ics = None
        self._third_party = None

        if dmz:
            self._dmz = SegmentDMZ(
                store=self._store,
                want_pubserv=dmz_pubserv,
                want_email=dmz_mail,
                want_vpn=dmz_vpn,
                vlan_num=vlan_counter,
            )
            vlan_counter += 1

        if srv:
            self._srv = SegmentSRV(
                store=self._store,
                want_dc=srv_dc,
                dc_count=srv_dc_count,
                want_shares=srv_shares,
                want_services=srv_services,
                want_db=srv_db,
                vlan_num=vlan_counter,
            )
            vlan_counter += 1

        if work_segments:
            for i in range(work_segment_count):
                self._wrk.append(
                    SegmentWRK(
                        store=self._store,
                        pc_num=work_segment_populations[i],
                        admin_pc_count=work_segment_admin_stations[i],
                        vlan_num=vlan_counter,
                    )
                )
                vlan_counter += 1

        self._accesses = (
            self._compute_internal_accesses()
        )  # important to have this before ICS, outside and attacker init

        self.trusted_internet = self._store.add(
            Device(
                name="trusted internet",
                vlan="internet",
                vlan_num=vlan_counter,
                os=OperatingSystem.MOCK,
                roles=[],
                segment_info=SegmentType.INTERNET,
            )
        )
        self.untrusted_internet = self._store.add(
            Device(
                name="untrusted internet",
                vlan="internet",
                vlan_num=vlan_counter,
                os=OperatingSystem.MOCK,
                roles=[],
                segment_info=SegmentType.INTERNET,
            )
        )

        vlan_counter += 1
        self._accesses = self._compute_external_accesses(self._accesses)

        if ics:
            self._ics = SegmentICS(store=self._store, controller_num=ics_controller_count, vlan_num=vlan_counter)
            vlan_counter += 1

        if ics:
            self._compute_ics_accesses(self._accesses, ics_airgap)

        if third_party:
            self._third_party = self._store.add(
                Device(
                    name="3rd_party_pc",
                    vlan="3RD PARTY",
                    vlan_num=vlan_counter,
                    roles=[DeviceRole.WORKSTATION],
                    os=OperatingSystem.random_desktop(),
                    segment_info=SegmentType.PARTNER,
                )
            )
            vlan_counter += 1
            self._accesses = self._compute_third_party_accesses(self._accesses)

        self._attacker = self._store.add(
            Device(
                name="attacker",
                vlan="ATK",
                vlan_num=vlan_counter,
                roles=[DeviceRole.WORKSTATION],
                os=OperatingSystem.NIX_DESKTOP,
                segment_info=SegmentType.ATTACKER,
            )
        )
        vlan_counter += 1

        self._accesses = self._compute_attacker_accesses(self._accesses)
        # here allow rule relaxation and user meddling
        self._store.block()

        handles = DeviceHandlesView(
            dmz=self._dmz,
            srv=self._srv,
            wrk=self._wrk,
            ics=self._ics,
            third_party=self._third_party,
            attacker=self._attacker,
            untrusted_internet=self.untrusted_internet,
            trusted_internet=self.trusted_internet,
        )

        return Topology(store=self._store, accesses=self._accesses, handles=handles)

    def _compute_internal_accesses(
        self,
    ) -> AccessesMapAndRules:
        accesses = AccessesMapAndRules()

        # internal

        if self._srv is not None:
            # DC to everyone
            for oid in self._store.handles:
                for dc_handle in self._srv.dcs:
                    accesses.add_bidirectional(
                        oid,
                        dc_handle,
                        AccessType.NETWORK,
                        AccessRule(
                            action=FwAction.ALLOW,
                            direction=RuleDirection.ESTABLISHED,
                            source_handle=oid,
                            source_ports=PortSet.DYNAMIC_PORTS,
                            destination_handle=dc_handle,
                            destination_ports=PortSet.ACTIVE_DIRECTORY,
                        ),
                    )
                    accesses.add_bidirectional(oid, dc_handle, AccessType.SHARED_FILESYS)

            # services to backbone
            accesses.add_bidirectional(
                self._srv.services,
                self._srv.db,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._srv.services,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._srv.db,
                    destination_ports=PortSet.DATABASES,
                ),
            )
            accesses.add_bidirectional(
                self._srv.services,
                self._srv.shares,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._srv.services,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._srv.shares,
                    destination_ports=PortSet.REMOTE_FILESYSTEM,
                ),
            )

        if self._dmz is not None and self._srv is not None:
            # DMZ to services + db
            accesses.add_bidirectional(
                self._dmz.public_services,
                self._srv.services,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._dmz.public_services,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._srv.services,
                    destination_ports=[],  # TODO: fill,
                ),
            )
            accesses.add_bidirectional(
                self._dmz.public_services,
                self._srv.db,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._dmz.public_services,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._srv.db,
                    destination_ports=PortSet.DATABASES,
                ),
            )

        # Admin PC remoting to everything -> we might want to make this more strict in the future
        for segment in self._wrk:
            for station in segment.admin_pcs:
                for handle in self._store.handles:
                    accesses.add_bidirectional(
                        station,
                        handle,
                        AccessType.NETWORK,
                        AccessRule(
                            action=FwAction.ALLOW,
                            direction=RuleDirection.ESTABLISHED,
                            source_handle=station,
                            source_ports=PortSet.DYNAMIC_PORTS,
                            destination_handle=handle,
                            destination_ports=PortSet.REMOTE_MANAGEMENT,
                        ),
                    )

        # SHARED FS and media between work PCs and admin stations
        for subnet1, subnet2 in itertools.combinations_with_replacement(self._wrk, 2):
            for station_1 in [*subnet1.workstations, *subnet1.admin_pcs]:
                for station_2 in [*subnet2.workstations, *subnet2.admin_pcs]:
                    accesses.add_bidirectional(station_1, station_2, AccessType.MEDIA)
                    if self._srv and self._srv.shares:
                        accesses.add_bidirectional(station_1, station_2, AccessType.SHARED_FILESYS)

        if self._srv and self._srv.shares:
            for subnet in self._wrk:
                for station in [*subnet.workstations, *subnet.admin_pcs]:
                    accesses.add_bidirectional(
                        station,
                        self._srv.shares,
                        AccessType.NETWORK,
                        AccessRule(
                            action=FwAction.ALLOW,
                            direction=RuleDirection.ESTABLISHED,
                            source_handle=station,
                            source_ports=PortSet.DYNAMIC_PORTS,
                            destination_handle=self._srv.shares,
                            destination_ports=PortSet.REMOTE_FILESYSTEM,
                        ),
                    )

        if self._dmz and self._dmz.public_services:
            for subnet in self._wrk:
                for station in [*subnet.admin_pcs, *subnet.workstations]:
                    accesses.add_bidirectional(
                        station,
                        self._dmz.public_services,
                        AccessType.NETWORK,
                        AccessRule(
                            action=FwAction.ALLOW,
                            direction=RuleDirection.ESTABLISHED,
                            source_handle=station,
                            source_ports=PortSet.DYNAMIC_PORTS,
                            destination_handle=self._dmz.public_services,
                            destination_ports=PortSet.WEB,
                        ),
                    )

        if self._dmz and self._dmz.email:
            for subnet in self._wrk:
                for station in [*subnet.admin_pcs, *subnet.workstations]:
                    accesses.add_bidirectional(
                        station,
                        self._dmz.email,
                        AccessType.NETWORK,
                        AccessRule(
                            action=FwAction.ALLOW,
                            direction=RuleDirection.ESTABLISHED,
                            source_handle=station,
                            source_ports=PortSet.DYNAMIC_PORTS,
                            destination_handle=self._dmz.email,
                            destination_ports=PortSet.EMAIL_CLIENT_SERVER,
                        ),
                    )

        if self._srv and self._srv.services:
            for subnet in self._wrk:
                for station in [*subnet.admin_pcs, *subnet.workstations]:
                    accesses.add_bidirectional(
                        station,
                        self._srv.services,
                        AccessType.NETWORK,
                        AccessRule(
                            action=FwAction.ALLOW,
                            direction=RuleDirection.ESTABLISHED,
                            source_handle=station,
                            source_ports=PortSet.DYNAMIC_PORTS,
                            destination_handle=self._srv.services,
                            destination_ports=[],  # TODO: fill
                        ),
                    )

        if self._dmz:
            accesses.add_bidirectional(
                self._dmz.vpn,
                self._dmz.email,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._dmz.vpn,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._dmz.email,
                    destination_ports=PortSet.EMAIL_CLIENT_SERVER,
                ),
            )

        if self._dmz and self._srv:
            accesses.add_bidirectional(
                self._dmz.vpn,
                self._srv.services,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._dmz.vpn,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._srv.services,
                    destination_ports=[],  # TODO: fill
                ),
            )

        if self._wrk and self._srv:
            developer_segment = random.choice(self._wrk)
            for station in [*developer_segment.workstations, *developer_segment.admin_pcs]:
                accesses.add_bidirectional(
                    station,
                    self._srv.services,
                    AccessType.NETWORK,
                    AccessRule(
                        action=FwAction.ALLOW,
                        direction=RuleDirection.ESTABLISHED,
                        source_handle=station,
                        source_ports=PortSet.DYNAMIC_PORTS,
                        destination_handle=self._srv.services,
                        destination_ports=[],  # TODO: fill - service maintenance as well
                    ),
                )
                accesses.add_bidirectional(
                    station,
                    self._srv.db,
                    AccessType.NETWORK,
                    AccessRule(
                        action=FwAction.ALLOW,
                        direction=RuleDirection.ESTABLISHED,
                        source_handle=station,
                        source_ports=PortSet.DYNAMIC_PORTS,
                        destination_handle=self._srv.db,
                        destination_ports=PortSet.DATABASES,  # TODO: add  maintenance as well?
                    ),
                )

        return accesses

    def _compute_external_accesses(self, accesses: AccessesMapAndRules) -> AccessesMapAndRules:
        if self._dmz:
            accesses.add_bidirectional(
                self.untrusted_internet,
                self._dmz.vpn,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self.untrusted_internet,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._dmz.vpn,
                    destination_ports=PortSet.VPN,
                ),
            )

            accesses.add_bidirectional(
                self.untrusted_internet,
                self._dmz.public_services,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self.untrusted_internet,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._dmz.public_services,
                    destination_ports=PortSet.WEB,
                ),
            )
            accesses.add_bidirectional(
                self._dmz.email,
                self.untrusted_internet,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.BIDIRECTIONAL,
                    source_handle=self._dmz.email,
                    source_ports=PortSet.EMAIL_SERVER_RELAY,
                    destination_handle=self.untrusted_internet,
                    destination_ports=PortSet.EMAIL_SERVER_RELAY,
                ),
            )

            accesses.add_bidirectional(
                self._dmz.vpn,
                self.trusted_internet,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._dmz.vpn,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self.trusted_internet,
                    destination_ports=PortSet.WEB,
                ),
            )
            accesses.add_bidirectional(
                self._dmz.email,
                self.trusted_internet,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._dmz.email,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self.trusted_internet,
                    destination_ports=PortSet.WEB,
                ),
            )
            accesses.add_bidirectional(
                self._dmz.public_services,
                self.trusted_internet,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._dmz.public_services,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self.trusted_internet,
                    destination_ports=PortSet.WEB,
                ),
            )

        if self._srv:
            accesses.add_bidirectional(
                self._srv.db,
                self.trusted_internet,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._srv.db,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self.trusted_internet,
                    destination_ports=PortSet.WEB,
                ),
            )
            accesses.add_bidirectional(
                self._srv.services,
                self.trusted_internet,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._srv.services,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self.trusted_internet,
                    destination_ports=PortSet.WEB,
                ),
            )
            accesses.add_bidirectional(
                self._srv.shares,
                self.trusted_internet,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._srv.shares,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self.trusted_internet,
                    destination_ports=PortSet.WEB,
                ),
            )

            for dc in self._srv.dcs:
                accesses.add_bidirectional(
                    dc,
                    self.trusted_internet,
                    AccessType.NETWORK,
                    AccessRule(
                        action=FwAction.ALLOW,
                        direction=RuleDirection.ESTABLISHED,
                        source_handle=dc,
                        source_ports=PortSet.DYNAMIC_PORTS,
                        destination_handle=self.trusted_internet,
                        destination_ports=PortSet.WEB,
                    ),
                )

        for subnet in self._wrk:
            for station in [*subnet.workstations, *subnet.admin_pcs]:
                accesses.add_bidirectional(
                    station,
                    self.untrusted_internet,
                    AccessType.NETWORK,
                    AccessRule(
                        action=FwAction.ALLOW,
                        direction=RuleDirection.ESTABLISHED,
                        source_handle=station,
                        source_ports=PortSet.DYNAMIC_PORTS,
                        destination_handle=self.untrusted_internet,
                        destination_ports=[
                            *PortSet.WEB,
                            *PortSet.EMAIL_CLIENT_SERVER,
                            *PortSet.DNS,
                        ],
                    ),
                )
        return accesses

    def _compute_ics_accesses(self, accesses: AccessesMapAndRules, ics_airgap: bool) -> AccessesMapAndRules:
        # OT
        if self._ics:
            for scada in self._ics.controllers:
                accesses.add_bidirectional(
                    scada,
                    self._ics.master,
                    AccessType.NETWORK,
                    AccessRule(
                        action=FwAction.ALLOW,
                        direction=RuleDirection.BIDIRECTIONAL,
                        source_handle=scada,
                        source_ports=PortSet.ICS,
                        destination_handle=self._ics.master,
                        destination_ports=PortSet.ICS,
                    ),
                )
            ics_maintenance_subnet = random.choice(list(filter(lambda subnet: subnet.admin_pcs, self._wrk)))
            for station in ics_maintenance_subnet.admin_pcs:
                accesses.add_bidirectional(self._ics.master, station, AccessType.MEDIA)
                if not ics_airgap:
                    accesses.add_bidirectional(
                        station,
                        self._ics.master,
                        AccessType.NETWORK,
                        AccessRule(
                            action=FwAction.ALLOW,
                            direction=RuleDirection.ESTABLISHED,
                            source_handle=station,
                            source_ports=PortSet.DYNAMIC_PORTS,
                            destination_handle=self._ics.master,
                            destination_ports=PortSet.REMOTE_MANAGEMENT,
                        ),
                    )
        return accesses

    def _compute_third_party_accesses(self, accesses: AccessesMapAndRules) -> AccessesMapAndRules:
        if self._dmz:
            accesses.add_bidirectional(
                self._third_party,
                self._dmz.vpn,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._third_party,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._dmz.vpn,
                    destination_ports=PortSet.VPN,
                ),
            )
            accesses.add_bidirectional(
                self._third_party,
                self._dmz.public_services,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._third_party,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self._dmz.public_services,
                    destination_ports=PortSet.WEB,
                ),
            )
            accesses.add_bidirectional(
                self._third_party,
                self.untrusted_internet,
                AccessType.NETWORK,
                AccessRule(
                    action=FwAction.ALLOW,
                    direction=RuleDirection.ESTABLISHED,
                    source_handle=self._third_party,
                    source_ports=PortSet.DYNAMIC_PORTS,
                    destination_handle=self.untrusted_internet,
                    destination_ports=[*PortSet.WEB, *PortSet.EMAIL_CLIENT_SERVER, *PortSet.DNS],
                ),
            )

        return accesses

    def _compute_attacker_accesses(self, accesses: AccessesMapAndRules) -> AccessesMapAndRules:
        accesses.add_bidirectional(
            self._attacker,
            self.untrusted_internet,
            AccessType.NETWORK,
            AccessRule(
                action=FwAction.ALLOW,
                direction=RuleDirection.ESTABLISHED,
                source_handle=self._attacker,
                source_ports=PortSet.ALL,
                destination_handle=self.untrusted_internet,
                destination_ports=PortSet.ALL,
            ),
        )

        # TODO: IMPORTANT - attacker must have all capabilities on the device representing the untrusted internet

        return accesses


@serialize
@dataclasses.dataclass
class Topology:
    _handles: DeviceHandlesView
    _accesses: AccessesMapAndRules
    _store: DeviceStore

    def __init__(self, store: DeviceStore, handles: DeviceHandlesView, accesses: AccessesMapAndRules) -> None:
        self._store = store
        self._handles = handles
        self._accesses = accesses

    @classmethod
    def from_parameters(cls, params: TopologyParameters) -> Self:
        return _TopologyBuilder.build_from_params(**params.as_dict())

    @property
    def handles(self) -> DeviceHandlesView:
        return self._handles

    @property
    def accesses(self) -> AccessesMapAndRules:
        return self._accesses

    @property
    def store(self) -> DeviceStore:
        return self._store

    def set_prohibited_services(self, service_store: ServiceStore) -> None:
        for device in self.store.get_all_with(lambda _: True):
            for role in device.roles:
                match role:
                    case DeviceRole.WEB_SERVER:
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("skype"),
                                    service_store.get_by_name("redisStack"),
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("smb"),
                                    service_store.get_by_name("freeFTP"),
                                    service_store.get_by_name("MSoffice"),
                                    service_store.get_by_name("thunderbird"),
                                    service_store.get_by_name("outlook"),
                                    service_store.get_by_name("wireguard"),
                                }
                            )
                        )
                        device.compulsory_services = device.compulsory_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                    service_store.get_by_name("docker-daemon"),
                                }
                            )
                        )
                    case DeviceRole.APP_HOSTING:
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("skype"),
                                    service_store.get_by_name("redisStack"),
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("smb"),
                                    service_store.get_by_name("freeFTP"),
                                    service_store.get_by_name("MSoffice"),
                                    service_store.get_by_name("thunderbird"),
                                    service_store.get_by_name("outlook"),
                                    service_store.get_by_name("wireguard"),
                                }
                            )
                        )
                        device.compulsory_services = device.compulsory_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                    service_store.get_by_name("docker-daemon"),
                                }
                            )
                        )
                    case DeviceRole.WORKSTATION:
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("redisStack"),
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("smb"),
                                    service_store.get_by_name("freeFTP"),
                                    service_store.get_by_name("printspooler"),
                                    service_store.get_by_name("customapp"),
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                }
                            )
                        )
                    case DeviceRole.NETWORK_SHARE:
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("skype"),
                                    service_store.get_by_name("redisStack"),
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                    service_store.get_by_name("MSoffice"),
                                    service_store.get_by_name("thunderbird"),
                                    service_store.get_by_name("outlook"),
                                    service_store.get_by_name("wireguard"),
                                    service_store.get_by_name("customapp"),
                                }
                            )
                        )
                        device.compulsory_services = device.compulsory_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("smb"),
                                    service_store.get_by_name("freeFTP"),
                                }
                            )
                        )
                    case DeviceRole.DATABASE:
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("skype"),
                                    service_store.get_by_name("smb"),
                                    service_store.get_by_name("freeFTP"),
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                    service_store.get_by_name("MSoffice"),
                                    service_store.get_by_name("thunderbird"),
                                    service_store.get_by_name("outlook"),
                                    service_store.get_by_name("wireguard"),
                                    service_store.get_by_name("customapp"),
                                }
                            )
                        )
                        device.compulsory_services = device.compulsory_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("redisStack"),
                                }
                            )
                        )
                    case DeviceRole.BACKUP_SERVER:  # ATM equal to network share
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("skype"),
                                    service_store.get_by_name("redisStack"),
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                    service_store.get_by_name("MSoffice"),
                                    service_store.get_by_name("thunderbird"),
                                    service_store.get_by_name("outlook"),
                                    service_store.get_by_name("wireguard"),
                                    service_store.get_by_name("customapp"),
                                }
                            )
                        )
                        device.compulsory_services = device.compulsory_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("smb"),
                                    service_store.get_by_name("freeFTP"),
                                }
                            )
                        )
                    case DeviceRole.DOMAIN_CONTROLLER:
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("skype"),
                                    service_store.get_by_name("redisStack"),
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                    service_store.get_by_name("MSoffice"),
                                    service_store.get_by_name("thunderbird"),
                                    service_store.get_by_name("Firefox"),
                                    service_store.get_by_name("outlook"),
                                    service_store.get_by_name("wireguard"),
                                    service_store.get_by_name("freeFTP"),
                                    service_store.get_by_name("docker-daemon"),
                                    service_store.get_by_name("customapp"),
                                    service_store.get_by_name("telnet"),
                                    service_store.get_by_name("TeamViewer"),
                                    service_store.get_by_name("openVNC"),
                                }
                            )
                        )
                        device.compulsory_services = device.compulsory_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("smb"),
                                }
                            )
                        )
                    case DeviceRole.MAIL_SERVER:
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("skype"),
                                    service_store.get_by_name("redisStack"),
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                    service_store.get_by_name("MSoffice"),
                                    service_store.get_by_name("thunderbird"),
                                    service_store.get_by_name("outlook"),
                                    service_store.get_by_name("wireguard"),
                                    service_store.get_by_name("customapp"),
                                    service_store.get_by_name("smb"),
                                    service_store.get_by_name("freeFTP"),
                                }
                            )
                        )
                    case DeviceRole.NETWORK_APPLIANCE:
                        device.prohibited_services = ServiceSetOption.ALL()
                    case DeviceRole.OPERATIONAL_TECHNOLOGY:
                        device.prohibited_services = ServiceSetOption.ALL()
                    case DeviceRole.REMOTE_ACCESS_GATEWAY:
                        device.prohibited_services = device.prohibited_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("skype"),
                                    service_store.get_by_name("redisStack"),
                                    service_store.get_by_name("mysql"),
                                    service_store.get_by_name("nginx"),
                                    service_store.get_by_name("apache-web"),
                                    service_store.get_by_name("MSoffice"),
                                    service_store.get_by_name("thunderbird"),
                                    service_store.get_by_name("outlook"),
                                    service_store.get_by_name("wireguard"),
                                    service_store.get_by_name("freeFTP"),
                                    service_store.get_by_name("docker-daemon"),
                                    service_store.get_by_name("customapp"),
                                    service_store.get_by_name("telnet"),
                                    service_store.get_by_name("TeamViewer"),
                                    service_store.get_by_name("openVNC"),
                                }
                            )
                        )
                        device.compulsory_services = device.compulsory_services.union(
                            ServiceSetOption.SOME(
                                {
                                    service_store.get_by_name("wireguard"),
                                }
                            )
                        )

            match device.os:
                case OperatingSystem.WINDOWS_SERVER:
                    device.prohibited_services = device.prohibited_services.union(
                        ServiceSetOption.SOME(
                            {
                                service_store.get_by_name("Firefox"),
                                service_store.get_by_name("bash"),
                                service_store.get_by_name("sudo"),
                                service_store.get_by_name("lightdm"),
                            }
                        )
                    )
                case OperatingSystem.WINDOWS_DESKTOP:
                    device.prohibited_services = device.prohibited_services.union(
                        ServiceSetOption.SOME(
                            {
                                service_store.get_by_name("Firefox"),
                                service_store.get_by_name("bash"),
                                service_store.get_by_name("sudo"),
                                service_store.get_by_name("lightdm"),
                            }
                        )
                    )
                case OperatingSystem.NIX_DESKTOP:
                    device.prohibited_services = device.prohibited_services.union(
                        ServiceSetOption.SOME(
                            {
                                service_store.get_by_name("Edge"),
                                service_store.get_by_name("powershell"),
                                service_store.get_by_name("printspooler"),
                                service_store.get_by_name("lightdm"),
                                service_store.get_by_name("TeamViewer"),
                                service_store.get_by_name("rdp"),
                                service_store.get_by_name("winRM"),
                                service_store.get_by_name("winlogon"),
                                service_store.get_by_name("MSoffice"),
                                service_store.get_by_name("outlook"),
                            }
                        )
                    )
                case OperatingSystem.NIX_SERVER:
                    device.prohibited_services = device.prohibited_services.union(
                        ServiceSetOption.SOME(
                            {
                                service_store.get_by_name("Edge"),
                                service_store.get_by_name("powershell"),
                                service_store.get_by_name("printspooler"),
                                service_store.get_by_name("lightdm"),
                                service_store.get_by_name("TeamViewer"),
                                service_store.get_by_name("rdp"),
                                service_store.get_by_name("winRM"),
                                service_store.get_by_name("winlogon"),
                                service_store.get_by_name("MSoffice"),
                                service_store.get_by_name("outlook"),
                            }
                        )
                    )

    def finalize(self, service_store: ServiceStore) -> Self:
        self.set_prohibited_services(service_store)
        # add default firewallentry : "DENY *:* <-> *:*"
        self._accesses.rules.append(
            AccessRule(
                action=FwAction.DENY,
                direction=RuleDirection.BIDIRECTIONAL,
                source_handle=None,
                source_ports=PortSet.ALL,
                destination_handle=None,
                destination_ports=PortSet.ALL,
            )
        )

        return self
