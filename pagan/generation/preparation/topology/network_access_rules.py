from enum import StrEnum
from typing import List
from pydantic.dataclasses import dataclass as PydanticDataclass

from pagan.shared.primitives.types import DeviceHandle, PortNum, PortRange


class FwAction(StrEnum):
    ALLOW = "allow"
    DENY = "deny"


class RuleDirection(StrEnum):
    ONEWAY = "oneway"
    BIDIRECTIONAL = "bidirectional"
    ESTABLISHED = (
        "established"  # bidirectional but must be always initialized by the source of the rule
    )


@PydanticDataclass
class AccessRule:
    action: FwAction
    direction: RuleDirection
    source_handle: DeviceHandle | None  # none is allowed here for ease of topology generation, but needs to be checked before adding to the firewall
    source_ports: List[PortNum | PortRange]
    destination_handle: DeviceHandle | None
    destination_ports: List[PortNum | PortRange]
