import networkx as nx
import random

from typing import Tuple, Dict

from pagan.shared.primitives.devices import Device
from pagan.shared.stores.devices import DeviceStore
from pagan.shared.primitives.types import OrdinalId


# this is a rework of tools.scenario_creat.graph_create.device_graph
class InfraBuilder:
    def __init__(self, topology: nx.Graph, max_vlan_size: int, min_vlan_size: int = 1):
        self._topology = topology
        self._max_vlan_size = max_vlan_size
        self._min_vlan_size = min_vlan_size
        self._device_graph = nx.Graph()
        self._vlan_dict = dict()  # for-each vlan its switch/router represented here
        self._internet_present: bool = "red" in map(
            lambda x: x[1], topology.nodes(data="color", default=None)
        )
        self._colors = set()
        while len(self._colors) < topology.number_of_nodes() + 1:
            self._colors.add("#" + "".join([random.choice("0123456789ABCDEF") for j in range(6)]))

        self._colors = list(self._colors)
        self._actual_device_id = 0
        self.devices = DeviceStore()
        self.handles = dict()

    def create_device_graph(
        self,
    ) -> Tuple[nx.Graph, DeviceStore, Dict[str, OrdinalId]]:
        if self._internet_present:
            self._vlan_dict[-1] = "WAN"
            self._device_graph.add_node("WAN", color=self._colors[-1])
        for node_num, color in self._topology.nodes(data="color", default=None):
            if color is None:
                raise RuntimeError(
                    "Malformed topology graph: missing color indicating vlan properties."
                )
            if color == "green":
                self.add_attacker_vlan(node_num)
            elif color == "red":
                self._add_outside_vlan(node_num)
            else:
                self._add_inside_vlan(node_num)
        self._connect_vlans()
        return self._device_graph, self.devices, self.handles

    def add_attacker_vlan(self, vlan_num: int) -> None:
        self._vlan_dict[vlan_num] = "attacker"
        self.handles["attacker"] = self.devices.add(Device("attacker", "atk", -1))
        self._actual_device_id += 1
        self._device_graph.add_node("attacker", color=self._colors[vlan_num], vlan=vlan_num)

    def _add_outside_vlan(self, vlan_num: int) -> None:
        devices = 1

        switch_name = "outer_vlan{}_switch".format(vlan_num)
        self._vlan_dict[vlan_num] = switch_name
        self._device_graph.add_node(switch_name, color=self._colors[vlan_num], vlan=vlan_num)

        for i in range(devices):
            device_name = "vlan{}_device{}".format(vlan_num, i)
            self.handles[device_name] = self.devices.add(Device(device_name, "outer", vlan_num))
            self._actual_device_id += 1
            self._device_graph.add_node(device_name, color=self._colors[vlan_num], vlan=vlan_num)
            self._device_graph.add_edge(switch_name, device_name, type="l2")

    def _add_inside_vlan(self, vlan_num: int) -> None:
        # decide when to add l2-switch/l3switch/border router
        # if vlan connects to outside party and inside use router
        # if vlan only connects to another with a border router present/ l3 switch  use an l2 switch
        # if multiple vlans connected then use l3 switch

        devices = random.randint(self._min_vlan_size, self._max_vlan_size)

        neighbors = next(filter(lambda pair: pair[0] == vlan_num, self._topology.adjacency()))[1]
        border_vlan = False

        for neighbor in neighbors:
            if self._topology.nodes[neighbor]["color"] in ["red", "green"]:
                border_vlan = True

        switch_name = "intra_vlan{}_switch".format(vlan_num)
        self._device_graph.add_node(switch_name, color=self._colors[vlan_num], vlan=vlan_num)

        for i in range(devices):
            device_name = "vlan{}_device{}".format(vlan_num, i)
            self.handles[device_name] = self.devices.add(Device(device_name, "inner", vlan_num))
            self._actual_device_id += 1
            self._device_graph.add_node(device_name, color=self._colors[vlan_num], vlan=vlan_num)
            self._device_graph.add_edge(switch_name, device_name, type="l2")

        if border_vlan:
            router_name = "vlan{}_router".format(vlan_num)
            self._device_graph.add_node(router_name, color=self._colors[vlan_num], vlan=vlan_num)
            self._device_graph.add_edge(router_name, switch_name, type="l2")
            self._vlan_dict[vlan_num] = router_name
        else:
            self._vlan_dict[vlan_num] = switch_name

    def _connect_vlans(self) -> None:
        for vlan_num, color in self._topology.nodes(data="color"):
            if color == "red":
                self._device_graph.add_edge(
                    self._vlan_dict[-1], self._vlan_dict[vlan_num], type="l3"
                )

            elif color == "green":
                if self._internet_present:
                    self._device_graph.add_edge(
                        self._vlan_dict[-1],
                        self._vlan_dict[vlan_num],
                        type="l3",
                    )
                else:
                    for neighbor in next(
                        filter(
                            lambda pair: pair[0] == vlan_num,
                            self._topology.adjacency(),
                        )
                    )[1]:
                        self._device_graph.add_edge(
                            self._vlan_dict[vlan_num],
                            self._vlan_dict[neighbor],
                            type="l3",
                        )

            else:
                for neighbor in next(
                    filter(
                        lambda pair: pair[0] == vlan_num,
                        self._topology.adjacency(),
                    )
                )[1]:
                    if self._topology.nodes[neighbor]["color"] == "red":
                        self._device_graph.add_edge(
                            self._vlan_dict[-1],
                            self._vlan_dict[vlan_num],
                            type="l3",
                        )
                    else:
                        self._device_graph.add_edge(
                            self._vlan_dict[vlan_num],
                            self._vlan_dict[neighbor],
                            type="l2",
                        )


def show_device_graph(graph: nx.Graph):
    import matplotlib.pyplot as plt

    l = nx.kamada_kawai_layout(graph)
    colors = [color for color in map(lambda node: node[1]["color"], graph.nodes(data=True))]
    edge_colors = [
        "green" if layer == "l3" or layer == "l3/trunk" else "grey"
        for layer in map(lambda edge: edge[2]["type"], graph.edges(data=True))
    ]
    nx.draw(
        graph,
        pos=l,
        with_labels=True,
        node_color=colors,
        edge_color=edge_colors,
    )
    plt.show()
