import random
import networkx as nx

from typing import Dict, List, Tuple
from chainingiterator import Chi
from dataclasses import asdict

from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.capabilities import CapabilityTemplate, LimitType
from pagan.shared.primitives.predicate_implementations import All, Any, Empty
from pagan.shared.primitives.predicate_trait import Predicate
from pagan.shared.primitives.types import AccessType, ActionHandle, CapabilityType, DeviceHandle
from pagan.generation.preparation.topology import DeviceHandlesView, graphify_accesses
from pagan.userinputs.goal_definition import GoalDescription, TargetType
from pagan.adversary import Quiver


def choose_target_device(
    target_type: TargetType,
    devices: DeviceHandlesView,
    accesses: Dict[DeviceHandle, List[Tuple[DeviceHandle, AccessType]]],
    topology_cardinality,
) -> DeviceHandle:
    graph = graphify_accesses(accesses)
    for i in range(topology_cardinality):
        target = _choose_target_device_blind(target_type, devices)
        if nx.has_path(graph, devices.untrusted_internet, target):
            return target
    raise ProgramLogicError("Could not find device conforming to goal and reachable from the attacker")


def _choose_target_device_blind(target_type: TargetType, devices: DeviceHandlesView) -> DeviceHandle:
    if target_type == TargetType.DOMAIN_CONTROLLER:
        if devices.srv is None:
            raise ProgramLogicError("DC specified as target, but not present in the topology.")
        if not devices.srv.dcs:
            raise ProgramLogicError("DC specified as target, but not present in the topology.")
        return random.choice(devices.srv.dcs)
    elif target_type == TargetType.DATABASE:
        if devices.srv is None:
            raise ProgramLogicError("DB specified as target, but not present in the topology.")
        if devices.srv.db is None:
            raise ProgramLogicError("DB specified as target, but not present in the topology.")
        return devices.srv.db
    elif target_type == TargetType.SHARES_SERVER:
        if devices.srv is None:
            raise ProgramLogicError("SHARES specified as target, but not present in the topology.")
        if devices.srv.shares is None:
            raise ProgramLogicError("SHARES specified as target, but not present in the topology.")
        return devices.srv.shares
    elif target_type == TargetType.ICS:
        if devices.ics is None:
            raise ProgramLogicError("ICS specified as target, but not present in the topology.")
        if devices.ics.master is None:
            raise ProgramLogicError("ICS specified as target, but not present in the topology.")
        return devices.ics.master
    elif target_type == TargetType.ADMINISTRATOR_WORKSTATION:
        usable_segments: List = Chi(devices.wrk).filter(lambda segment: segment.admin_pcs).collect(list)
        if not usable_segments:
            raise ProgramLogicError("Admin station specified as target, but not present in the topology.")
        return random.choice(random.choice(usable_segments).admin_pcs)
    elif target_type == TargetType.WORKSTATION:
        usable_segments: List = Chi(devices.wrk).filter(lambda segment: segment.workstations).collect(list)
        if not usable_segments:
            raise ProgramLogicError("Admin station specified as target, but not present in the topology.")
        return random.choice(random.choice(usable_segments).workstations)
    else:
        raise TypeError(f"{target_type} is not a valid target type")


def validate_target_action(action: ActionHandle) -> ActionHandle:
    if action not in asdict(Quiver.impact).values():
        raise ProgramLogicError(f"{action} is an invalid goal action. Choose from {Quiver.impact}")
    return action


def compute_goal_specifics(
    goal_description: GoalDescription, chosen_target: DeviceHandle, devices: DeviceHandlesView
) -> Predicate:
    sub_predicates = []
    for spec in goal_description.required_capabilities:
        if spec.to == CapabilityType.PERSISTENCE:
            if (target_type := spec.target) == TargetType.DOMAIN_CONTROLLER:
                if devices.srv is None:
                    raise ProgramLogicError("DC specified as target, but not present in the topology.")
                if not devices.srv.dcs:
                    raise ProgramLogicError("DC specified as target, but not present in the topology.")
                sub_predicates.append(
                    Any(
                        *[
                            CapabilityTemplate(
                                to=CapabilityType.PERSISTENCE,
                                privilege_limit=spec.privilege,
                                on_device=device,
                                limit_type=LimitType.MINIMUM,
                            )
                            for device in devices.srv.dcs
                        ]
                    )
                )
            elif target_type == TargetType.DATABASE:
                if devices.srv is None:
                    raise ProgramLogicError("DB specified as target, but not present in the topology.")
                if devices.srv.db is None:
                    raise ProgramLogicError("DB specified as target, but not present in the topology.")
                sub_predicates.append(
                    All(
                        *[
                            CapabilityTemplate(
                                to=CapabilityType.PERSISTENCE,
                                privilege_limit=spec.privilege,
                                on_device=devices.srv.db,
                                limit_type=LimitType.MINIMUM,
                            )
                        ]
                    )
                )
            elif target_type == TargetType.SHARES_SERVER:
                if devices.srv is None:
                    raise ProgramLogicError("SHARES specified as target, but not present in the topology.")
                if devices.srv.shares is None:
                    raise ProgramLogicError("SHARES specified as target, but not present in the topology.")
                sub_predicates.append(
                    All(
                        *[
                            CapabilityTemplate(
                                to=CapabilityType.PERSISTENCE,
                                privilege_limit=spec.privilege,
                                on_device=devices.srv.shares,
                                limit_type=LimitType.MINIMUM,
                            )
                        ]
                    )
                )
            elif target_type == TargetType.ICS:
                if devices.ics is None:
                    raise ProgramLogicError("ICS specified as target, but not present in the topology.")
                if devices.ics.master is None:
                    raise ProgramLogicError("ICS specified as target, but not present in the topology.")
                sub_predicates.append(
                    All(
                        *[
                            CapabilityTemplate(
                                to=CapabilityType.PERSISTENCE,
                                privilege_limit=spec.privilege,
                                on_device=devices.ics.master,
                                limit_type=LimitType.MINIMUM,
                            )
                        ]
                    )
                )
            elif target_type == TargetType.ADMINISTRATOR_WORKSTATION:
                usable_segments: List = Chi(devices.wrk).filter(lambda segment: segment.admin_pcs).collect(list)
                if not usable_segments:
                    raise ProgramLogicError("Admin station specified as target, but not present in the topology.")
                sub_predicates.append(
                    Any(
                        *[
                            CapabilityTemplate(
                                to=CapabilityType.PERSISTENCE,
                                privilege_limit=spec.privilege,
                                on_device=device,
                                limit_type=LimitType.MINIMUM,
                            )
                            for subnet in devices.wrk
                            for device in subnet.admin_pcs
                        ]
                    )
                )
            elif target_type == TargetType.WORKSTATION:
                usable_segments: List = Chi(devices.wrk).filter(lambda segment: segment.workstations).collect(list)
                if not usable_segments:
                    raise ProgramLogicError("Admin station specified as target, but not present in the topology.")
                sub_predicates.append(
                    Any(
                        *[
                            CapabilityTemplate(
                                to=CapabilityType.PERSISTENCE,
                                privilege_limit=spec.privilege,
                                on_device=device,
                                limit_type=LimitType.MINIMUM,
                            )
                            for subnet in devices.wrk
                            for device in subnet.workstations
                        ]
                    )
                )
        elif spec.to == CapabilityType.DOMAIN_LOGIN:
            sub_predicates.append(
                All(
                    *[
                        CapabilityTemplate(
                            to=CapabilityType.DOMAIN_LOGIN,
                            privilege_limit=spec.privilege,
                            on_device=None,
                            limit_type=LimitType.MINIMUM,
                        )
                    ]
                )
            )
        elif spec.to == CapabilityType.CONTROL_DOMAIN:
            sub_predicates.append(
                All(
                    *[
                        CapabilityTemplate(
                            to=CapabilityType.CONTROL_DOMAIN,
                            privilege_limit=spec.privilege,
                            on_device=None,
                            limit_type=LimitType.MINIMUM,
                        )
                    ]
                )
            )
        else:
            sub_predicates.append(
                All(
                    *[
                        CapabilityTemplate(
                            to=spec.to,
                            privilege_limit=spec.privilege,
                            on_device=chosen_target,
                            limit_type=LimitType.MINIMUM,
                        )
                    ]
                )
            )
            if spec.target != goal_description.target:
                print(f"Specified capability {spec.to} does not target the general target. Defaulting.")

    return All(*sub_predicates) if sub_predicates else Empty()
