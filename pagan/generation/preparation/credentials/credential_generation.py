import random

from pagan.shared.primitives.credentials import Credential, CredentialType
from pagan.shared.primitives.person import ITRole
from pagan.shared.primitives.primitives_sets import DeviceSetOption, ServiceSetOption
from pagan.shared.stores.credentials import CredentialStore
from pagan.shared.stores.people import PersonStore
from pagan.shared.stores.services import ServiceStore
from pagan_rs.pagan_rs import Tag, Service


# THIS is now constructed to fit the MVP, a little bit more thought will be needed to get this to cover all the cases


def generate_credentials(people: PersonStore, services: ServiceStore, credential_store: CredentialStore) -> None:
    for person in people.get_all_with(lambda _: True):
        for service_handle in services.get_handles_with(lambda _: True):
            service = services.get(service_handle)
            if (
                Tag.EmailClient in service.tags()
                or Tag.OfficeApplication in service.tags()
                or Tag.Teleconference in service.tags()
                or Tag.PasswordManager in service.tags()
                or Tag.App in service.tags()
                or Tag.EmailClient in service.tags()
                or Tag.Messenger in service.tags()
            ):
                cred = Credential(
                    devices=DeviceSetOption.ALL(),
                    type=CredentialType.APP_PRIMARY,
                    services=ServiceSetOption.SOME({service_handle}),
                    account=-1,
                    online_guess_complexity=(online := random.uniform(25, 75)),
                    unseal_complexity=random.uniform(0, online),
                )

                cred_handle = credential_store.add(cred)
                person.add_credential(cred)

            if (
                ITRole.DEVELOPER in person.it_roles
                or ITRole.SERVER_ADMIN in person.it_roles
                or ITRole.RESTRICTED_SERVER_ADMIN in person.it_roles
            ) and (Tag.Database in service.tags() or Tag.DevelopmentTool in service.tags()):
                cred = Credential(
                    devices=DeviceSetOption.ALL(),
                    type=CredentialType.APP_PRIMARY,
                    services=ServiceSetOption.SOME({service_handle}),
                    account=-1,
                    online_guess_complexity=(online := random.uniform(25, 75)),
                    unseal_complexity=random.uniform(0, online),
                )

                cred_handle = credential_store.add(cred)
                person.add_credential(cred)
