import random
from typing import Tuple, Generator, List, Set

from pagan.shared.primitives.accounts import InfraUserAccount
from pagan.shared.primitives.capabilities import Capability, PrivilegeLevel, LimitType
from pagan.generation.preparation.topology import Topology
from pagan.shared.primitives.credentials import Credential, CredentialType
from pagan.shared.primitives.devices import Device, SegmentType
from pagan.shared.primitives.person import ITRole, Person
from pagan.shared.primitives.primitives_sets import ServiceSetOption, DeviceSetOption
from pagan.shared.stores.accounts import AccountStore
from pagan.shared.stores.credentials import CredentialStore
from pagan.shared.stores.people import PersonStore
from pagan.shared.stores.services import ServiceStore
from pagan.shared.primitives.types import DeviceHandle, CapabilityType, AccountHandle, CredentialHandle, PersonHandle
from pagan_rs.pagan_rs import Tag

from pagan.shared.utils import coin_toss

BASIC_USER_CAPS_LOCAL = [
    CapabilityType.ACCESS_FILESYS_GENERIC,
    CapabilityType.ACCESS_FILESYS_SHARED,
    CapabilityType.FILESYS_READ,
    CapabilityType.FILESYS_WRITE,
    CapabilityType.CONFIG_LOCAL_USER,
    CapabilityType.EXECUTE_CODE,
    CapabilityType.PERSIST_LOG_OUT,
    CapabilityType.IMPERSONATE,
]

ADVANCED_USER_CAPS = [
    CapabilityType.ACCOUNT_MANIPULATION_USER_ADD,
    CapabilityType.ACCOUNT_MANIPULATION_USER_EDIT,
    CapabilityType.SHUTDOWN,
]


RESTRICTED_ADMIN_CAPS = [
    CapabilityType.ACCOUNT_MANIPULATION_USER_DELETE,
    CapabilityType.CONFIG_LOCAL_MACHINE,
]

FULL_ADMIN_CAPS = [
    CapabilityType.SE_DEBUG,
    CapabilityType.SE_LOAD_DRIVER,
    CapabilityType.SE_TOKEN_IMPERSONATE,
    CapabilityType.SE_CREATE_SECURITY_TOKEN,
]

SYSTEM_CAPS = [
    CapabilityType.ACCESS_FILESYS_GENERIC,
    CapabilityType.ACCESS_FILESYS_ELEVATED,
    CapabilityType.FILESYS_READ,
    CapabilityType.FILESYS_WRITE,
    CapabilityType.ACCOUNT_MANIPULATION_USER_DELETE,
    CapabilityType.ACCOUNT_MANIPULATION_USER_EDIT,
    CapabilityType.ACCOUNT_MANIPULATION_USER_ADD,
    CapabilityType.CONFIG_LOCAL_USER,
    CapabilityType.CONFIG_LOCAL_MACHINE,
    CapabilityType.SE_LOAD_DRIVER,
    CapabilityType.SE_DEBUG,
    CapabilityType.SE_TOKEN_IMPERSONATE,
    CapabilityType.SE_CREATE_SECURITY_TOKEN,
    CapabilityType.EXECUTE_CODE,
    CapabilityType.SHUTDOWN,
    CapabilityType.PERSIST_LOG_OUT,
]

SERVICE_CAPS = [
    CapabilityType.ACCESS_FILESYS_GENERIC,
    CapabilityType.FILESYS_READ,
    CapabilityType.FILESYS_WRITE,
    CapabilityType.EXECUTE_CODE,
    CapabilityType.SHUTDOWN,
    CapabilityType.PERSIST_LOG_OUT,
]


def generate_user_account(
    topology: Topology,
    person: Person,
    service_store: ServiceStore,
    caps_template: List[CapabilityType],
    person_id: PersonHandle,
) -> Tuple[Set[DeviceHandle], InfraUserAccount, Credential]:
    computed_caps = {}
    devices = set()
    for segment in topology.handles.wrk:
        for device in segment.workstations:
            devices.add(device)
            computed_caps[device] = [
                Capability(
                    to=cap_type, privilege_limit=PrivilegeLevel.USER, limit_type=LimitType.EXACT, on_device=device
                )
                for cap_type in caps_template
            ]
            if topology.domain_controlled:
                computed_caps[device].append(
                    Capability(
                        to=CapabilityType.DOMAIN_LOGIN,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.EXACT,
                        on_device=device,
                    )
                )

    credential = Credential(
        devices=DeviceSetOption.SOME(devices),
        type=CredentialType.OS,
        services=ServiceSetOption.SOME(
            set(
                service_store.get_handles_with(
                    lambda service: Tag.SystemComponent in service.tags()
                    or Tag.RemoteAccess in service.tags()
                    or Tag.AdministrativeTool in service.tags()
                    or Tag.FileSharing in service.tags()
                )
            )
        ),
        account=-1,
        online_guess_complexity=(online := random.uniform(35, 85)),
        unseal_complexity=random.uniform(0, online),
    )
    account = InfraUserAccount(
        owned_by=person_id,
        nickname=f"{person.name}.{person.age}",
        credential=credential,
        capabilities=computed_caps,
        domain_account=topology.domain_controlled,
    )
    return devices, account, credential


def generate_workstation_admin_accounts(
    topology: Topology,
    person: Person,
    service_store: ServiceStore,
    caps_template: List[CapabilityType],
    person_id: PersonHandle,
) -> Generator[Tuple[List[DeviceHandle], InfraUserAccount, Credential], None, None]:
    if coin_toss(topology.admin_password_sanity):  # sane pwd policy
        for segment in topology.handles.wrk:
            for station in segment.workstations:
                computed_caps = {
                    station: [
                        Capability(
                            to=cap_type,
                            privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                            limit_type=LimitType.EXACT,
                            on_device=station,
                        )
                        for cap_type in caps_template
                    ]
                }

                credential = Credential(
                    devices=DeviceSetOption.SOME({station}),
                    type=CredentialType.OS,
                    services=ServiceSetOption.SOME(
                        set(
                            service_store.get_handles_with(
                                lambda service: Tag.SystemComponent in service.tags()
                                or Tag.RemoteAccess in service.tags()
                                or Tag.AdministrativeTool in service.tags()
                                or Tag.FileSharing in service.tags()
                            )
                        )
                    ),
                    account=-1,
                    online_guess_complexity=(online := random.uniform(50, 100)),
                    unseal_complexity=random.uniform(0, online),
                )
                account = InfraUserAccount(
                    owned_by=person_id,
                    nickname=f"{person.name}.{person.age}adm@{station}",
                    credential=credential,
                    capabilities=computed_caps,
                    domain_account=False,
                )

                yield [station], account, credential

            # devices = set()
            # computed_caps = {}
            for station in segment.admin_pcs:
                # devices.add(station)
                ### Admin account
                computed_caps = {
                    station: [
                        Capability(
                            to=cap_type,
                            privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                            limit_type=LimitType.EXACT,
                            on_device=station,
                        )
                        for cap_type in caps_template
                    ]
                }

                credential = Credential(
                    devices=DeviceSetOption.SOME({station}),
                    type=CredentialType.OS,
                    services=ServiceSetOption.SOME(
                        set(
                            service_store.get_handles_with(
                                lambda service: Tag.SystemComponent in service.tags()
                                or Tag.RemoteAccess in service.tags()
                                or Tag.AdministrativeTool in service.tags()
                                or Tag.FileSharing in service.tags()
                            )
                        )
                    ),
                    account=-1,
                    online_guess_complexity=(online := random.uniform(50, 100)),
                    unseal_complexity=random.uniform(0, online),
                )
                account = InfraUserAccount(
                    owned_by=person_id,
                    nickname=f"{person.name}.{person.age}adm@{station}",
                    credential=credential,
                    capabilities=computed_caps,
                    domain_account=False,
                )

                yield [station], account, credential

            computed_caps = {}
            devices = set()
            for station in segment.admin_pcs:
                devices.add(station)
                ### user accs
                computed_caps[station] = [
                    Capability(
                        to=cap_type,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.EXACT,
                        on_device=station,
                    )
                    for cap_type in BASIC_USER_CAPS_LOCAL
                ]

            credential = Credential(
                devices=DeviceSetOption.SOME(devices),
                type=CredentialType.OS,
                services=ServiceSetOption.SOME(
                    set(
                        service_store.get_handles_with(
                            lambda service: Tag.SystemComponent in service.tags()
                            or Tag.RemoteAccess in service.tags()
                            or Tag.AdministrativeTool in service.tags()
                            or Tag.FileSharing in service.tags()
                        )
                    )
                ),
                account=-1,
                online_guess_complexity=(online := random.uniform(35, 85)),
                unseal_complexity=random.uniform(0, online),
            )
            account = InfraUserAccount(
                owned_by=person_id,
                nickname=f"{person.name}.{person.age}",
                credential=credential,
                capabilities=computed_caps,
                domain_account=False,
            )

            yield devices, account, credential

    else:  # insane pwd policy
        # admin on all work an admin stations with same credentials
        # user on admin stations with same cred
        computed_admin_caps = {}
        computed_user_caps = {}
        devices = set()
        user_devices = set()
        for segment in topology.handles.wrk:
            for station in segment.workstations:
                devices.add(station)
                computed_admin_caps[station] = [
                    Capability(
                        to=cap_type,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=station,
                    )
                    for cap_type in caps_template
                ]
            for station in segment.admin_pcs:
                devices.add(station)
                computed_admin_caps[station] = [
                    Capability(
                        to=cap_type,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=station,
                    )
                    for cap_type in caps_template
                ]

                user_devices.add(station)
                computed_user_caps[station] = [
                    Capability(
                        to=cap_type,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.EXACT,
                        on_device=station,
                    )
                    for cap_type in BASIC_USER_CAPS_LOCAL
                ]
        user_cred = Credential(
            devices=DeviceSetOption.SOME(user_devices),
            type=CredentialType.OS,
            services=ServiceSetOption.SOME(
                set(
                    service_store.get_handles_with(
                        lambda service: Tag.SystemComponent in service.tags()
                        or Tag.RemoteAccess in service.tags()
                        or Tag.AdministrativeTool in service.tags()
                        or Tag.FileSharing in service.tags()
                    )
                )
            ),
            account=-1,
            online_guess_complexity=(online := random.uniform(35, 85)),
            unseal_complexity=random.uniform(0, online),
        )
        user_acc = InfraUserAccount(
            owned_by=person_id,
            nickname=f"{person.name}.{person.age}",
            credential=user_cred,
            capabilities=computed_user_caps,
            domain_account=False,
        )

        yield user_devices, user_acc, user_cred

        admin_cred = Credential(
            devices=DeviceSetOption.SOME(devices),
            type=CredentialType.OS,
            services=ServiceSetOption.SOME(
                set(
                    service_store.get_handles_with(
                        lambda service: Tag.SystemComponent in service.tags()
                        or Tag.RemoteAccess in service.tags()
                        or Tag.AdministrativeTool in service.tags()
                        or Tag.FileSharing in service.tags()
                    )
                )
            ),
            account=-1,
            online_guess_complexity=(online := random.uniform(50, 100)),
            unseal_complexity=random.uniform(0, online),
        )
        admin_acc = InfraUserAccount(
            owned_by=person_id,
            nickname=f"{person.name}.{person.age}adm",
            credential=admin_cred,
            capabilities=computed_admin_caps,
            domain_account=False,
        )

        yield devices, admin_acc, admin_cred


def generate_server_admin_accounts(
    topology: Topology,
    person: Person,
    service_store: ServiceStore,
    caps_template: List[CapabilityType],
    person_id: PersonHandle,
) -> Generator[Tuple[DeviceHandle, InfraUserAccount, Credential], None, None]:
    for device in [
        topology.handles.dmz.vpn,
        topology.handles.dmz.email,
        topology.handles.dmz.public_services,
        topology.handles.srv.services,
        topology.handles.srv.db,
        topology.handles.srv.shares,
    ]:
        if device is not None:
            admin_caps = {
                device: [
                    Capability(
                        to=cap_type,
                        privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                        limit_type=LimitType.EXACT,
                        on_device=device,
                    )
                    for cap_type in caps_template
                ]
            }

            user_caps = {
                device: [
                    Capability(
                        to=cap_type,
                        privilege_limit=PrivilegeLevel.USER,
                        limit_type=LimitType.EXACT,
                        on_device=device,
                    )
                    for cap_type in BASIC_USER_CAPS_LOCAL
                ]
            }

            admin_cred = Credential(
                devices=DeviceSetOption.SOME({device}),
                type=CredentialType.OS,
                services=ServiceSetOption.SOME(
                    set(
                        service_store.get_handles_with(
                            lambda service: Tag.SystemComponent in service.tags()
                            or Tag.RemoteAccess in service.tags()
                            or Tag.AdministrativeTool in service.tags()
                            or Tag.FileSharing in service.tags()
                        )
                    )
                ),
                account=-1,
                online_guess_complexity=(online := random.uniform(50, 100)),
                unseal_complexity=random.uniform(0, online),
            )

            admin_acc = InfraUserAccount(
                owned_by=person_id,
                nickname=f"{person.name}.{person.age}adm@{device}",
                credential=admin_cred,
                capabilities=admin_caps,
                domain_account=False,
            )

            yield [device], admin_acc, admin_cred

            user_cred = Credential(
                devices=DeviceSetOption.SOME({device}),
                type=CredentialType.OS,
                services=ServiceSetOption.SOME(
                    set(
                        service_store.get_handles_with(
                            lambda service: Tag.SystemComponent in service.tags()
                            or Tag.RemoteAccess in service.tags()
                            or Tag.AdministrativeTool in service.tags()
                            or Tag.FileSharing in service.tags()
                        )
                    )
                ),
                account=-1,
                online_guess_complexity=(online := random.uniform(50, 100)),
                unseal_complexity=random.uniform(0, online),
            )

            user_acc = InfraUserAccount(
                owned_by=person_id,
                nickname=f"{person.name}.{person.age}@{device}",
                credential=user_cred,
                capabilities=user_caps,
                domain_account=False,
            )

            yield [device], user_acc, user_cred


def generate_ics_admin_account(
    topology: Topology, person: Person, service_store: ServiceStore
) -> Tuple[DeviceHandle, InfraUserAccount, Credential]:
    device = topology.handles.ics.master
    caps = {
        device: [
            Capability(
                to=cap_type,
                privilege_limit=PrivilegeLevel.USER,
                limit_type=LimitType.EXACT,
                on_device=device,
            )
            for cap_type in BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS + FULL_ADMIN_CAPS
        ]
    }

    credential = Credential(
        devices=DeviceSetOption.SOME({device}),
        type=CredentialType.OS,
        services=ServiceSetOption.SOME(
            set(
                service_store.get_handles_with(
                    lambda service: Tag.SystemComponent in service.tags()
                    or Tag.RemoteAccess in service.tags()
                    or Tag.AdministrativeTool in service.tags()
                    or Tag.FileSharing in service.tags()
                )
            )
        ),
        account=-1,
        online_guess_complexity=(online := random.uniform(50, 100)),
        unseal_complexity=random.uniform(0, online),
    )

    account = InfraUserAccount(
        owned_by=person_id,
        nickname=f"{person.name}.{person.age}_icsadm",
        credential=credential,
        capabilities=caps,
        domain_account=False,
    )
    return device, account, credential


def generate_domain_admin_account(
    topology: Topology, service_store: ServiceStore
) -> Tuple[List[DeviceHandle], InfraUserAccount, Credential]:
    computed_caps = {}
    devices = set(
        device
        for device in topology.store.handles
        if topology.store.get(device).segment_info in [SegmentType.DMZ, SegmentType.INTERNAL]
    )

    for device in devices:
        computed_caps[device] = [
            Capability(
                to=cap_type,
                privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                limit_type=LimitType.EXACT,
                on_device=device,
            )
            for cap_type in BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS + FULL_ADMIN_CAPS
        ]

    credential = Credential(
        devices=DeviceSetOption.SOME(devices),
        type=CredentialType.OS,
        services=ServiceSetOption.SOME(
            set(
                service_store.get_handles_with(
                    lambda service: Tag.SystemComponent in service.tags()
                    or Tag.RemoteAccess in service.tags()
                    or Tag.AdministrativeTool in service.tags()
                    or Tag.FileSharing in service.tags()
                )
            )
        ),
        account=-1,
        online_guess_complexity=(online := random.uniform(80, 100)),
        unseal_complexity=random.uniform(0, online),
    )
    account = InfraUserAccount(
        owned_by=None,
        nickname=f"domainAdm",
        credential=credential,
        capabilities=computed_caps,
        domain_account=True,
    )

    return devices, account, credential


# admin cap on *all* devices, one credential


def generate_service_accounts(
    topology: Topology,
) -> Generator[Tuple[Device, InfraUserAccount, InfraUserAccount], None, None]:
    for device in topology.store.get_all_with(lambda _: True):
        system_acc = InfraUserAccount(
            owned_by=None,
            nickname=f"{'NT_AUTHORITY/SYSTEM' if device.os.is_microsoft() else 'root'}@{device.name}",
            credential=None,
            capabilities={
                device.store_ref: [
                    Capability(
                        to=cap_typ,
                        privilege_limit=PrivilegeLevel.LOCAL_SYSTEM,
                        limit_type=LimitType.EXACT,
                        on_device=device.store_ref,
                    )
                    for cap_typ in SYSTEM_CAPS
                ]
            },
            domain_account=False,
        )

        service_acc = InfraUserAccount(
            owned_by=None,
            nickname=f"services@{device.name}",
            credential=None,
            capabilities={
                device.store_ref: [
                    Capability(
                        to=cap_typ,
                        privilege_limit=PrivilegeLevel.SERVICE,
                        limit_type=LimitType.EXACT,
                        on_device=device.store_ref,
                    )
                    for cap_typ in SERVICE_CAPS
                ]
            },
            domain_account=False,
        )
        yield device, system_acc, service_acc


def generate_developer_accounts(
    topology: Topology, person: Person, service_store: ServiceStore, person_id: PersonHandle
) -> Tuple[List[DeviceHandle], InfraUserAccount, Credential]:
    devices = set()
    caps = {}
    for device in [
        topology.handles.dmz.public_services,
        topology.handles.srv.services,
        topology.handles.srv.db,
    ]:
        if device is not None:
            devices.add(device)
            caps[device] = [
                Capability(
                    to=cap_typ,
                    privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                    limit_type=LimitType.EXACT,
                    on_device=device,
                )
                for cap_typ in BASIC_USER_CAPS_LOCAL
            ]

    credential = Credential(
        devices=DeviceSetOption.SOME(devices),
        type=CredentialType.OS,
        services=ServiceSetOption.SOME(
            set(
                service_store.get_handles_with(
                    lambda service: Tag.SystemComponent in service.tags()
                    or Tag.RemoteAccess in service.tags()
                    or Tag.AdministrativeTool in service.tags()
                    or Tag.FileSharing in service.tags()
                )
            )
        ),
        account=-1,
        online_guess_complexity=(online := random.uniform(80, 100)),
        unseal_complexity=random.uniform(0, online),
    )
    account = InfraUserAccount(
        owned_by=person_id,
        nickname=f"{person.name}_dev",
        credential=credential,
        capabilities=caps,
        domain_account=True,
    )

    return devices, account, credential


def generate_infra_accounts(
    topology: Topology,
    population: PersonStore,
    services: ServiceStore,
) -> Tuple[AccountStore, CredentialStore]:
    account_store = AccountStore()
    credential_store = CredentialStore()

    for device, system_acc, services_acc in generate_service_accounts(topology):
        system_acc_handle, _ = save_acc_and_cred(system_acc, account_store, None, credential_store)
        services_acc_handle, _ = save_acc_and_cred(services_acc, account_store, None, credential_store)
        device.add_services_account(services_acc_handle)
        device.add_system_account(system_acc_handle)
    if topology.domain_controlled:
        devices, domain_admin_acc, domain_admin_cred = generate_domain_admin_account(topology, services)
        domain_adm_handle, domain_adm_cred_handle = save_acc_and_cred(
            domain_admin_acc, account_store, domain_admin_cred, credential_store
        )
        for device in devices:
            topology.store.get(device).add_account(domain_adm_handle, domain_admin_acc)

    for person_id in population.get_handles_with(fn=lambda _: True):
        person = population.get(person_id)
        for role in person.it_roles:
            match role:
                case ITRole.BASIC_USER:
                    devices, acc, cred = generate_user_account(
                        topology, person, services, BASIC_USER_CAPS_LOCAL, person_id
                    )
                    acc_handle, _ = save_acc_and_cred(acc, account_store, cred, credential_store)
                    for device_handle in devices:
                        topology.store.get(device_handle).add_account(acc_handle, acc)
                    person.add_account(acc, acc_handle)
                case ITRole.ADVANCED_USER:
                    devices, acc, cred = generate_user_account(
                        topology, person, services, BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS, person_id
                    )
                    acc_handle, _ = save_acc_and_cred(acc, account_store, cred, credential_store)
                    for device_handle in devices:
                        topology.store.get(device_handle).add_account(acc_handle, acc)
                    person.add_account(acc, acc_handle)
                # case ITRole.VIP_USER: - difference is confidentiality of owned data, otherwise basic user. We won't us this in the MVP
                case ITRole.RESTRICTED_STATION_ADMIN:
                    for devices, acc, cred in generate_workstation_admin_accounts(
                        topology,
                        person,
                        services,
                        BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS,
                        person_id,
                    ):
                        acc_handle, _ = save_acc_and_cred(acc, account_store, cred, credential_store)
                        for device_handle in devices:
                            topology.store.get(device_handle).add_account(acc_handle, acc)
                        person.add_account(acc, acc_handle)
                case ITRole.RESTRICTED_SERVER_ADMIN:
                    for devices, acc, cred in generate_server_admin_accounts(
                        topology,
                        person,
                        services,
                        BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS,
                        person_id,
                    ):
                        acc_handle, _ = save_acc_and_cred(acc, account_store, cred, credential_store)
                        for device_handle in devices:
                            topology.store.get(device_handle).add_account(acc_handle, acc)
                        person.add_account(acc, acc_handle)
                case ITRole.STATION_ADMIN:
                    for devices, acc, cred in generate_workstation_admin_accounts(
                        topology,
                        person,
                        services,
                        BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS + FULL_ADMIN_CAPS,
                        person_id,
                    ):
                        acc_handle, _ = save_acc_and_cred(acc, account_store, cred, credential_store)
                        for device_handle in devices:
                            topology.store.get(device_handle).add_account(acc_handle, acc)
                        person.add_account(acc, acc_handle)
                case ITRole.SERVER_ADMIN:
                    for devices, acc, cred in generate_server_admin_accounts(
                        topology,
                        person,
                        services,
                        BASIC_USER_CAPS_LOCAL + ADVANCED_USER_CAPS + RESTRICTED_ADMIN_CAPS + FULL_ADMIN_CAPS,
                        person_id,
                    ):
                        acc_handle, _ = save_acc_and_cred(acc, account_store, cred, credential_store)
                        for device_handle in devices:
                            topology.store.get(device_handle).add_account(acc_handle, acc)
                        person.add_account(acc, acc_handle)
                case ITRole.DEVELOPER:
                    devices, acc, cred = generate_developer_accounts(topology, person, services, person_id)
                    acc_handle, _ = save_acc_and_cred(acc, account_store, cred, credential_store)
                    for device_handle in devices:
                        topology.store.get(device_handle).add_account(acc_handle, acc)
                    person.add_account(acc, acc_handle)
                case ITRole.ICS_ADMIN:
                    devices, acc, cred = generate_ics_admin_account(topology, person, services)
                    acc_handle, _ = save_acc_and_cred(acc, account_store, cred, credential_store)
                    for device_handle in devices:
                        topology.store.get(device_handle).add_account(acc_handle, acc)
                    person.add_account(acc, acc_handle)
                case ITRole.DOMAIN_ADMIN:
                    if topology.domain_controlled:
                        person.add_account(domain_admin_acc, domain_adm_handle)

    return account_store, credential_store


def save_acc_and_cred(acc, account_store, cred, credential_store) -> Tuple[AccountHandle, CredentialHandle]:
    acc_handle = account_store.add(acc)
    acc.finalize_credential(acc_handle)
    cred_handle = credential_store.add(cred)

    return acc_handle, cred_handle
