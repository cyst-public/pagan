import dataclasses

from pagan_rs.pagan_rs import EnablerType

from pagan.adversary import Quiver, QuiverStore
from pagan.shared.primitives.enablers import Enabler, EnablerLocality
from pagan.shared.primitives.primitives_sets import ServiceSetOption, ActionSetOption
from pagan.shared.primitives.types import EnablerImpact
from pagan.shared.stores.enabler import EnablerStore
from pagan.shared.stores.services import ServiceStore


@dataclasses.dataclass
class EnablerHandles:
    teamviewer_exploit: int
    winrm_auth_bypass: int
    sudo_privesc: int
    spoolsv_privesc: int
    printnightmare: int
    sudoers_bad_access: int
    ftp_exploit: int
    smb_credleak_exploit: int
    smb_anonymous: int
    eternalblue: int
    mysql_dos_exploit: int
    redis_exploit: int
    wireguard_privesc: int
    browser_cred_store_dump: int
    user_runs_malware: int
    nginx_exploit: int
    apache_exploit: int
    openvnc_exploit: int
    rdp_data_manipulation_exploit: int
    bruteforce_enabler: int
    ssh_exploit: int
    suid_misconfig: int


def generate_enablers(service_store: ServiceStore) -> EnablerStore:
    store = EnablerStore(_action_store_ref=QuiverStore, _service_store_ref=service_store)
    handles = EnablerHandles(
        ssh_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="made up",
                services=ServiceSetOption.SOME(
                    {
                        service_store.get_by_name("ssh"),
                    }
                ),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        bruteforce_enabler=store.add(
            Enabler(
                type=EnablerType.Cwe,
                description="",
                services=ServiceSetOption.SOME(
                    {
                        service_store.get_by_name("ssh"),
                        service_store.get_by_name("rdp"),
                        service_store.get_by_name("openVNC"),
                        service_store.get_by_name("smb"),
                        service_store.get_by_name("winRM"),
                        service_store.get_by_name("TeamViewer"),
                        service_store.get_by_name("redisStack"),
                        service_store.get_by_name("mysql"),
                    }
                ),
                actions=ActionSetOption.SOME({Quiver.credential_access.bruteforce}),
                locality=EnablerLocality.BOTH,
                impact={EnablerImpact.ALLOW},
            )
        ),
        rdp_data_manipulation_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="made up",
                services=ServiceSetOption.SOME(
                    {
                        service_store.get_by_name("rdp"),
                    }
                ),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_authed_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.DATA_TAMPERING},
            )
        ),
        openvnc_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="made up",
                services=ServiceSetOption.SOME(
                    {
                        service_store.get_by_name("openVNC"),
                    }
                ),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        apache_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="made up",
                services=ServiceSetOption.SOME(
                    {
                        service_store.get_by_name("apache-web"),
                    }
                ),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        nginx_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="made up",
                services=ServiceSetOption.SOME(
                    {
                        service_store.get_by_name("nginx"),
                    }
                ),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_authed_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.AUTH_MANIPULATION},
            )
        ),
        user_runs_malware=store.add(
            Enabler(
                type=EnablerType.UserError,
                description="",
                services=ServiceSetOption.SOME(
                    {
                        service_store.get_by_name("outlook"),
                        service_store.get_by_name("thunderbird"),
                        service_store.get_by_name("skype"),
                    }
                ),
                actions=ActionSetOption.SOME({Quiver.movement.phishing_with_malware}),
                locality=EnablerLocality.BOTH,
                impact={EnablerImpact.ALLOW},
            )
        ),
        browser_cred_store_dump=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="made up",
                services=ServiceSetOption.SOME(
                    {service_store.get_by_name("Edge"), service_store.get_by_name("Firefox")}
                ),
                actions=ActionSetOption.SOME({Quiver.credential_access.exploit_local_service}),
                locality=EnablerLocality.LOCAL,
                impact={EnablerImpact.ALLOW, EnablerImpact.CREDENTIAL_LEAK},
            )
        ),
        wireguard_privesc=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="made up",
                services=ServiceSetOption.SOME({service_store.get_by_name("wireguard")}),
                actions=ActionSetOption.SOME({Quiver.privilege_escalation.exploit}),
                locality=EnablerLocality.LOCAL,
                impact={EnablerImpact.ALLOW, EnablerImpact.CHANGE_MACHINE_CONFIGURATION},
            )
        ),
        redis_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2022-0543",
                services=ServiceSetOption.SOME({service_store.get_by_name("redisStack")}),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        mysql_dos_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2022-0543",
                services=ServiceSetOption.SOME({service_store.get_by_name("mysql")}),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.AVAILABILITY_VIOLATION},
            )
        ),
        eternalblue=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2017-0144",
                services=ServiceSetOption.SOME({service_store.get_by_name("smb")}),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        smb_anonymous=store.add(
            Enabler(
                type=EnablerType.Misconfiguration,
                description="anonymous samba logins enabled",
                services=ServiceSetOption.SOME({service_store.get_by_name("smb")}),
                actions=ActionSetOption.SOME({Quiver.movement.use_known_credentials}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW},
            )
        ),
        smb_credleak_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2017-0147 like",
                services=ServiceSetOption.SOME({service_store.get_by_name("smb")}),
                actions=ActionSetOption.SOME({Quiver.credential_access.exploit_remote_service}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.CREDENTIAL_LEAK},
            )
        ),
        ftp_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2019-19383",
                services=ServiceSetOption.SOME({service_store.get_by_name("freeFTP")}),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_authed_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        sudoers_bad_access=store.add(
            Enabler(
                type=EnablerType.Misconfiguration,
                description="bad access rights on sudo",
                services=ServiceSetOption.SOME(
                    {service_store.get_by_name("bash")}
                ),  # should be some notepad apps, but we will just grop those under bash
                actions=ActionSetOption.SOME({Quiver.privilege_escalation.tamper_sudoers}),
                locality=EnablerLocality.LOCAL,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        printnightmare=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2021-34527",
                services=ServiceSetOption.SOME({service_store.get_by_name("printspooler")}),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        spoolsv_privesc=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2022-21999",
                services=ServiceSetOption.SOME({service_store.get_by_name("printspooler")}),
                actions=ActionSetOption.SOME({Quiver.privilege_escalation.exploit}),
                locality=EnablerLocality.LOCAL,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        sudo_privesc=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2022-21999",
                services=ServiceSetOption.SOME({service_store.get_by_name("sudo")}),
                actions=ActionSetOption.SOME({Quiver.privilege_escalation.exploit}),
                locality=EnablerLocality.LOCAL,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        winrm_auth_bypass=store.add(
            Enabler(
                type=EnablerType.Cwe,
                description="Improper access control implementation",
                services=ServiceSetOption.SOME({service_store.get_by_name("winRM")}),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        teamviewer_exploit=store.add(
            Enabler(
                type=EnablerType.Cve,
                description="CVE-2021-34859",
                services=ServiceSetOption.SOME({service_store.get_by_name("TeamViewer")}),
                actions=ActionSetOption.SOME({Quiver.movement.exploit_open_application}),
                locality=EnablerLocality.REMOTE,
                impact={EnablerImpact.ALLOW, EnablerImpact.ARBITRARY_CODE_EXECUTION},
            )
        ),
        suid_misconfig=store.add(
            Enabler(
                type=EnablerType.Misconfiguration,
                description="suid allowed on system",
                services=ServiceSetOption.SOME({service_store.get_by_name("customapp")}),
                actions=ActionSetOption.SOME({Quiver.privilege_escalation.suid_executables}),
                locality=EnablerLocality.LOCAL,
                impact={EnablerImpact.ALLOW},
            )
        ),
    )

    return store, handles
