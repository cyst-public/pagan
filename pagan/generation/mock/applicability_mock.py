from dataclasses import dataclass
from typing import List, Optional

from pagan.adversary import Quiver
from pagan.generation.mock.enablers_mock import EnablerHandles
from pagan.shared.primitives.primitives_sets import ServiceVulnerabilitySetOption, ServiceSetOption
from pagan.shared.primitives.types import ServiceHandle, ActionHandle, EnablerHandle
from pagan.shared.stores.services import ServiceStore


@dataclass
class ApplicabilityEntry:
    service: ServiceHandle
    action: ActionHandle
    enabler: Optional[EnablerHandle]


@dataclass
class ApplicabilityMatrix:
    entries: List[ApplicabilityEntry]

    def get(self, action: ActionHandle) -> ServiceVulnerabilitySetOption:
        matching = set()
        for item in self.entries:
            if item.action == action:
                matching.add((item.service, item.enabler if item.enabler else -1))

        return ServiceVulnerabilitySetOption.SOME(matching)

    def get_services(self, action: ActionHandle) -> ServiceSetOption:
        matching = set()
        for item in self.entries:
            if item.action == action:
                matching.add(item.service)

        return ServiceSetOption.SOME(matching)


def get_applicabilities(service_store: ServiceStore, enabler_handles: EnablerHandles) -> ApplicabilityMatrix:
    return ApplicabilityMatrix(
        entries=[
            ApplicabilityEntry(
                service=service_store.get_by_name("ssh"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.ssh_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("ssh"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("ssh"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("ssh"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(service=service_store.get_by_name("ssh"), action=Quiver.impact.resource_exhaustion, enabler=None),
            ApplicabilityEntry(
                service=service_store.get_by_name("rdp"),
                action=Quiver.movement.exploit_authed_application,
                enabler=enabler_handles.rdp_data_manipulation_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("rdp"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("rdp"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("rdp"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(service=service_store.get_by_name("rdp"), action=Quiver.impact.resource_exhaustion, enabler=None),
            ApplicabilityEntry(
                service=service_store.get_by_name("openVNC"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.openvnc_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("openVNC"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("openVNC"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("openVNC"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("rdp"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(service=service_store.get_by_name("openVNC"), action=Quiver.impact.resource_exhaustion, enabler=None),
            ApplicabilityEntry(
                service=service_store.get_by_name("nginx"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.nginx_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("nginx"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("apache-web"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.apache_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("apache-web"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("outlook"),
                action=Quiver.movement_preparation.create_internal_phishing_mail_as_client,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("outlook"),
                action=Quiver.movement.phishing_with_malware,
                enabler=enabler_handles.user_runs_malware,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("thunderbird"),
                action=Quiver.movement_preparation.create_internal_phishing_mail_as_client,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("thunderbird"),
                action=Quiver.movement.phishing_with_malware,
                enabler=enabler_handles.user_runs_malware,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("Firefox"),
                action=Quiver.credential_access.exploit_local_service,
                enabler=enabler_handles.browser_cred_store_dump,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("Edge"),
                action=Quiver.credential_access.exploit_local_service,
                enabler=enabler_handles.browser_cred_store_dump,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("wireguard"),
                action=Quiver.privilege_escalation.exploit,
                enabler=enabler_handles.wireguard_privesc,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("skype"),
                action=Quiver.movement_preparation.create_internal_phishing_mail_as_client,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("skype"),
                action=Quiver.movement.phishing_with_malware,
                enabler=enabler_handles.user_runs_malware,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("redisStack"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.redis_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("redisStack"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("redisStack"),
                action=Quiver.impact.manipulate_data,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("redisStack"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("mysql"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.mysql_dos_exploit,
            ),  # TODO: here this is not actual movement, redo the exploit i guess
            ApplicabilityEntry(
                service=service_store.get_by_name("mysql"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("mysql"),
                action=Quiver.impact.manipulate_data,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("mysql"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("smb"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.eternalblue,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("smb"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("smb"),
                action=Quiver.movement.use_known_credentials,
                enabler=enabler_handles.smb_anonymous,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("smb"), action=Quiver.discovery, enabler=None
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("smb"),
                action=Quiver.credential_access.exploit_remote_service,
                enabler=enabler_handles.smb_credleak_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("smb"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("smb"),
                action=Quiver.impact.manipulate_data,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("smb"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("freeFTP"),
                action=Quiver.movement.exploit_authed_application,
                enabler=enabler_handles.ftp_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("freeFTP"),
                action=Quiver.impact.manipulate_data,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("freeFTP"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("freeFTP"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("docker-daemon"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("powershell"),
                action=Quiver.discovery,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("powershell"),
                action=Quiver.impact.manipulate_data,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("powershell"),
                action=Quiver.privilege_escalation.create_system_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("bash"),
                action=Quiver.discovery,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("bash"),
                action=Quiver.impact.manipulate_data,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("bash"),
                action=Quiver.privilege_escalation.tamper_sudoers,
                enabler=enabler_handles.sudoers_bad_access,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("printspooler"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.printnightmare,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("printspooler"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("printspooler"),
                action=Quiver.privilege_escalation.exploit,
                enabler=enabler_handles.spoolsv_privesc,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("printspooler"),
                action=Quiver.privilege_escalation.create_system_service,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("sudo"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("sudo"),
                action=Quiver.privilege_escalation.exploit,
                enabler=enabler_handles.sudo_privesc,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("customapp"),
                action=Quiver.privilege_escalation.suid_executables,
                enabler=enabler_handles.suid_misconfig,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("winRM"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.winrm_auth_bypass,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("winRM"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("winRM"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("TeamViewer"),
                action=Quiver.movement.exploit_open_application,
                enabler=enabler_handles.teamviewer_exploit,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("TeamViewer"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("TeamViewer"),
                action=Quiver.credential_access.bruteforce,
                enabler=enabler_handles.bruteforce_enabler,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("TeamViewer"),
                action=Quiver.impact.stop_service,
                enabler=None,
            ),
            ApplicabilityEntry(service=service_store.get_by_name("TeamViewer"), action=Quiver.impact.resource_exhaustion, enabler=None),
            ApplicabilityEntry(
                service=service_store.get_by_name("winlogon"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
            ApplicabilityEntry(
                service=service_store.get_by_name("lightdm"),
                action=Quiver.movement.use_known_credentials,
                enabler=None,
            ),
        ]
    )
