from typing import Any, Dict

from pagan.shared.primitives.capabilities import PrivilegeLevel
from pagan.adversary import Quiver
from pagan.shared.primitives.services import Service
from pagan.shared.primitives.types import ActionGuard

MOCK_DATA: Dict[str, Dict[str, Any]] = {
    "ssh": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),  # not needed as we do not allow suid on this service
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.exploit_authed_application,
                    Quiver.movement.exploit_open_application,
                    Quiver.impact.stop_service,
                    Quiver.credential_access.bruteforce,
                ],
            )
        },
    },
    "rdp": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.use_known_credentials,
                    Quiver.movement.exploit_authed_application,
                    Quiver.movement.exploit_open_application,
                    Quiver.impact.stop_service,
                ],
            )
        },
    },
    "vnc": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.use_known_credentials,
                    Quiver.movement.exploit_authed_application,
                    Quiver.movement.exploit_open_application,
                    Quiver.impact.stop_service,
                ],
            )
        },
    },
    "nginx": {
        "accessible_to": PrivilegeLevel.SERVICE,
        "autoelevate": False,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.exploit_authed_application,
                    Quiver.movement.exploit_open_application,
                    Quiver.impact.stop_service,
                    Quiver.credential_access.bruteforce,
                ],
            )
        },
    },
    "apache-web": {
        "accessible_to": PrivilegeLevel.SERVICE,
        "autoelevate": False,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.exploit_authed_application,
                    Quiver.movement.exploit_open_application,
                    Quiver.impact.stop_service,
                    Quiver.credential_access.bruteforce,
                ],
            )
        },
    },
    "outlook": {
        "accessible_to": PrivilegeLevel.USER,
        "autoelevate": False,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement_preparation.create_internal_phishing_mail_as_client,
                    Quiver.movement.phishing_with_malware,
                    Quiver.credential_access.phishing,
                ],
            )
        },
    },
    "thunderbird": {
        "accessible_to": PrivilegeLevel.USER,
        "autoelevate": False,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement_preparation.create_internal_phishing_mail_as_client,
                    Quiver.movement.phishing_with_malware,
                    Quiver.credential_access.phishing,
                ],
            )
        },
    },
    "msoffice": {
        "accessible_to": PrivilegeLevel.USER,
        "autoelevate": False,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement_preparation.taint_shared_content,
                    Quiver.movement.pure_lateral.with_tainted_content,
                    Quiver.credential_access.read_unsecure_store,
                ],
            )
        },
    },
    "firefox": {"accessible_to": PrivilegeLevel.USER, "autoelevate": False, "impact": frozenset()},
    "edge": {"accessible_to": PrivilegeLevel.USER, "autoelevate": False, "impact": frozenset()},
    "skype": {"accessible_to": PrivilegeLevel.USER, "autoelevate": False, "impact": frozenset()},
    "redisStack": {
        "accessible_to": PrivilegeLevel.SERVICE,
        "autoelevate": False,
        "impact": frozenset(),
    },
    "mysql": {"accessible_to": PrivilegeLevel.USER, "autoelevate": False, "impact": frozenset()},
    "smb": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
    },
    "freeFTP": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
    },
    "powershell": {
        "accessible_to": PrivilegeLevel.USER,
        "autoelevate": False,
        "impact": frozenset(),
    },
    "bash": {
        "accessible_to": PrivilegeLevel.USER,
        "autoelevate": False,
        "impact": frozenset(),
    },
    "printspooler": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
    },
    "sudo": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": False,
        "impact": frozenset(),
    },
    "custom_app_with_admin_privesc": {
        "accessible_to": PrivilegeLevel.USER,
        "autoelevate": False,
        "impact": frozenset(),
    },
    "telnet": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.use_known_credentials,
                    Quiver.movement.exploit_authed_application,
                    Quiver.movement.exploit_open_application,
                    Quiver.impact.stop_service,
                    Quiver.credential_access.bruteforce,
                ],
            )
        },
    },
    "WinRM": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.use_known_credentials,
                    Quiver.movement.exploit_authed_application,
                    Quiver.movement.exploit_open_application,
                    Quiver.impact.stop_service,
                ],
            )
        },
    },
    "TeamViewer": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": False,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.use_known_credentials,
                    Quiver.movement.exploit_authed_application,
                    Quiver.movement.exploit_open_application,
                    Quiver.impact.stop_service,
                ],
            )
        },
    },
    "winlogon": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.use_known_credentials,
                ],
            )
        },
    },
    "lightdm": {
        "accessible_to": PrivilegeLevel.ADMINISTRATOR,
        "autoelevate": True,
        "impact": frozenset(),
        "applicable_actions": {
            ActionGuard(
                guard=Service.is_process,
                actions=[
                    Quiver.movement.use_known_credentials,
                ],
            )
        },
    },
}
