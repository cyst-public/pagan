import random
import copy
from uuid import UUID

from dataclasses import dataclass, field
from collections import defaultdict
from typing import (
    Dict,
    FrozenSet,
    Generator,
    Iterable,
    List,
    Optional,
    Self,
    Set,
    Tuple,
    Union,
)
from enum import Enum, auto
from chainingiterator import Chi
from pagan.generation.scenario.path_tree import PathTree

from pagan.shared.exceptions import ImpossibleScenarioPathError, ProgramLogicError
from pagan.shared.primitives.actions import (
    CapabilitySpecifier,
    CredentialTarget,
    PrivilegeOrigin,
    UsageScheme,
)
from pagan.shared.primitives.capabilities import (
    AttackerCapability,
    CapabilityTemplate,
    CapabilityType,
    LimitType,
)
from pagan.shared.primitives.credentials import (
    BaseCredential,
    Credential,
    CredentialType,
    ImplementsCredentialTemplateCheck,
    WildcardCredential,
)
from pagan.shared.primitives.set_option import SetOptionLen
from pagan.shared.primitives.types import EnablerImpact, EdgeKey
from pagan.shared.primitives.primitives_sets import (
    AccountSetOption,
    DeviceSetOption,
    ServiceAndVulnerability,
    ServiceSetOption,
    ServiceVulnerabilitySetOption,
)
from pagan.shared.primitives.tokens import Token, TokenLocationSpecifier, TokenType
from pagan.shared.primitives.types import (
    ActionHandle,
    DeviceHandle,
    NodeHandle,
    ServiceHandle,
    AccountHandle,
    AccessType,
)
from pagan.generation.scenario.scenario_automaton import (
    EdgeData,
    NodeData,
    ScenarioStateMachine,
)
from pagan.shared.primitives.accounts import InfraUserAccount
from pagan.shared.scenario_config import ScenarioConfig
from pagan.shared.stores.accounts import AccountStore
from pagan.shared.capability_computation import CapabilityComputationEngine
from pagan_rs.pagan_rs import ProcessIntegrityLevel, PrivilegeLevel
from pagan_rs.pagan_rs import Impact as ServiceImpact


def service_impacts_to_capabilities(impacts: List[ServiceImpact]) -> List[CapabilityType]:
    capabilities: List[CapabilityType] = []
    for impact in impacts:
        if impact == ServiceImpact.ReadFile:
            capabilities.extend(
                [
                    CapabilityType.ACCESS_FILESYS_GENERIC,
                    CapabilityType.ACCESS_FILESYS_ELEVATED,
                    CapabilityType.ACCESS_FILESYS_SHARED,
                    CapabilityType.FILESYS_WRITE,
                ]
            )
        elif impact == ServiceImpact.WriteFile:
            capabilities.extend(
                [
                    CapabilityType.ACCESS_FILESYS_GENERIC,
                    CapabilityType.ACCESS_FILESYS_ELEVATED,
                    CapabilityType.ACCESS_FILESYS_SHARED,
                    CapabilityType.FILESYS_WRITE,
                ]
            )
        elif impact == ServiceImpact.ExecuteCode:
            for cap in CapabilityType:
                if cap != CapabilityType.PERSISTENCE:
                    capabilities.append(cap)

    return capabilities


class PathSuccessState(Enum):
    SUCCESSFUL = auto()
    FAILED = auto()
    CUT_BACK = auto()

    @classmethod
    def combine(cls, accumulator: Self, elem: Self) -> Self:
        if accumulator == cls.SUCCESSFUL or elem == cls.SUCCESSFUL:
            return cls.SUCCESSFUL
        if accumulator == cls.FAILED and elem == cls.FAILED:
            return cls.FAILED
        return cls.CUT_BACK


@dataclass
class _WildcardUse:
    generation: List[NodeHandle] = field(init=False, default_factory=list)
    usages: List[NodeHandle] = field(init=False, default_factory=list)

    def set_generation(self, handle: NodeHandle) -> None:
        self.generation.append(handle)


@dataclass
class _BuildingEcho:
    state: PathSuccessState
    subtrees: List[Self]  # TODO: find the exact DS for this


@dataclass
class ScenarioBuilder:
    configuration: ScenarioConfig
    _state_machine: ScenarioStateMachine = field(init=False, default_factory=ScenarioStateMachine)
    _built: bool = field(init=False, default=False)
    _echo_cache: Dict[NodeHandle, _BuildingEcho] = field(init=False, default_factory=dict)
    _wildcard_uses: Dict[UUID, _WildcardUse] = field(init=False, default_factory=lambda: defaultdict(_WildcardUse))
    _wildcards: Dict[WildcardCredential, WildcardCredential] = field(
        init=False, default_factory=dict
    )  # imagine having to do this because sets suck
    _accepting_states: Set[NodeHandle] = field(init=False, default_factory=set)
    _paths: List[List[NodeHandle]] = field(init=False, default_factory=list)
    _edge_keys: Dict[Tuple[NodeHandle, NodeHandle], List[int]] = field(
        init=False, default_factory=lambda: defaultdict(list)
    )

    def get_or_insert_wildcard(self, wc: WildcardCredential) -> WildcardCredential:
        if (registered := self._wildcards.get(wc, None)) is not None:
            return registered
        self._wildcards[wc] = wc
        return wc

    @dataclass
    class ChildInfo:
        edge_data: EdgeData
        node_data: NodeData

    class Utils:
        @staticmethod
        def filtered_capabilities_for_device(
            capabilities: Iterable[AttackerCapability], device: DeviceHandle
        ) -> Set[AttackerCapability]:
            return Chi(capabilities).filter(lambda cap: cap.on_device is None or cap.on_device == device).collect(set)

        @staticmethod
        def filtered_tokens_for_device(tokens: Iterable[Token], device: DeviceHandle) -> Set[Token]:
            return Chi(tokens).filter(lambda t: t.device is None or t.device == device).collect(set)

        @staticmethod
        def filtered_credentials_for_device(
            credentials: Iterable[ImplementsCredentialTemplateCheck],
            device: DeviceHandle,
        ) -> Set[ImplementsCredentialTemplateCheck]:
            return (
                Chi(credentials)
                .filter(lambda c: c.fits_template(WildcardCredential(devices=DeviceSetOption.SOME({device}))))
                .collect(set)
            )

        @staticmethod
        def choose_best_capabilities(
            attacker_capabilities: Set[AttackerCapability], scheme: UsageScheme
        ) -> Optional[Tuple[AccountHandle, Set[AttackerCapability]]]:
            caps_by_user: Dict[AccountHandle, Set[AttackerCapability]] = defaultdict(set)
            for acap in attacker_capabilities:
                if any(cap.matching(acap) for cap in scheme.required_capabilities):
                    caps_by_user[handle if (handle := acap.as_user) is not None else -1].add(
                        acap
                    )  # TODO: sanity check if this can happen, if not remove optional from AttackerCap
            usable_caps = dict(
                Chi(caps_by_user.items())
                .filter(lambda caps: CapabilityComputationEngine.is_applicable_scheme(scheme, caps[1]))
                .collect(dict)
            )

            best = -1
            m = 0.0
            for (
                user,
                caps,
            ) in (
                usable_caps.items()
            ):  # this is really simple, we probably want some mapping of capabilities to their usefulness
                # ATM leave it as it is, most accounts have all their caps on the same level
                # also it is natural to have more useful privileges with higher priv levels in genreal
                if (avg := sum(map(lambda cap: cap.privilege_limit, caps)) / len(caps)) > m:
                    best = user
                    m = avg
            return best, usable_caps.get(best, None)

        @staticmethod
        def choose_best_tokens(
            scheme: UsageScheme,
            usable_tokens: Set[Token],
            services: ServiceSetOption,
        ) -> Tuple[Set[Token], ServiceSetOption]:
            full_intersection = copy.copy(services)
            res = set()
            for token_type in TokenType.decompose(scheme.given):
                best_set = ServiceSetOption.NONE()
                best_token = Token(type=TokenType.NONE, device=None)
                for token in filter(lambda t: t.type == token_type, usable_tokens):
                    intersection = services.intersection(token.services)
                    if intersection.is_none():
                        continue
                    elif intersection.is_all():
                        best_token = token
                        break
                    elif intersection.is_some():
                        # TODO: EXTEND more complex comparisons over the actual services in the future (owners, suid, ...)
                        if intersection.size() > best_set.size():
                            best_set = intersection
                            best_token = token
                    else:
                        raise ProgramLogicError("Invalid ServiceSetOption instance.")
                if best_set.is_none():
                    raise ImpossibleScenarioPathError(
                        "No token was found with non-empty intersection to action's service set."
                    )
                res.add(best_token)
                full_intersection = full_intersection.intersection(best_token.services)
                if full_intersection.is_none():
                    raise ImpossibleScenarioPathError(
                        "The combination of tokens has no intersection with the action's services."
                    )
            return res, full_intersection

        @staticmethod
        def choose_credentials(
            user_store: AccountStore,
            scheme: UsageScheme,
            creds: Set[ImplementsCredentialTemplateCheck],
            device: DeviceHandle,
            services: ServiceSetOption,
        ) -> Tuple[Optional[Set[ImplementsCredentialTemplateCheck]], ServiceSetOption]:
            # TODO: this is not ready for MFA atm, as we need to implement some kind of relation between those primary
            #  and MFA creds belonging to each other

            if not TokenType.CREDENTIAL.present_in(scheme.given) and not TokenType.CREDENTIAL_2FA_TOKEN.present_in(
                scheme.given
            ):
                return None, services
            filtered_creds = (
                Chi(creds)
                .filter(
                    lambda cred: cred.fits_template(
                        WildcardCredential(
                            accounts=AccountSetOption.ALL(),
                            devices=DeviceSetOption.SOME({device}),
                            type=CredentialType.UNSPEC,
                            services=services,
                        )
                    )
                )
                .collect(list)
            )
            if scheme.privilege_origin != PrivilegeOrigin.CREDENTIAL:
                # the caps the attacker gains does not depend on the cred (the only option that did not match the
                # above is SERVICE (as no other is used for actions with required credentials)) - so we just choose one random cred
                # TODO: the above is a hella dangerous assumption for being future-proof
                return {random.choice(filtered_creds)} if filtered_creds else None, services.intersection(
                    Chi(filtered_creds).foldl(
                        accumulator=ServiceSetOption.NONE(),
                        func=lambda acc, elem: acc.union(elem.services),
                    )
                )

            # else: ( in all these scenarios the attacker inherits a full set of privileges of some user)
            # TODO: RECONSIDER
            #  we will do this the greedy way for the MVP, we choose the cred that gives us the most powerful user on
            #  the actual device, we wont try to look into the future for now, (we define most powerful as best CODE_EXEC)
            # in the future for example with stealth as a priority, we might want to login as a less privileged
            # user and then do elevation
            best_cred = None
            highest_privs = PrivilegeLevel.Service
            for cred, account_handle in (
                Chi(filtered_creds)
                .map(
                    lambda cred: (cred, cred.account)
                    if isinstance(cred, BaseCredential)
                    else [(cred, account) for user in cred.accounts]
                )
                .flatten(stop_condition=lambda elem: isinstance(elem, Tuple))
            ):
                if not isinstance(account := user_store.get(account_handle), InfraUserAccount):
                    continue
                cap_level = (
                    Chi(account.capabilities)
                    .filter(
                        lambda cap: cap.on_device == device and cap.to == CapabilityType.EXECUTE_CODE
                    )  # TODO: is this really what we want to check?
                    .map(lambda cap: cap.privilege_level)
                    .max()
                )
                if cap_level >= highest_privs:
                    highest_privs = cap_level
                    best_cred = cred
            if best_cred is None:
                raise ImpossibleScenarioPathError("No suitable credential found.")

            return {best_cred} if best_cred is not None else None, services.intersection(best_cred.services)

    def __post_init__(self) -> None:
        if self._state_machine.ROOT != (
            ret := self._state_machine.add_node(
                NodeData(
                    device=self.configuration.topology.handles.untrusted_internet,
                    attacker_capabilities=frozenset(),
                    attacker_tokens=frozenset(self.configuration.available_tokens),
                    available_credentials=frozenset(self.configuration.available_credentials),
                )
            )
        ):
            raise ProgramLogicError(
                f"Auto-adding attacker node did not return the expected static NodeHandle (ROOT = 0), but {ret}"
            )

    @property
    def automaton(self) -> ScenarioStateMachine:
        if self._built:
            return self._state_machine
        raise ProgramLogicError("Attack state machine has not been built yet.")

    @property
    def accepting_states(self) -> List[NodeHandle]:
        if self._built:
            return self._accepting_states
        raise ProgramLogicError("Scenario is not yet built.")

    @property
    def edge_keys(self) -> Dict[Tuple[NodeHandle, NodeHandle], List[int]]:
        return self._edge_keys

    @property
    def paths(self) -> List[List[NodeHandle]]:
        return self._paths

    def build(self) -> Self:
        tree = PathTree(self.configuration.goal.device)
        tree.build(
            self.configuration.topology.accesses.elems,
            max_depth=self.configuration.max_devices_touched,
            start_device=self.configuration.topology.handles.untrusted_internet,
            notouch=[
                self.configuration.topology.handles.attacker,
                self.configuration.topology.handles.untrusted_internet,
                self.configuration.topology.handles.trusted_internet,
            ],
        )
        # tha below part will be a for cycle over all paths in the future
        # device_path = random.choice(list(tree.paths))
        device_path = sorted(list(tree.paths), key=lambda path: len(path))[0]
        self._build_for_path_start(device_path)
        self._built = True
        self.automaton.freeze()
        return self

    def _add_to_automaton(self, old_handle: NodeHandle, child: ChildInfo) -> NodeHandle:
        new_handle = self._state_machine.add_node(data=child.node_data)
        edge_key = self._state_machine.add_edge(
            from_handle=old_handle,
            to_handle=new_handle,
            transition_guard=child.edge_data,
        )
        self._edge_keys[(old_handle, new_handle)].append(edge_key)
        if child.node_data.accepting_state:
            self._accepting_states.add(new_handle)
        return new_handle

    def _build_for_path_start(self, device_path: List[DeviceHandle]) -> None:
        root = self._state_machine.ROOT
        _echo = self._build_for_path_step(
            node_handle=root, device_path=device_path, depth=0, path_index=0, no_remain=True, history=[]
        )

        _actual_state_data = self._state_machine.query_node_data(root)
        _actual_state_metadata = self._state_machine.query_node_processing_state(root)

        print(_actual_state_metadata.finished_for)
        print("successful scenario" if _echo.state == PathSuccessState.SUCCESSFUL else "unsuccesful scenario")

        # TODO: sanity check, success?
        # TODO: issue - echoes are created for each path and if the same state is visited from another device path
        # the echo might change locally in the state, but is not propagated back in the direction of the beginnig state
        # the root state itself should be fine, becaouse it is part of every branch, arbitrary states are not the same tho

    def _build_for_path_step(
        self,
        node_handle: NodeHandle,
        device_path: List[DeviceHandle],
        depth: int,
        path_index: int,  # due to repeating of devices on the path, we need the precise index
        history: List[NodeHandle],
        no_remain: bool = False,  # enforce moving to another device - needed for the attacker machine @ step 1
    ) -> _BuildingEcho:
        history.append(node_handle)
        actual_node = node_handle
        actual_state = self._state_machine.query_node_data(actual_node)
        actual_node_metadata = self._state_machine.query_node_processing_state(actual_node)
        if actual_state.accepting_state:
            self._paths.append(history.copy())
        # print(f"Processing node: {node_handle}, depth: {depth}, for path: {device_path}")
        if depth > self.configuration.max_action_count or actual_state.accepting_state:
            return _BuildingEcho(
                state=PathSuccessState.SUCCESSFUL if actual_state.accepting_state else PathSuccessState.CUT_BACK,
                subtrees=list(),
            )

        if actual_node_metadata.is_finished_for(tuple(device_path[path_index + 1 :])):
            return self._echo_cache[node_handle]
            # TODO::future improvement: skip to first state that is related to a not-yet explored device

        next_device = None
        children_iter = Chi([])
        echoes_archive: List[_BuildingEcho] = list()

        if not no_remain:
            next_device = actual_state.device
            children_iter = Chi(
                self._fork_on_available_actions(actual_state=actual_state, next_device=next_device)
            ).map(lambda fork: (fork[0], fork[1], path_index))

        if path_index + 1 < len(device_path):
            # otherwise moving on, prepare your stomach
            next_device = device_path[path_index + 1]
            children_iter.chain(
                Chi(self._fork_on_available_actions(actual_state=actual_state, next_device=next_device)).map(
                    lambda fork: (fork[0], fork[1], path_index + 1)
                )
            )  # add states from moving to next device
        echo_state = (
            children_iter.map(
                lambda fork_wildcreds_path_idx: (
                    (new_handle := self._add_to_automaton(old_handle=node_handle, child=fork_wildcreds_path_idx[0])),
                    fork_wildcreds_path_idx[2],
                    Chi(fork_wildcreds_path_idx[1]).foreach(
                        lambda wc: self._wildcard_uses[wc.id].set_generation(new_handle)
                    ),
                )
            )  # add each state to automaton, add wildcard creation info, return handles
            .filter(lambda handle_path_idx: handle_path_idx[0] != node_handle)
            .map(
                lambda handle_path_idx: self._build_for_path_step(
                    node_handle=handle_path_idx[0],
                    device_path=device_path,
                    depth=depth + 1,
                    path_index=handle_path_idx[1],
                    history=history.copy(),
                )
            )  # recurse with building for all
            .map(lambda echo: (echoes_archive.append(echo), echo.state)[-1])  # save echoes, and mine states
            .foldl(
                accumulator=PathSuccessState.FAILED,
                func=PathSuccessState.combine,
                stop_condition=None,
            )  # combine states
        )

        self._state_machine.finish_processing(handle=node_handle, devices=device_path[path_index + 1 :])
        old_echo = self._echo_cache.get(node_handle, None)
        new_echo = (
            _BuildingEcho(state=echo_state, subtrees=echoes_archive)
            if old_echo is None
            else _BuildingEcho(
                state=PathSuccessState.combine(old_echo.state, echo_state),
                subtrees=old_echo.subtrees + echoes_archive,
            )
        )
        self._echo_cache[node_handle] = new_echo
        return new_echo

    def _fork_on_available_actions(
        self, actual_state: NodeData, next_device: DeviceHandle
    ) -> Generator[Tuple[ChildInfo, Set[WildcardCredential]], None, None]:
        usable_tokens = ScenarioBuilder.Utils.filtered_tokens_for_device(actual_state.attacker_tokens, next_device)
        usable_capabilities = ScenarioBuilder.Utils.filtered_capabilities_for_device(
            actual_state.attacker_capabilities, next_device
        )
        usable_credentials = ScenarioBuilder.Utils.filtered_credentials_for_device(
            actual_state.available_credentials, next_device
        )

        available_actions = self.configuration.action_store.get_applicable(
            tokens=usable_tokens, capabilities=usable_capabilities
        )

        for action, use_index, scheme in available_actions:
            as_user, used_capabilities = ScenarioBuilder.Utils.choose_best_capabilities(usable_capabilities, scheme)
            available_services = self.configuration.action_service_applicability.get_services(action).except_of(
                self.configuration.topology.store.get(next_device).prohibited_services,
                case_all=self.configuration.service_store.handles,
            )
            used_tokens = None
            used_credentials = None

            try:
                (
                    used_tokens,
                    available_services,
                ) = ScenarioBuilder.Utils.choose_best_tokens(scheme, usable_tokens, available_services)
                (
                    used_credentials,
                    available_services,
                ) = ScenarioBuilder.Utils.choose_credentials(
                    self.configuration.user_account_store,
                    scheme,
                    usable_credentials,
                    next_device,
                    available_services,
                )
            except ImpossibleScenarioPathError as e:
                # print(e, action, scheme)
                continue  # failed route, nothing to add to automaton
            try:
                for option in self._fork_on_specific_scheme(
                    action_handle=action,
                    scheme=scheme,
                    available_services=available_services,
                ):
                    yield self._compute_new_state_and_transition(
                        action=action,
                        scheme=scheme,
                        scheme_index=use_index,
                        used_credentials=used_credentials,
                        runas=as_user,
                        services_and_enablers=option,
                        old_state=actual_state,
                        target_device=next_device,
                        actual_device=actual_state.device,
                    )
            except ImpossibleScenarioPathError as e:
                # print(e, action, scheme)
                continue  # failed route, nothing to add to automaton

    def _fork_on_specific_scheme(
        self,
        action_handle: ActionHandle,
        scheme: UsageScheme,
        available_services: ServiceSetOption,
    ) -> Generator[Union[ServiceVulnerabilitySetOption, ServiceSetOption], None, None]:
        if available_services == ServiceSetOption.NONE:
            return

        # case A: no credential or capability granted
        if not TokenType.CAPABILITY.present_in(  # also matches CAPABILITY_ALL
            scheme.grant
        ) and not TokenType.CREDENTIAL.present_in(scheme.grant):
            if scheme.required_enabler is None:
                yield available_services

            else:  # enabler needed
                service_enabler_pairs = set()
                for service in available_services.as_iterator(case_all=self.configuration.service_store.handles):
                    for enabler in self.configuration.enabler_store.get_handles_for_service(service).intersection(
                        self.configuration.enabler_store.get_handles_for_action(action_handle)
                    ):
                        if self.configuration.enabler_store.get(enabler).matches_proto(scheme.required_enabler):
                            service_enabler_pairs.add(ServiceAndVulnerability(service, enabler))
                yield ServiceVulnerabilitySetOption.SOME(service_enabler_pairs) if len(
                    service_enabler_pairs
                ) > 0 else ServiceVulnerabilitySetOption.NONE()

        # case B: credential granted but no capability
        elif not TokenType.CAPABILITY.present_in(  # also matches CAPABILITY_ALL
            scheme.grant
        ) and TokenType.CREDENTIAL.present_in(scheme.grant):
            if scheme.required_enabler is None:
                yield available_services

            else:  # enabler needed
                service_enabler_pairs = set()
                for service in available_services.as_iterator(case_all=self.configuration.service_store.handles):
                    for enabler in self.configuration.enabler_store.get_handles_for_service(service).intersection(
                        self.configuration.enabler_store.get_handles_for_action(action_handle)
                    ):
                        if self.configuration.enabler_store.get(enabler).matches_proto(scheme.required_enabler):
                            service_enabler_pairs.add(ServiceAndVulnerability(service, enabler))

                yield ServiceVulnerabilitySetOption.SOME(service_enabler_pairs) if len(
                    service_enabler_pairs
                ) > 0 else ServiceVulnerabilitySetOption.NONE()

        # case C: capability and no credential
        elif TokenType.CAPABILITY.present_in(  # also matches CAPABILITY_ALL
            scheme.grant
        ) and not TokenType.CREDENTIAL.present_in(scheme.grant):
            if scheme.required_enabler is None:
                if (
                    scheme.capability_specifier != CapabilitySpecifier.SERVICE
                ):  # NONE or ACTION -  can't be ENABLER as UsageScheme::__post_init__ would panic
                    if scheme.privilege_origin == PrivilegeOrigin.SERVICE:
                        buckets = defaultdict(set)
                        for service in available_services.as_iterator(
                            case_all=self.configuration.service_store.handles
                        ):
                            key = self.configuration.service_store.get(service).get_effective_privilege()
                            buckets[key].add(service)
                        yield from Chi(buckets.values()).map(lambda bucket: ServiceSetOption.SOME(bucket))
                    else:
                        yield available_services
                else:
                    buckets: Dict[
                        Union[
                            Tuple[CapabilityType],
                            Tuple[
                                CapabilityType,
                                Union[PrivilegeLevel, ProcessIntegrityLevel],
                            ],
                        ],
                        Set[ServiceHandle],
                    ] = defaultdict(set)
                    for service in available_services.as_iterator(case_all=self.configuration.service_store.handles):
                        key = (
                            tuple(self.configuration.service_store.get(service).impacts())
                            if scheme.privilege_origin != PrivilegeOrigin.SERVICE
                            else tuple(self.configuration.service_store.get(service).impacts())
                            + tuple(self.configuration.service_store.get(service).get_effective_privilege())
                        )
                        buckets[key].add(service)
                    yield from Chi(buckets.values()).map(lambda bucket: ServiceSetOption.SOME(bucket))

            else:  # enabler needed
                service_enabler_pairs: Set[ServiceAndVulnerability] = set()
                for service in available_services.as_iterator(case_all=self.configuration.service_store.handles):
                    for enabler in self.configuration.enabler_store.get_handles_for_service(service).intersection(
                        self.configuration.enabler_store.get_handles_for_action(action_handle)
                    ):
                        if self.configuration.enabler_store.get(enabler).matches_proto(scheme.required_enabler):
                            service_enabler_pairs.add(ServiceAndVulnerability(service, enabler))
                if scheme.capability_specifier == CapabilitySpecifier.SERVICE:
                    buckets: Dict[
                        Union[
                            Tuple[CapabilityType],
                            Tuple[
                                CapabilityType,
                                Union[PrivilegeLevel, ProcessIntegrityLevel],
                            ],
                        ],
                        Set[ServiceHandle],
                    ] = defaultdict(set)

                    for service_and_vulnerability in service_enabler_pairs:
                        service = service_and_vulnerability.service
                        enabler = service_and_vulnerability.enabler
                        key = (
                            tuple(map(int, self.configuration.service_store.get(service).impacts()))
                            if scheme.privilege_origin != PrivilegeOrigin.SERVICE
                            else tuple(map(int, self.configuration.service_store.get(service).impacts()))
                            + tuple(self.configuration.service_store.get(service).get_effective_privilege())
                        )
                        buckets[key].add(service_and_vulnerability)
                    yield from Chi(buckets.values()).map(lambda bucket: ServiceVulnerabilitySetOption.SOME(bucket))

                elif scheme.capability_specifier == CapabilitySpecifier.ENABLER:
                    buckets: Dict[
                        Union[
                            Tuple[EnablerImpact],
                            Tuple[
                                EnablerImpact,
                                Union[PrivilegeLevel, ProcessIntegrityLevel],
                            ],
                        ],
                        Set[ServiceAndVulnerability],
                    ] = defaultdict(set)

                    for service_and_vulnerability in service_enabler_pairs:
                        service = service_and_vulnerability.service
                        enabler = service_and_vulnerability.enabler
                        key = (
                            tuple(self.configuration.enabler_store.get(enabler).impact)
                            if scheme.privilege_origin != PrivilegeOrigin.SERVICE
                            else tuple(self.configuration.enabler_store.get(enabler).impact)
                            + tuple(self.configuration.service_store.get(service).get_effective_privilege())
                        )
                        buckets[key].add(service_and_vulnerability)
                    yield from Chi(buckets.values()).map(lambda bucket: ServiceVulnerabilitySetOption.SOME(bucket))
                else:
                    if scheme.privilege_origin != PrivilegeOrigin.SERVICE():
                        yield ServiceVulnerabilitySetOption.SOME(service_enabler_pairs)
                    else:
                        buckets = defaultdict(set)

                        for service_and_vulnerability in service_enabler_pairs:
                            service = service_and_vulnerability.service
                            enabler = service_and_vulnerability.enabler
                            key = self.configuration.service_store.get(service).get_effective_privilege()
                            buckets[key].add(service_and_vulnerability)
                        yield from Chi(buckets.values()).map(lambda bucket: ServiceVulnerabilitySetOption.SOME(bucket))
                    # yield ServiceVulnerabilitySetOption.SOME(service_enabler_pairs)
        # case D: capability and credential - does not exist in the adversary framework
        else:
            raise ProgramLogicError("Action granting both CREDENTIAL and CAPABILITies should not exits ATM")

    def _compute_new_state_and_transition(
        self,
        action: ActionHandle,
        scheme: UsageScheme,
        scheme_index: int,
        used_credentials: Set[Credential],
        runas: AccountHandle,
        services_and_enablers: Union[ServiceSetOption, ServiceVulnerabilitySetOption],
        old_state: NodeData,
        target_device: DeviceHandle,
        actual_device: DeviceHandle,
    ) -> Tuple[ChildInfo, Set[WildcardCredential]]:
        new_tokens = set()
        new_capabilities = set()
        new_credentials = set()
        new_runas = runas
        wildcard_credentials = set()

        # these branchings should handle "side effects"
        if TokenType.CAPABILITY.present_in(scheme.grant):
            try:
                service_representant_idx = (
                    next(services_and_enablers.as_iterator(case_all=self.configuration.service_store.handles))
                    if services_and_enablers.inner_type() == ServiceHandle
                    else next(services_and_enablers.as_iterator_notall()).service
                    # only if its ServiceAndVuln - and we had to create that manually -> no way we got the ALL option
                )
            except StopIteration:
                raise ImpossibleScenarioPathError("No services to apply action on")

            if scheme.privilege_origin == PrivilegeOrigin.SERVICE():
                if services_and_enablers.size() <= SetOptionLen.INTEGER(0):
                    raise ProgramLogicError("Privilege defined by service but there is none available.")
                target_privilege = self.configuration.service_store.get(
                    service_representant_idx
                ).get_effective_privilege()
                if target_privilege == PrivilegeLevel.LocalSystem:
                    new_runas = self.configuration.topology.store.get(target_device).system_account
                elif target_privilege == PrivilegeLevel.Service:
                    new_runas = self.configuration.topology.store.get(target_device).service_account
                elif target_privilege == PrivilegeLevel.User or target_privilege == PrivilegeLevel.Administrator:
                    if not self.configuration.action_store.get(action).remote:
                        if target_privilege == PrivilegeLevel.User:
                            new_runas = runas
                            # TODO: after MVP reconsider whether we want user switching on the same capability layer
                        if target_privilege == PrivilegeLevel.Administrator:
                            (
                                new_runas,
                                success,
                            ) = self.configuration.user_account_store.uac_elevate(runas)
                            if not success:
                                print("UAC elevation not succesful, continuing without account change")
                    else:
                        if target_privilege == PrivilegeLevel.User:
                            new_runas = self.configuration.topology.store.get(target_device).get_random_user_account()
                        if target_privilege == PrivilegeLevel.Administrator:
                            new_runas = self.configuration.topology.store.get(target_device).get_random_admin_account()
                else:
                    ProgramLogicError("Invalid privilege level with PrivilegeOrigin.SERVICE")
            elif scheme.privilege_origin == PrivilegeOrigin.CREDENTIAL():
                new_runas = next(iter(used_credentials)).get_account(
                    limit_to=self.configuration.topology.store.get(target_device).get_user_accounts()
                )
            elif scheme.privilege_origin == PrivilegeOrigin.RANDOM_USER():
                new_runas = self.configuration.topology.store.get(target_device).get_random_user_account()
            elif scheme.privilege_origin == PrivilegeOrigin.CREATED_USER():
                raise NotImplementedError(
                    "User creation by an attacker is not yet implemented"
                )  # TODO: handle user creation
            elif scheme.privilege_origin == PrivilegeOrigin.INHERIT_USER():
                new_runas = runas
            elif scheme.privilege_origin == PrivilegeOrigin.ELEVATE_SPLIT_TOKEN():
                (
                    new_runas,
                    elevation_success,
                ) = self.configuration.user_account_store.uac_elevate(runas)
                if not elevation_success:
                    print("UAC elevation not succesful, continuing without account change")  # TODO: logging
            elif scheme.privilege_origin == PrivilegeOrigin.ADVERSARY_CHOICE():
                new_runas = self.configuration.topology.store.get(
                    target_device
                ).system_account  # TODO: reconsider after MVP
            elif scheme.privilege_origin == PrivilegeOrigin.FIXED(PrivilegeLevel.LocalSystem):
                new_runas = self.configuration.topology.store.get(target_device).system_account
            elif scheme.privilege_origin == PrivilegeOrigin.FIXED(PrivilegeLevel.Administrator):
                (
                    new_runas,
                    elevation_success,
                ) = self.configuration.user_account_store.uac_elevate(
                    runas
                )  # TODO: reconsider after MVP, atm no action uses this
            elif scheme.privilege_origin == PrivilegeOrigin.FIXED(PrivilegeLevel.User):
                new_runas = runas  # TODO: reconsider after MVP, atm no action uses this
            elif scheme.privilege_origin == PrivilegeOrigin.FIXED(PrivilegeLevel.Service):
                new_runas = self.configuration.topology.store.get(target_device).service_account

            capability_satchel = Chi(
                CapabilityComputationEngine.all_inherited_from_user(
                    new_runas,
                    self.configuration.user_account_store.get(new_runas),
                    target_device,
                )
            )

            if TokenType.CAPABILITY_ALL.present_in(scheme.grant):
                new_capabilities = capability_satchel.collect(set)
            else:
                inherited_capability_types: FrozenSet[CapabilityType] = frozenset()
                if scheme.capability_specifier == CapabilitySpecifier.ACTION:
                    for capability in scheme.provided_capabilities:
                        new_capabilities.add(
                            AttackerCapability(
                                to=capability.to,
                                privilege_limit=self.configuration.user_account_store.get(
                                    new_runas
                                ).get_privilege_level(target_device),
                                limit_type=LimitType.EXACT,
                                on_device=target_device,
                                as_user=new_runas,
                            )
                        )
                elif scheme.capability_specifier == CapabilitySpecifier.SERVICE:
                    inherited_capability_types = frozenset(
                        service_impacts_to_capabilities(
                            self.configuration.service_store.get(service_representant_idx).impacts()
                        )
                    )
                elif scheme.capability_specifier == CapabilitySpecifier.ENABLER:
                    enabler_representant_idx = next(services_and_enablers.as_iterator_notall()).enabler
                    inherited_capability_types = (
                        Chi(self.configuration.enabler_store.get(enabler_representant_idx).impact)
                        .map(lambda impact: impact.to_capability_types())
                        .flatten(stop_condition=lambda res: isinstance(res, CapabilityType))
                        .collect(frozenset)
                    )
                else:
                    raise ProgramLogicError("Missing capability specifiers")
                new_capabilities = capability_satchel.filter(
                    lambda atkcap: atkcap.to in inherited_capability_types
                ).collect(set)

            for capability in scheme.provided_capabilities:
                new_capabilities.add(
                    AttackerCapability(
                        to=capability.to,
                        privilege_limit=self.configuration.user_account_store.get(new_runas).get_privilege_level(
                            target_device
                        ),
                        limit_type=LimitType.EXACT,
                        on_device=target_device,
                        as_user=new_runas,
                    )
                )

        if TokenType.CREDENTIAL.present_in(scheme.grant) and not TokenType.CREDENTIAL_2FA_TOKEN.present_in(
            scheme.grant
        ):
            if scheme.credential_target == CredentialTarget.OS_ALL_LOCAL:
                new_credentials = new_credentials.union(
                    set(
                        self.configuration.topology.store.get(target_device).get_local_os_credentials(
                            self.configuration.user_account_store
                        )
                    )
                )
            elif scheme.credential_target == CredentialTarget.OS_CACHED:
                new_credentials.add(
                    WildcardCredential(
                        accounts=self.configuration.topology.store.get(target_device).get_user_accounts(),
                        type=CredentialType.OS,
                    )
                )
            elif scheme.credential_target == CredentialTarget.OS_ACTIVE_ACCOUNT:
                cred = self.configuration.topology.store.get(target_device).get_active_os_account_credential(
                    self.configuration.user_account_store, runas
                )
                if cred is not None:
                    new_credentials.append(cred)
            elif scheme.credential_target == CredentialTarget.SERVICE_SELF_ALL:
                new_credentials = new_credentials.union(
                    set(
                        self.configuration.credential_store.get_all_with(
                            lambda cred: cred.fits_template(
                                WildcardCredential(
                                    accounts=AccountSetOption.ALL(),
                                    devices=DeviceSetOption.SOME({target_device}),
                                    type=scheme.credential_type,
                                    services=services_and_enablers.get_service_set(),
                                )
                            )
                        )
                    )
                )
            elif scheme.credential_target == CredentialTarget.SERVICE_SELF:
                new_credentials = new_credentials.union(
                    set(
                        self.configuration.credential_store.get_all_with(
                            lambda cred: cred.fits_template(
                                WildcardCredential(
                                    accounts=AccountSetOption.ALL(),
                                    devices=DeviceSetOption.SOME({target_device}),
                                    type=scheme.credential_type,
                                    services=services_and_enablers.get_service_set(),
                                )
                            )
                            and cred.online_guess_complexity
                            <= self.configuration.adversary.allowed_online_guess_complexity
                        )
                    )
                )
            elif (
                scheme.credential_target == CredentialTarget.SERVICE_OTHER
            ):  # unused ATM - so ANY_ACTUAL_USER is copied here
                accounts = set(
                    self.configuration.user_account_store.get(runas).get_related_accounts(
                        self.configuration.people_store
                    )
                )
                new_credentials.add(
                    wc := self.get_or_insert_wildcard(
                        WildcardCredential(
                            accounts=AccountSetOption.SOME(accounts) if accounts else AccountSetOption.NONE(),
                            type=CredentialType.ANY_PRIMARY,
                        )
                    )
                )
                wildcard_credentials.add(wc)
            elif scheme.credential_target == CredentialTarget.MIMIC_USER:
                raise ProgramLogicError("MIMIC option of credential target should be used for 2fa only actions")
            elif scheme.credential_target == CredentialTarget.MIMIC_ALL:
                raise ProgramLogicError("MIMIC option of credential target should be used for 2fa only actions")
            elif scheme.credential_target == CredentialTarget.ANY_ACTUAL_USER:
                accounts = set(
                    self.configuration.user_account_store.get(runas).get_related_accounts(
                        self.configuration.people_store
                    )
                )
                new_credentials.add(
                    wc := self.get_or_insert_wildcard(
                        WildcardCredential(
                            accounts=AccountSetOption.SOME(accounts) if accounts else AccountSetOption.NONE(),
                            type=CredentialType.ANY_PRIMARY,
                        )
                    )
                )
                wildcard_credentials.add(wc)
            elif scheme.credential_target == CredentialTarget.ANY_PRESENT_USERS:
                accounts = set(
                    self.configuration.topology.store.get(actual_device).get_associated_accounts(
                        self.configuration.user_account_store
                    )
                )
                new_credentials.add(
                    wc := self.get_or_insert_wildcard(
                        WildcardCredential(
                            accounts=AccountSetOption.SOME(accounts) if accounts else AccountSetOption.NONE(),
                            type=CredentialType.ANY_PRIMARY,
                        )
                    )
                )
                wildcard_credentials.add(wc)
            else:
                raise ProgramLogicError("Non existing CredentialTarget option")

        if TokenType.CREDENTIAL_2FA_TOKEN.present_in(scheme.grant) and not TokenType.CREDENTIAL.present_in(
            scheme.grant
        ):
            if scheme.credential_target == CredentialTarget.OS_ALL_LOCAL:
                raise ProgramLogicError(f"option {scheme.credential_target} invalid for MFA")
            elif scheme.credential_target == CredentialTarget.OS_CACHED:
                raise ProgramLogicError(f"option {scheme.credential_target} invalid for MFA")
            elif scheme.credential_target == CredentialTarget.OS_ACTIVE_ACCOUNT:
                raise ProgramLogicError(f"option {scheme.credential_target} invalid for MFA")
            elif scheme.credential_target == CredentialTarget.SERVICE_SELF_ALL:
                new_credentials = new_credentials.union(
                    set(
                        self.configuration.credential_store.get_all_with(
                            lambda cred: cred.fits_template(
                                WildcardCredential(
                                    accounts=AccountSetOption.ALL(),
                                    devices=DeviceSetOption.SOME({target_device}),
                                    type=CredentialType.MFA,
                                    services=services_and_enablers.get_service_set(),
                                )
                            )
                        )
                    )
                )
            elif scheme.credential_target == CredentialTarget.SERVICE_SELF:
                raise ProgramLogicError(f"option {scheme.credential_target} invalid for pure MFA")

            elif scheme.credential_target == CredentialTarget.SERVICE_OTHER:
                # we assume all mfa codes are available in this service'
                # TODO: check whether here a wildcard should be added instead of matching creds, also with related accounts
                new_credentials = new_credentials.union(
                    set(
                        self.configuration.credential_store.get_all_with(
                            lambda cred: cred.fits_template(
                                WildcardCredential(
                                    accounts=AccountSetOption.SOME({new_runas}),
                                    devices=DeviceSetOption.ALL(),
                                    type=CredentialType.MFA,
                                    services=ServiceSetOption.ALL(),
                                )
                            )
                        )
                    )
                )
            elif scheme.credential_target == CredentialTarget.MIMIC_USER:
                # we assume all mfa codes are available in this service
                # TODO: check if this should be with all related accounts
                new_credentials = new_credentials.union(
                    self.configuration.credential_store.get_all_with(
                        lambda cred: cred.fits_template(
                            WildcardCredential(
                                accounts=AccountSetOption.SOME({next(iter(used_credentials)).account}),
                                devices=DeviceSetOption.ALL(),
                                type=CredentialType.MFA,
                                services=ServiceSetOption.ALL(),
                            )
                        )
                    )
                )
            elif scheme.credential_target == CredentialTarget.MIMIC_ALL:
                raise NotImplementedError  # TODO - this has to be done here - mimic user, service, device
            elif scheme.credential_target == CredentialTarget.ANY_ACTUAL_USER:
                raise NotImplementedError  # TODO: ATM unused
            elif scheme.credential_target == CredentialTarget.ANY_PRESENT_USERS:
                raise NotImplementedError  # TODO: ATM unused
            else:
                raise ProgramLogicError("Non existing CredentialTarget option")

        if TokenType.CREDENTIAL.present_in(scheme.grant) and TokenType.CREDENTIAL_2FA_TOKEN.present_in(scheme.grant):
            # we need to separate this option as here we want the actual pairs of multi factor creds that work together
            if scheme.credential_target == CredentialTarget.OS_ALL_LOCAL:
                raise ProgramLogicError(f"option {scheme.credential_target} invalid for MFA")
            elif scheme.credential_target == CredentialTarget.OS_CACHED:
                raise ProgramLogicError(f"option {scheme.credential_target} invalid for MFA")
            elif scheme.credential_target == CredentialTarget.OS_ACTIVE_ACCOUNT:
                raise ProgramLogicError(f"option {scheme.credential_target} invalid for MFA")
            elif scheme.credential_target == CredentialTarget.SERVICE_SELF_ALL:
                new_credentials = new_credentials.union(
                    set(
                        self.configuration.credential_store.get_all_with(
                            lambda cred: cred.fits_template(
                                WildcardCredential(
                                    accounts=AccountSetOption.ALL(),
                                    devices=DeviceSetOption.SOME({target_device}),
                                    type=scheme.credential_type,
                                    services=services_and_enablers.get_service_set(),
                                )
                            )
                        )
                    )
                )
            elif scheme.credential_target == CredentialTarget.SERVICE_SELF:
                new_credentials = new_credentials.union(
                    set(
                        self.configuration.credential_store.get_all_with(
                            lambda cred: cred.fits_template(
                                WildcardCredential(
                                    accounts=AccountSetOption.ALL(),
                                    devices=DeviceSetOption.SOME({target_device}),
                                    type=scheme.credential_type,
                                    services=services_and_enablers.get_service_set(),
                                )
                            )
                            and cred.online_guess_complexity
                            <= self.configuration.adversary.allowed_online_guess_complexity
                        )
                    )
                )
            elif scheme.credential_target == CredentialTarget.SERVICE_OTHER:
                # unused ATM - so ANY_ACTUAL_USER is copied here
                accounts = set(
                    self.configuration.user_account_store.get(runas).get_related_accounts(
                        self.configuration.people_store
                    )
                )
                new_credentials.add(
                    wc := self.get_or_insert_wildcard(
                        WildcardCredential(
                            accounts=AccountSetOption.SOME(accounts) if accounts else AccountSetOption.NONE(),
                            type=CredentialType.ANY_PRIMARY,
                        )
                    )
                )
                wildcard_credentials.add(wc)
            elif scheme.credential_target == CredentialTarget.MIMIC_USER:
                raise ProgramLogicError("MIMIC option of credential target should be used for 2fa only actions")
            elif scheme.credential_target == CredentialTarget.MIMIC_ALL:
                raise ProgramLogicError("MIMIC option of credential target should be used for 2fa only actions")

            elif scheme.credential_target == CredentialTarget.ANY_ACTUAL_USER:
                accounts = set(
                    self.configuration.user_account_store.get(runas).get_related_accounts(
                        self.configuration.people_store
                    )
                )
                new_credentials.add(
                    wc := self.get_or_insert_wildcard(
                        WildcardCredential(
                            accounts=AccountSetOption.SOME(accounts) if accounts else AccountSetOption.NONE(),
                            type=CredentialType.APP_ANY_PASSABLE,
                        )
                    )
                )
                wildcard_credentials.add(wc)
            elif scheme.credential_target == CredentialTarget.ANY_PRESENT_USERS:
                accounts = set(
                    self.configuration.topology.store.get(actual_device).get_associated_accounts(
                        self.configuration.user_account_store
                    )
                )
                new_credentials.add(
                    wc := self.get_or_insert_wildcard(
                        WildcardCredential(
                            accounts=AccountSetOption.SOME(accounts) if accounts else AccountSetOption.NONE(),
                            type=CredentialType.APP_ANY_PASSABLE,
                        )
                    )
                )
                wildcard_credentials.add(wc)
            else:
                raise ProgramLogicError("Non existing CredentialTarget option")

        # add the tokens themselves
        for token_type in TokenType:
            if not token_type.present_in(scheme.grant):
                continue
            if (
                token_type == TokenType.SESSION
                and Chi(new_capabilities).filter(lambda cap: cap.to == CapabilityType.EXECUTE_CODE).count() < 1
            ):
                continue
                # we cannot give session if the action does not grant some sort of code execution

            if token_type == TokenType.ACCESS_VIA_NETWORK:
                for device_handle, _ in self.configuration.topology.accesses.get_from(
                    target_device, [AccessType.NETWORK]
                ):
                    new_tokens.add(Token(type=TokenType.ACCESS_VIA_NETWORK, device=device_handle))
            if token_type == TokenType.SESSION:
                # this should be done only if session is valid -> we know that after checking for CE capability !!!!!
                # move this to where session tokens are computed
                for device_handle, access_type in self.configuration.topology.accesses.get_from(
                    target_device, [AccessType.MEDIA, AccessType.SHARED_FILESYS]
                ):
                    new_tokens.add(
                        Token(
                            type=TokenType.ACCESS_VIA_REMOVABLE_MEDIA
                            if access_type == AccessType.MEDIA
                            else TokenType.ACCESS_VIA_SHARED_FILESYS,
                            device=device_handle,
                        )
                    )
                if (target_device_instance := self.configuration.topology.store.get(target_device)).virtualized:
                    new_tokens.add(Token(type=TokenType.CONTAINERIZED, device=target_device))
                if not target_device_instance.usb_access:
                    new_tokens.add(Token(type=TokenType.NO_USB_ACCESS, device=target_device))
                new_tokens.add(
                    Token(
                        type=target_device_instance.get_segment_token_type(),
                        device=None,
                    )
                )
            if (target_spec := Token.get_target_location_specifier(token_type)) == TokenLocationSpecifier.MARGINAL:
                continue

            if token_type != TokenType.CREDENTIAL or token_type != TokenType.CREDENTIAL_2FA_TOKEN:
                new_tokens.add(
                    Token(
                        type=token_type,
                        device=None if target_spec == TokenLocationSpecifier.UNIVERSAL else target_device,
                    )
                )
            if token_type == TokenType.CREDENTIAL or token_type == TokenType.CREDENTIAL_2FA_TOKEN:
                for credential in new_credentials:
                    for device in credential.devices.as_iterator(case_all=self.configuration.topology.store.handles):
                        new_tokens.add(Token(type=token_type, device=device))

        full_capabilities = new_capabilities.union(old_state.attacker_capabilities)
        full_tokens = new_tokens.union(old_state.attacker_tokens)

        accepting = self._check_if_accepting(
            device=target_device,
            action=action,
            tokens=full_tokens,
            capabilities=full_capabilities,
        )

        new_state = NodeData(
            device=target_device,
            attacker_capabilities=frozenset(full_capabilities),
            attacker_tokens=frozenset(full_tokens),
            available_credentials=frozenset(new_credentials.union(old_state.available_credentials)),
            accepting_state=accepting,
        )
        transition = EdgeData(
            action=action,
            scheme_idx=scheme_index,
            service_enabler_pairs=services_and_enablers,
            runas=runas if not scheme.privilege_origin == PrivilegeOrigin.CREDENTIAL else new_runas,
            creds=used_credentials.copy() if used_credentials is not None else set(),
        )
        return (
            self.ChildInfo(node_data=new_state, edge_data=transition),
            wildcard_credentials,
        )

    def _check_if_accepting(
        self,
        device: DeviceHandle,
        action: ActionHandle,
        tokens: Set[Token],
        capabilities: Set[CapabilityTemplate],
    ) -> bool:
        res = self.configuration.goal.conforming(device=device, action=action, tokens=tokens, capabilities=capabilities)

        return res
