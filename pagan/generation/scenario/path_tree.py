from chainingiterator import Chi
from dataclasses import dataclass, field
from typing import Dict, Generator, Iterator, List, Optional, Tuple

from pagan.shared.primitives.types import AccessType, DeviceHandle


@dataclass
class DevicePathListView:
    """
    Utility clas that wraps a device path and locks it from iterator
    flattening by being atomic for iteration.
    """

    path: List[DeviceHandle]


@dataclass
class _PathNode:
    depth: int
    value: DeviceHandle
    children: List["_PathNode"] = field(init=False, default_factory=list)

    def report_path(self, path: List[DeviceHandle], goal: DeviceHandle) -> Generator[DevicePathListView, None, None]:
        path_copy = path.copy()
        path_copy.append(self.value)
        if not self.children:
            yield DevicePathListView(path_copy)
            return
        if self.value == goal:
            yield DevicePathListView(path_copy)
        yield from Chi(self.children).map(lambda ch: ch.report_path(path_copy, goal))

    def extend_path(
        self,
        accesses: Dict[DeviceHandle, List[Tuple[DeviceHandle, AccessType]]],
        max_depth: int,
        goal: DeviceHandle,
        actual_path: List[DeviceHandle],
        attacker: DeviceHandle,
        notouch: List[DeviceHandle],
    ) -> bool:
        if self.depth > max_depth:
            return False
        if self.depth == max_depth and self.value != goal:
            return False
        if self.depth == max_depth and self.value == goal:
            return True
        moves = (
            Chi(actual_path)
            .chain(Chi(accesses[self.value]).filter(lambda tup: tup[1] == AccessType.NETWORK).map(lambda tup: tup[0]))
            .filter(lambda dev: dev != self.value and dev != attacker and dev not in notouch)
            .collect(set)
        )
        children_candidates = Chi(moves).map(lambda oid: _PathNode(depth=self.depth + 1, value=oid))
        self.children = children_candidates.filter(
            lambda child: child.extend_path(
                accesses, max_depth, goal, Chi(actual_path).chain([self.value]).collect(list), attacker, notouch
            )
            is True
        ).collect(list)
        if len(self.children) != 0 or self.value == goal:
            return True
        return False


@dataclass
class PathTree:
    goal: DeviceHandle
    root: Optional[_PathNode] = field(init=False, default=None)

    @property
    def paths(self) -> Iterator[List[DeviceHandle]]:
        if self.root is None:
            return
        yield from Chi(self.root.report_path(list(), self.goal)).flatten().map(lambda view: view.path)

    def build(
        self,
        accesses: Dict[DeviceHandle, List[Tuple[DeviceHandle, AccessType]]],
        max_depth: int,
        start_device: DeviceHandle,
        notouch: List[DeviceHandle],
    ) -> bool:
        self.root = _PathNode(depth=0, value=start_device)
        return self.root.extend_path(accesses, max_depth, self.goal, [], start_device, notouch)
