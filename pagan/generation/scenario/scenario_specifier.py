import random
from collections import defaultdict

import networkx as nx

from pagan.generation.preparation.topology import Topology
from pagan.generation.scenario.scenario_automaton import EdgeData, NodeData
from pagan.generation.scenario.scenariobuilder import ScenarioBuilder
from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.primitives_sets import (
    ServiceAndVulnerability,
    ServiceSetOption,
    ServiceVulnerabilitySetOption,
)
from pagan.shared.primitives.types import ServiceHandle

from chainingiterator import Chi


class ScenarioSpecifier:
    def __init__(self, scenario: ScenarioBuilder, topology: Topology) -> None:
        self.scenario = scenario
        self.topology = topology
        for device in self.topology.store.get_all_with(lambda _: True):
            device.services = device.services.union(device.compulsory_services)

    def do_specify(self) -> None:
        # print("accepting states", self.scenario.accepting_states)
        # goal_node = random.choice(list(self.scenario.accepting_states))
        # chosen_path = random.choice(
        #     (paths := list(nx.all_simple_paths(self.scenario.automaton.graph, self.scenario.automaton.ROOT, goal_node)))
        # )
        chosen_path = random.choice(self.scenario.paths)
        self.scenario.automaton.chosen_goal_node = chosen_path[-1]
        # self.scenario.automaton.paths = nx.all_simple_paths(
        #     self.scenario.automaton.graph, self.scenario.automaton.ROOT, goal_node
        # )
        # print(paths)
        print("path was chosen: ", chosen_path)
        path_iter_forward = iter(chosen_path)
        next(path_iter_forward)
        for u, v in zip(chosen_path, path_iter_forward):
            k = random.choice(self.scenario.edge_keys[(u, v)])
            transition_data: EdgeData = self.scenario.automaton.graph.edges[u, v, k]["transition_guard"]
            if (se := transition_data.service_enabler_pairs.random_choice()) is None:
                continue

            node_data = self.scenario.automaton.query_node_data(v)
            device = self.topology.store.get(node_data.device)
            print("------------>")
            print(transition_data)
            print("------------>")

            print("==============")
            print(node_data)
            print("==============")

            if isinstance(se, ServiceAndVulnerability):
                device.services = device.services.union(ServiceSetOption.SOME({se.service}))
                device.enablers = device.enablers.union(ServiceVulnerabilitySetOption.SOME({se}))
            elif isinstance(se, int):
                device.services = device.services.union(ServiceSetOption.SOME({se}))
        print("enriching done")
        traverse_rules = dict()
        to_remove = []
        for u, v, k, data in self.scenario.automaton.edges(data=True):
            edge_data: EdgeData = data["transition_guard"]
            node_data: NodeData = self.scenario.automaton.graph.nodes[v]["state"]
            se = edge_data.service_enabler_pairs
            device = self.topology.store.get(node_data.device)
            if se.inner_type() is ServiceAndVulnerability:
                # traverse_rules[(u, v, k)] = True if not se.intersection(device.enablers).is_none() else False
                if se.intersection(device.enablers).is_none():
                    to_remove.append((u, v, k))
            elif se.inner_type() is ServiceHandle:
                # traverse_rules[(u, v, k)] = True if not se.intersection(device.services).is_none() else False
                if se.intersection(device.services).is_none():
                    to_remove.append((u, v, k))
            else:
                raise ProgramLogicError("transition guard has invalid service & enablers type")
        # nx.set_edge_attributes(self.scenario.automaton.graph, name="traversable", values=traverse_rules)
        self.scenario.automaton.graph.remove_edges_from(to_remove)
        print("cutting graph(edges) done")
        non_reachable = [
            node
            for node in self.scenario.automaton.graph.nodes
            if not nx.has_path(self.scenario.automaton.graph, self.scenario.automaton.ROOT, node)
        ]
        # self.scenario.automaton.graph.remove_nodes_from(list(nx.isolates(self.scenario.automaton.graph)))
        self.scenario.automaton.graph.remove_nodes_from(non_reachable)
        print("cutting graph(nodes) done")

        for device in self.topology.store.get_all_with(lambda _: True):
            for service_handle in device.services.as_iterator(
                case_all=self.scenario.configuration.service_store.get_handles_with(lambda _: True)
            ):
                if (service := self.scenario.configuration.service_store.get(service_handle)).ports() is not None:
                    for port in service.ports():
                        device.open_ports.add(port)

        filtered_paths = []
        for path in self.scenario.paths:
            if Chi(path).all(lambda node: node in self.scenario.automaton.graph.nodes):
                filtered_paths.append(path)

        self.scenario._paths = filtered_paths
        self.scenario.automaton.paths = filtered_paths
