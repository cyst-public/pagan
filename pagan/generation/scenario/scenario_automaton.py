import networkx as nx

from dataclasses import dataclass, field
from typing import Dict, FrozenSet, Generator, List, Optional, Set, Tuple, Union
from enum import Enum, auto
from chainingiterator import Chi
from serde import serialize

from pagan.shared.exceptions import AttackGraphException, ProgramLogicError
from pagan.shared.primitives.capabilities import AttackerCapability, PrivilegeLevel
from pagan.shared.primitives.credentials import Credential
from pagan.shared.primitives.primitives_sets import (
    ServiceSetOption,
    ServiceVulnerabilitySetOption,
)
from pagan.shared.primitives.set_option import SetOption
from pagan.shared.primitives.tokens import Token
from pagan.shared.primitives.types import (
    ActionHandle,
    DeviceHandle,
    EdgeKey,
    NodeHandle,
    AccountHandle,
)


@dataclass(frozen=True)
class NodeData:
    """
    Class representing the state of the attack.
    The class is frozen, and so are the included sets, this should allow
    implicit `hash()` and `eq()` functionality.
    """

    device: DeviceHandle
    attacker_capabilities: FrozenSet[AttackerCapability]
    attacker_tokens: FrozenSet[Token]
    available_credentials: FrozenSet[Credential]
    accepting_state: bool = field(default=False)  # goal node


@serialize(serializer=SetOption.serialize_setOption_includers)
@dataclass(frozen=True)
class EdgeData:
    action: ActionHandle
    scheme_idx: int
    service_enabler_pairs: Union[ServiceVulnerabilitySetOption, ServiceSetOption]
    runas: Union[AccountHandle, PrivilegeLevel]
    creds: Set[Credential]


class GraphCycleInfo(Enum):
    NONE = auto()
    PREEXISTING = auto()
    CREATED = auto()


@dataclass
class NodeProcessingState:
    finished_for: Set[Tuple[DeviceHandle]] = field(init=False, default_factory=set)
    # a list of all devices that we have already explored from the particular state

    def finish_for(self, device: Tuple[DeviceHandle]) -> None:
        self.finished_for.add(device)

    def is_finished_for(self, device: Tuple[DeviceHandle]) -> bool:
        return device in self.finished_for


@dataclass
class ScenarioStateMachine:
    ROOT: NodeHandle = 0
    graph: nx.MultiDiGraph = field(init=False, default_factory=nx.MultiDiGraph)
    reverse_translator: Dict[NodeData, NodeHandle] = field(init=False, default_factory=dict)
    next_handle: int = field(init=False, default=0)
    active_handles: Dict[NodeHandle, NodeData] = field(init=False, default_factory=dict)
    node_processing_states: Dict[NodeHandle, NodeProcessingState] = field(init=False, default_factory=dict)
    _locked: bool = field(init=False, default=False)
    paths: List[List[NodeHandle]] = field(init=False, default_factory=list)
    chosen_goal_node: NodeHandle = field(init=False)

    def add_node(self, data: NodeData) -> NodeHandle:
        # print(f"Adding node with: {NodeData}")
        if self._locked:
            raise ProgramLogicError("Automaton is already frozen - cannot add new nodes.")
        if (handle := self.reverse_translator.get(data, None)) is not None:
            return handle
        handle = self.next_handle
        self.reverse_translator[data] = handle
        self.graph.add_node(handle, state=data)
        self.next_handle += 1
        self.active_handles[handle] = data
        self.node_processing_states[handle] = NodeProcessingState()
        return handle

    def add_edge(self, from_handle: NodeHandle, to_handle: NodeHandle, transition_guard: EdgeData) -> EdgeKey:
        if self._locked:
            raise ProgramLogicError("Automaton is already frozen - cannot add new edges.")
        if from_handle not in self.active_handles.keys():
            raise AttackGraphException(
                f"Edge creation cannot be done with non-existing node <from_handle:{from_handle}>."
            )
        if to_handle not in self.active_handles.keys():
            raise AttackGraphException(f"Edge creation cannot be done with non-existing node <to_handle:{to_handle}>.")

        key = self.graph.add_edge(from_handle, to_handle, transition_guard=transition_guard)
        # TODO: cycle check - we could halt the building early in this case
        #  use nx.find_cycle(source=from_handle, orientation='original')
        # careful with multigraphs -> keys have to be used
        return key

    def query_node_processing_state(self, handle: NodeHandle) -> NodeProcessingState:
        return self.node_processing_states[handle]

    def finish_processing(self, handle: NodeHandle, devices: List[DeviceHandle]) -> None:
        self.node_processing_states[handle].finish_for(tuple(devices))

    def nodes(self) -> Generator[Tuple[NodeHandle, NodeData], None, None]:
        yield from Chi(self.reverse_translator.items())

    def edges(self, nodes: Optional[List[NodeHandle]] = None, data: bool = False) -> "nx.MultiEdgeView":
        return self.graph.edges(nbunch=nodes, data=data, keys=True)

    def query_node_data(self, handle: NodeHandle) -> NodeData:
        if (data := self.active_handles.get(handle, None)) is not None:
            return data
        raise AttackGraphException("Queried node handle does not exist.")

    def freeze(self) -> None:
        self._locked = True


def edge_match(first: EdgeData, second: EdgeData) -> bool:
    return True


def node_match(first: NodeData, second: NodeData) -> bool:
    return True


def node_match_wrap(first: Dict, second: Dict) -> bool:
    return node_match(first["state"], second["state"])


def edge_match_wrap(first: Dict, second: Dict) -> bool:
    for key_1 in first.keys():
        for key_2 in second.keys():
            if edge_match(first[key_1]["transition_guard"], second[key_2]["transition_guard"]):
                return True
    return False


def stop_condition(node: Dict) -> bool:
    return node["state"].accepting_state
