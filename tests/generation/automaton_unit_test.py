from typing import Set
import unittest
from pagan.generation.scenario.scenario_automaton import (
    ScenarioStateMachine,
    NodeData,
    EdgeData,
    NodeHandle,
    NodeProcessingState,
)
from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.capabilities import AttackerCapability, CapabilityType, PrivilegeLevel
from pagan.shared.primitives.credentials import Credential, CredentialType

import random

from pagan.shared.primitives.primitives_sets import ServiceSetOption
from pagan.shared.primitives.tokens import Token, TokenType

atk_acc = 0
user1_acc = 1
user1_app_acc = 2
user2_acc = 3

services = list(range(0, 15))
atk_device = 0
device1 = 1
device2 = 2


class ScenarioStateMachineTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.state_machine: ScenarioStateMachine = ScenarioStateMachine()
        cls.credential1 = Credential(
            account=user1_app_acc,
            device=device1,
            type=CredentialType.APP_PRIMARY,
            services=ServiceSetOption.SOME(set(random.sample(services, 3))),
            online_guess_complexity=60,
            unseal_complexity=1,
        )
        cls.credential2 = Credential(
            account=user1_acc,
            device=device1,
            type=CredentialType.OS,
            services=ServiceSetOption.SOME(set(random.sample(services, 6))),
            online_guess_complexity=60,
            unseal_complexity=30,
        )
        cls.credential3 = Credential(
            account=user1_acc,
            device=device2,
            type=CredentialType.OS,
            services=ServiceSetOption.SOME(set(random.sample(services, 6))),
            online_guess_complexity=60,
            unseal_complexity=30,
        )
        cls.credential4 = Credential(
            account=user2_acc,
            device=device2,
            type=CredentialType.OS,
            services=ServiceSetOption.SOME(set(random.sample(services, 6))),
            online_guess_complexity=60,
            unseal_complexity=30,
        )

        cls.starting_tokens: Set[Token] = {
            Token(
                type=TokenType.ACCESS_VIA_NETWORK,
                device=device1,
                services=ServiceSetOption.SOME(set(random.choice([cls.credential2.services.as_iterator_notall()]))),
            )
        }

    def test_build(self):
        self.root: NodeHandle = self.state_machine.add_node(
            data=(
                rootdata := NodeData(
                    device=atk_device,
                    attacker_capabilities=frozenset(),
                    attacker_tokens=frozenset(self.starting_tokens),
                    available_credentials=frozenset(),
                )
            )
        )

        self.assertEqual(self.root, 0, "Root is not handle 0")
        self.assertDictEqual(
            self.state_machine.active_handles,
            {0: rootdata},
        )

        self.first_state = self.state_machine.add_node(
            data=(
                ndata := NodeData(
                    device=device1,
                    attacker_capabilities=frozenset(
                        [
                            AttackerCapability(
                                to=CapabilityType.ACCESS_FILESYS_GENERIC,
                                privilege_limit=PrivilegeLevel.USER,
                                on_device=device1,
                                as_user=user1_acc,
                            ),
                            AttackerCapability(
                                to=CapabilityType.FILESYS_WRITE,
                                privilege_limit=PrivilegeLevel.USER,
                                on_device=device1,
                                as_user=user1_acc,
                            ),
                        ]
                    ),
                    attacker_tokens=frozenset(self.starting_tokens),
                    available_credentials=frozenset(),
                )
            )
        )
        self.state_machine.add_edge(
            self.root,
            self.first_state,
            transition_guard=EdgeData(
                action=0, scheme_idx=0, service_enabler_pairs=ServiceSetOption.NONE(), runas=atk_acc
            ),
        )
        n_handle = self.state_machine.add_node(data=ndata)
        self.assertEqual(self.first_state, n_handle)

        self.second_state = self.state_machine.add_node(
            data=(
                ndata := NodeData(
                    device=device1,
                    attacker_capabilities=frozenset(
                        [
                            AttackerCapability(
                                to=CapabilityType.ACCESS_FILESYS_GENERIC,
                                privilege_limit=PrivilegeLevel.USER,
                                on_device=device1,
                                as_user=user1_acc,
                            ),
                            AttackerCapability(
                                to=CapabilityType.FILESYS_WRITE,
                                privilege_limit=PrivilegeLevel.USER,
                                on_device=device1,
                                as_user=user1_acc,
                            ),
                            AttackerCapability(
                                to=CapabilityType.EXECUTE_CODE,
                                privilege_limit=PrivilegeLevel.USER,
                                on_device=device1,
                                as_user=user1_acc,
                            ),
                        ]
                    ),
                    attacker_tokens=frozenset(
                        updated := self.starting_tokens.union({Token(TokenType.SESSION, device=device1)})
                    ),
                    available_credentials=frozenset(),
                )
            )
        )
        self.starting_tokens = updated

        self.state_machine.add_edge(
            self.first_state,
            self.second_state,
            transition_guard=EdgeData(
                action=1, scheme_idx=0, service_enabler_pairs=ServiceSetOption.NONE(), runas=atk_acc
            ),
        )

        self.third_state = self.state_machine.add_node(
            data=(
                ndata := NodeData(
                    device=device1,
                    attacker_capabilities=frozenset(
                        [
                            AttackerCapability(
                                to=CapabilityType.ACCESS_FILESYS_GENERIC,
                                privilege_limit=PrivilegeLevel.USER,
                                on_device=device1,
                                as_user=user1_acc,
                            ),
                            AttackerCapability(
                                to=CapabilityType.FILESYS_WRITE,
                                privilege_limit=PrivilegeLevel.USER,
                                on_device=device1,
                                as_user=user1_acc,
                            ),
                            AttackerCapability(
                                to=CapabilityType.EXECUTE_CODE,
                                privilege_limit=PrivilegeLevel.USER,
                                on_device=device1,
                                as_user=user1_acc,
                            ),
                        ]
                    ),
                    attacker_tokens=frozenset(
                        updated := self.starting_tokens.union({Token(TokenType.ACCESS_VIA_NETWORK, device=device2)})
                    ),
                    available_credentials=frozenset(),
                )
            )
        )
        self.state_machine.add_edge(
            self.second_state,
            self.third_state,
            transition_guard=EdgeData(
                action=1, scheme_idx=0, service_enabler_pairs=ServiceSetOption.NONE(), runas=atk_acc
            ),
        )

        self.assertNotEqual(self.second_state, self.third_state)

    def test_freeze(self):
        self.state_machine.freeze()
        self.assertRaises(
            ProgramLogicError,
            self.state_machine.add_node,
            data=(
                NodeData(
                    device=atk_device,
                    attacker_capabilities=frozenset(),
                    attacker_tokens=frozenset(self.starting_tokens),
                    available_credentials=frozenset(),
                )
            ),
        )


if __name__ == "__main__":
    unittest.main()
