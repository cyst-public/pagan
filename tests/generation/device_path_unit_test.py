import unittest

from pagan.generation.scenario.path_tree import PathTree
from pagan.shared.stores.stores import DeviceStore
from pagan.shared.primitives.types import AccessType


class PathTreeTest(unittest.TestCase):
    def setUp(self) -> None:
        self.device_store = DeviceStore()
        self.attacker = 1
        self.pc1 = 2
        self.pc2 = 3
        self.pc3 = 4
        self.pc4 = 5
        self.accesses = {
            self.attacker: [(self.pc1, AccessType.NETWORK)],
            self.pc1: [
                (self.pc2, AccessType.NETWORK),
                (self.pc3, AccessType.NETWORK),
                (self.pc4, AccessType.NETWORK),
            ],
            self.pc2: [(self.pc4, AccessType.NETWORK)],
            self.pc3: [(self.pc4, AccessType.NETWORK)],
            self.pc4: [(self.pc3, AccessType.NETWORK)],
        }
        self.tree = PathTree(goal=self.pc4)

    def test_paths(self):
        dev_limit = 5  # attacker doesn't count
        self.tree.build(self.accesses, 5, self.attacker)

        paths = list(self.tree.paths)

        self.assertIn([self.attacker, self.pc1, self.pc4], paths)
        self.assertIn([self.attacker, self.pc1, self.pc2, self.pc4], paths)
        self.assertIn([self.attacker, self.pc1, self.pc3, self.pc4], paths)
        self.assertIn([self.attacker, self.pc1, self.pc4, self.pc3, self.pc4], paths)

        for path in paths:
            self.assertEqual(self.attacker, path[0])
            self.assertEqual(self.pc4, path[-1])
            self.assertTrue(len(path) <= dev_limit + 1)
            count = 0
            for dev in path:
                if dev == self.attacker:
                    count += 1
            self.assertEqual(count, 1)


if __name__ == "__main__":
    unittest.main()
