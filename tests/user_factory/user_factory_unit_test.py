import unittest

from pagan.kits.kit_templates.user_kits.template_factory import (
    TemplateBasedUserFactory,
    UserTemplateType,
)
from pagan.shared.primitives.capabilities import Capability, CapabilityType, PrivilegeLevel

"""
class UserFactoryTest(unittest.TestCase):
    def test_creation_simple(self):
        users = list(
            TemplateBasedUserFactory(
                "user",
                {
                    1: UserTemplateType.STANDARD_USER_WITH_SHUTDOWN,
                    2: UserTemplateType.STANDARD_USER_WITH_NFS,
                },
            ).build()
        )

        self.assertEqual(len(users), 1)

    def test_creation_admin(self):
        users = list(
            TemplateBasedUserFactory(
                "admin",
                {
                    1: UserTemplateType.ADMINISTRATOR,
                    2: UserTemplateType.STANDARD_USER_WITH_NFS,
                },
            ).build()
        )

        self.assertEqual(len(users), 2)
        self.assertIn(
            Capability(
                to=CapabilityType.UAC_ELEVATE, privilege_limit=PrivilegeLevel.USER, on_device=1
            ),
            users[1].capabilities[1],
        )
"""
