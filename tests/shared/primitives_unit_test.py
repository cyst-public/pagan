import unittest

from pagan.shared.primitives.capabilities import (
    CapabilityTemplate,
    AttackerCapability,
    CapabilityType,
    PrivilegeLevel,
    LimitType,
)
from pagan.shared.primitives.enablers import EnablerImpact
from pagan.shared.exceptions import ProgramLogicError
from pagan.shared.primitives.predicate_implementations import Any, All, NoneOf, NotJust, Not
from pagan.shared.primitives.predicate_trait import Predicate
from pagan.shared.primitives.primitives_sets import (
    ServiceAndVulnerability,
    ServiceSetOption,
    ServiceVulnerabilitySetOption,
)
from pagan.shared.primitives.set_option import SetOptionLen
from pagan.shared.primitives.primitives_sets import ServiceSetOption
from pagan.shared.primitives.tokens import TokenType, CombinedToken, Token
from pagan.shared.primitives.types import ServiceHandle


class TokenTypeTest(unittest.TestCase):
    def test_combine(self):
        token_list = [
            TokenType.CREDENTIAL_2FA_TOKEN,
            TokenType.SESSION,
        ]
        combined = TokenType.combine(*token_list)

        self.assertEqual(
            combined & TokenType.CREDENTIAL_2FA_TOKEN,
            TokenType.CREDENTIAL_2FA_TOKEN,
        )
        self.assertEqual(combined & TokenType.SESSION, TokenType.SESSION)

    def test_decompose(self):
        token = CombinedToken(TokenType.CREDENTIAL_2FA_TOKEN | TokenType.SESSION)
        token_list = TokenType.decompose(token)

        self.assertIn(TokenType.CREDENTIAL_2FA_TOKEN, token_list)
        self.assertIn(TokenType.SESSION, token_list)
        self.assertEqual(len(token_list), 2)

    def test_present_in(self):
        token = TokenType.combine(
            TokenType.SESSION, TokenType.CAPABILITY_ALL, TokenType.INFECTED_REMOVABLE_MEDIA
        )

        self.assertTrue(TokenType.SESSION.present_in(token))
        self.assertFalse(TokenType.CREDENTIAL.present_in(token))
        self.assertTrue(TokenType.CAPABILITY_ALL.present_in(token))
        self.assertTrue(TokenType.CAPABILITY.present_in(token))


class PredicatesTest(unittest.TestCase):
    def setUp(self):
        self.predAnyT = Any(TokenType.SESSION, TokenType.ACCESS_VIA_NETWORK)
        self.predAnyC = Any(
            CapabilityTemplate(
                to=CapabilityType.EXECUTE_CODE,
                privilege_limit=PrivilegeLevel.USER,
                limit_type=LimitType.MINIMUM,
            ),
            CapabilityTemplate(
                to=CapabilityType.ACCESS_FILESYS_ELEVATED,
                privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                limit_type=LimitType.EXACT,
            ),
        )

        self.acap_rce_admin = AttackerCapability(
            to=CapabilityType.EXECUTE_CODE,
            privilege_limit=PrivilegeLevel.ADMINISTRATOR,
            on_device=None,
            as_user=1,
        )
        self.acap_rce_service = AttackerCapability(
            to=CapabilityType.EXECUTE_CODE,
            privilege_limit=PrivilegeLevel.SERVICE,
            on_device=None,
            as_user=1,
        )
        self.acap_rce_user = AttackerCapability(
            to=CapabilityType.EXECUTE_CODE,
            privilege_limit=PrivilegeLevel.USER,
            on_device=None,
            as_user=1,
        )
        self.acap_file_admin = CapabilityTemplate(
            to=CapabilityType.ACCESS_FILESYS_ELEVATED,
            privilege_limit=PrivilegeLevel.ADMINISTRATOR,
            limit_type=LimitType.EXACT,
        )
        self.acap_file_user = CapabilityTemplate(
            to=CapabilityType.ACCESS_FILESYS_ELEVATED,
            privilege_limit=PrivilegeLevel.USER,
            limit_type=LimitType.EXACT,
        )
        self.acap_fileGeneric_admin = CapabilityTemplate(
            to=CapabilityType.ACCESS_FILESYS_GENERIC,
            privilege_limit=PrivilegeLevel.ADMINISTRATOR,
            limit_type=LimitType.EXACT,
        )

        self.predAllT = All(TokenType.SESSION, TokenType.ACCESS_VIA_NETWORK)

        self.predAllC = All(
            CapabilityTemplate(
                to=CapabilityType.EXECUTE_CODE,
                privilege_limit=PrivilegeLevel.USER,
                limit_type=LimitType.MINIMUM,
            ),
            CapabilityTemplate(
                to=CapabilityType.ACCESS_FILESYS_ELEVATED,
                privilege_limit=PrivilegeLevel.ADMINISTRATOR,
                limit_type=LimitType.EXACT,
            ),
        )

        self.predAnyCombo = Any(self.predAnyC, self.predAnyT)
        self.predAllCombo = All(self.predAllT, self.predAllC)

        self.predAllAnyCombo = All(self.predAnyC, self.predAnyT)

        self.predAnyAllCombo = Any(self.predAllT, self.predAllC)

        self.predEnablerAll = All(
            EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.DATA_DISCLOSURE
        )
        self.predNotOneSimple = Not(EnablerImpact.ALLOW)
        self.predNotOneNested = Not(self.predEnablerAll)
        self.predNoneOf = NoneOf(
            EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.AVAILABILITY_VIOLATION
        )
        self.predNotJust = NotJust(EnablerImpact.ALLOW)

    def test_create_error(self):
        self.assertRaises(ProgramLogicError, Any)
        self.assertRaises(ProgramLogicError, All)
        self.assertRaises(ProgramLogicError, Any)
        self.assertRaises(ProgramLogicError, Any, TokenType.SESSION, self.predAnyT)
        self.assertRaises(
            ProgramLogicError, Not, EnablerImpact.DATA_TAMPERING, EnablerImpact.DATA_DISCLOSURE
        )

    def test_any_allT_allC_combo(self):
        self.assertEqual(
            self.predAnyAllCombo.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE()),
                ],
                caps=[
                    self.acap_rce_admin,
                    self.acap_file_admin,
                    self.acap_fileGeneric_admin,
                    self.acap_rce_service,
                ],
            ),
            True,
        )
        self.assertEqual(
            self.predAnyAllCombo.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.NO_USB_ACCESS, None, ServiceSetOption.NONE()),
                ],
                caps=[
                    self.acap_rce_admin,
                    self.acap_file_admin,
                    self.acap_fileGeneric_admin,
                    self.acap_rce_service,
                ],
            ),
            True,
        )
        self.assertEqual(
            self.predAnyAllCombo.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE()),
                ],
                caps=[self.acap_rce_admin, self.acap_fileGeneric_admin, self.acap_rce_service],
            ),
            True,
        )
        self.assertEqual(
            self.predAnyAllCombo.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.NO_USB_ACCESS, None, ServiceSetOption.NONE()),
                ],
                caps=[self.acap_rce_admin, self.acap_fileGeneric_admin, self.acap_rce_service],
            ),
            False,
        )

    def test_all_anyT_anyC_combo(self):
        self.assertEqual(
            self.predAllAnyCombo.eval(
                tokens=[Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE())],
                caps=[self.acap_rce_admin, self.acap_fileGeneric_admin],
            ),
            True,
        )
        self.assertEqual(
            self.predAllAnyCombo.eval(
                tokens=[Token(TokenType.NO_USB_ACCESS, None, ServiceSetOption.NONE())],
                caps=[
                    self.acap_rce_admin,
                    self.acap_file_admin,
                    self.acap_fileGeneric_admin,
                    self.acap_rce_service,
                ],
            ),
            False,
        )
        self.assertEqual(
            self.predAllAnyCombo.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE()),
                ],
                caps=[self.acap_fileGeneric_admin, self.acap_rce_service],
            ),
            False,
        )

    def test_all_allT_allC_combo(self):
        self.assertEqual(
            self.predAllCombo.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE()),
                ],
                caps=[
                    self.acap_rce_admin,
                    self.acap_file_admin,
                    self.acap_fileGeneric_admin,
                    self.acap_rce_service,
                ],
            ),
            True,
        )
        self.assertEqual(
            self.predAllCombo.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.NO_USB_ACCESS, None, ServiceSetOption.NONE()),
                ],
                caps=[
                    self.acap_rce_admin,
                    self.acap_file_admin,
                    self.acap_fileGeneric_admin,
                    self.acap_rce_service,
                ],
            ),
            False,
        )
        self.assertEqual(
            self.predAllCombo.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE()),
                ],
                caps=[self.acap_rce_admin, self.acap_fileGeneric_admin, self.acap_rce_service],
            ),
            False,
        )

    def test_any_anyC_anyT_combo(self):
        self.assertEqual(isinstance(self.predAnyCombo, Predicate), True, "not pred")
        self.assertEqual(
            self.predAnyCombo.eval(
                tokens=[Token(TokenType.NO_USB_ACCESS, None, ServiceSetOption.NONE())],
                caps=[self.acap_rce_service],
            ),
            False,
        )
        self.assertEqual(
            self.predAnyCombo.eval(
                tokens=[Token(TokenType.SESSION, None, ServiceSetOption.NONE())],
                caps=[self.acap_rce_service],
            ),
            True,
        )
        self.assertEqual(
            self.predAnyCombo.eval(
                tokens=[Token(TokenType.NO_USB_ACCESS, None, ServiceSetOption.NONE())],
                caps=[self.acap_rce_admin],
            ),
            True,
        )

    def test_all_capabilities(self):
        self.assertEqual(self.predAllC.eval(tokens=[], caps=[self.acap_file_admin]), False)
        self.assertEqual(self.predAllC.eval(tokens=[], caps=[self.acap_rce_admin]), False)
        self.assertEqual(
            self.predAllC.eval(
                tokens=[],
                caps=[self.acap_rce_admin, self.acap_file_admin, self.acap_fileGeneric_admin],
            ),
            True,
        )

    def test_all_token(self):
        self.assertEqual(
            self.predAllT.eval(
                tokens=[Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE())], caps=[]
            ),
            False,
        )
        self.assertEqual(
            self.predAllT.eval(
                tokens=[Token(TokenType.SESSION, None, ServiceSetOption.NONE())], caps=[]
            ),
            False,
        )
        self.assertEqual(
            self.predAllT.eval(
                tokens=[Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE())], caps=[]
            ),
            False,
        )
        self.assertEqual(
            self.predAllT.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE()),
                ],
                caps=[],
            ),
            True,
        )
        self.assertEqual(
            self.predAllT.eval(
                tokens=[
                    Token(TokenType.SESSION, None, ServiceSetOption.NONE()),
                    Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE()),
                    Token(TokenType.ACCESS_DMZ_SEGMENT, None, ServiceSetOption.NONE()),
                ],
                caps=[],
            ),
            True,
        )

    def test_any_capabilities(self):
        self.assertEqual(isinstance(self.predAnyC, Predicate), True, "not pred")
        self.assertEqual(
            self.predAnyC.eval(tokens=[], caps=[self.acap_rce_admin, self.acap_rce_service]),
            True,
        )
        self.assertEqual(
            self.predAnyC.eval(tokens=[], caps=[self.acap_rce_user, self.acap_rce_service]),
            True,
        )
        self.assertEqual(self.predAnyC.eval(tokens=[], caps=[self.acap_rce_service]), False)
        self.assertEqual(
            self.predAnyC.eval(tokens=[], caps=[self.acap_rce_service, self.acap_file_admin]),
            True,
        )
        self.assertEqual(
            self.predAnyC.eval(tokens=[], caps=[self.acap_file_user, self.acap_rce_service]),
            False,
        )
        self.assertEqual(
            self.predAnyC.eval(
                tokens=[], caps=[self.acap_rce_service, self.acap_fileGeneric_admin]
            ),
            False,
        )

    def test_any_tokens(self):
        self.assertEqual(isinstance(self.predAnyT, Predicate), True, "not pred")
        self.assertEqual(
            self.predAnyT.eval(
                tokens=[Token(TokenType.ACCESS_VIA_NETWORK, None, ServiceSetOption.NONE())], caps=[]
            ),
            True,
        )
        self.assertEqual(
            self.predAnyT.eval(
                tokens=[Token(TokenType.SESSION, None, ServiceSetOption.NONE())], caps=[]
            ),
            True,
        )
        self.assertEqual(
            self.predAnyT.eval(
                tokens=[Token(TokenType.NO_USB_ACCESS, None, ServiceSetOption.NONE())], caps=[]
            ),
            False,
        )

    def test_all_enablers(self):
        self.assertEqual(
            self.predEnablerAll.eval(
                enablers=[EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.DATA_DISCLOSURE]
            ),
            True,
        )
        self.assertEqual(
            self.predEnablerAll.eval(
                enablers=[EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.ALLOW]
            ),
            False,
        )
        self.assertEqual(
            self.predEnablerAll.eval(
                enablers=[EnablerImpact.DATA_TAMPERING, EnablerImpact.DATA_DISCLOSURE]
            ),
            False,
        )

    def test_negatives(self):
        self.assertEqual(
            self.predNotOneSimple.eval(
                enablers=[EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.DATA_DISCLOSURE]
            ),
            True,
        )
        self.assertEqual(
            self.predNotOneSimple.eval(
                enablers=[EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.ALLOW]
            ),
            False,
        )

        self.assertEqual(
            self.predNotOneNested.eval(
                enablers=[EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.DATA_DISCLOSURE]
            ),
            False,
        )
        self.assertEqual(
            self.predNotOneNested.eval(
                enablers=[EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.ALLOW]
            ),
            True,
        )

        self.assertEqual(
            self.predNoneOf.eval(enablers=[EnablerImpact.AUTH_MANIPULATION, EnablerImpact.ALLOW]),
            True,
        )

        self.assertEqual(
            self.predNoneOf.eval(
                enablers=[EnablerImpact.ARBITRARY_CODE_EXECUTION, EnablerImpact.ALLOW]
            ),
            False,
        )

        self.assertEqual(
            self.predNoneOf.eval(
                enablers=[
                    EnablerImpact.ARBITRARY_CODE_EXECUTION,
                    EnablerImpact.AVAILABILITY_VIOLATION,
                ]
            ),
            False,
        )

    def test_not_just(self):

        self.assertEqual(
            self.predNotJust.eval(enablers=[EnablerImpact.ALLOW, EnablerImpact.DATA_TAMPERING]),
            True,
        )
        self.assertEqual(self.predNotJust.eval(enablers=[EnablerImpact.ALLOW]), False)


class ServiceSetOptionTest(unittest.TestCase):
    def setUp(self) -> None:
        self.none: ServiceSetOption = ServiceSetOption.NONE()
        self.all = ServiceSetOption.ALL()
        self.default_some = ServiceSetOption.SOME({1, 2, 3})

    def test_eq(self):
        self.assertEqual(ServiceSetOption.NONE(), ServiceSetOption.NONE())
        self.assertEqual(ServiceSetOption.ALL(), ServiceSetOption.ALL())
        self.assertNotEqual(ServiceSetOption.ALL(), ServiceSetOption.NONE())
        self.assertEqual(ServiceSetOption.SOME({1, 2, 3}), ServiceSetOption.SOME({1, 2, 3}))
        self.assertNotEqual(ServiceSetOption.SOME({1, 2, 3}), ServiceSetOption.NONE())
        self.assertNotEqual(ServiceSetOption.SOME({1, 2, 3}), ServiceSetOption.ALL())
        self.assertNotEqual(ServiceSetOption.SOME({1, 2, 3}), ServiceSetOption.SOME({1, 2, 3, 4}))

    def test_len(self):
        self.assertEqual(SetOptionLen.INTEGER(0), ServiceSetOption.NONE().size())
        self.assertEqual(SetOptionLen.INFINITY(), ServiceSetOption.ALL().size())
        self.assertEqual(SetOptionLen.INTEGER(3), ServiceSetOption.SOME({1, 2, 3}).size())

    def test_intersections(self):
        self.assertEqual(
            ServiceSetOption.NONE().intersection(ServiceSetOption.NONE()), ServiceSetOption.NONE()
        )
        self.assertEqual(
            ServiceSetOption.NONE().intersection(ServiceSetOption.ALL()), ServiceSetOption.NONE()
        )
        self.assertEqual(
            ServiceSetOption.ALL().intersection(ServiceSetOption.NONE()), ServiceSetOption.NONE()
        )

        self.assertEqual(
            ServiceSetOption.ALL().intersection(ServiceSetOption.ALL()), ServiceSetOption.ALL()
        )
        self.assertEqual(
            ServiceSetOption.ALL().intersection(ServiceSetOption.SOME({1, 2, 3})),
            ServiceSetOption.SOME({1, 2, 3}),
        )

        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).intersection(ServiceSetOption.NONE()),
            ServiceSetOption.NONE(),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).intersection(ServiceSetOption.ALL()),
            ServiceSetOption.SOME({1, 2, 3}),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).intersection(ServiceSetOption.SOME({1, 3})),
            ServiceSetOption.SOME({1, 3}),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).intersection(ServiceSetOption.SOME({4, 5, 6})),
            ServiceSetOption.NONE(),
        )

    def test_unions(self):
        self.assertEqual(
            ServiceSetOption.NONE().union(ServiceSetOption.NONE()), ServiceSetOption.NONE()
        )
        self.assertEqual(
            ServiceSetOption.NONE().union(ServiceSetOption.ALL()), ServiceSetOption.ALL()
        )
        self.assertEqual(
            ServiceSetOption.ALL().union(ServiceSetOption.NONE()), ServiceSetOption.ALL()
        )

        self.assertEqual(
            ServiceSetOption.ALL().union(ServiceSetOption.ALL()), ServiceSetOption.ALL()
        )
        self.assertEqual(
            ServiceSetOption.ALL().union(ServiceSetOption.SOME({1, 2, 3})),
            ServiceSetOption.ALL(),
        )

        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).union(ServiceSetOption.NONE()),
            ServiceSetOption.SOME({1, 2, 3}),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).union(ServiceSetOption.ALL()),
            ServiceSetOption.ALL(),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).union(ServiceSetOption.SOME({1, 3})),
            ServiceSetOption.SOME({1, 2, 3}),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).union(ServiceSetOption.SOME({4, 5, 6})),
            ServiceSetOption.SOME({1, 2, 3, 4, 5, 6}),
        )

    def test_excepts(self):
        case_all = {1, 2, 3, 4, 5, 6, 7}
        self.assertEqual(
            ServiceSetOption.NONE().except_of(ServiceSetOption.NONE(), case_all),
            ServiceSetOption.NONE(),
        )
        self.assertEqual(
            ServiceSetOption.NONE().except_of(ServiceSetOption.ALL(), case_all),
            ServiceSetOption.NONE(),
        )
        self.assertEqual(
            ServiceSetOption.ALL().except_of(ServiceSetOption.NONE(), case_all),
            ServiceSetOption.ALL(),
        )

        self.assertEqual(
            ServiceSetOption.ALL().except_of(ServiceSetOption.ALL(), case_all),
            ServiceSetOption.NONE(),
        )

        self.assertEqual(
            ServiceSetOption.ALL().except_of(ServiceSetOption.SOME({1, 2, 3}), case_all),
            ServiceSetOption.SOME({4, 5, 6, 7}),
        )

        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).except_of(ServiceSetOption.NONE(), case_all),
            ServiceSetOption.SOME({1, 2, 3}),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).except_of(ServiceSetOption.ALL(), case_all),
            ServiceSetOption.NONE(),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).except_of(ServiceSetOption.SOME({1, 3}), case_all),
            ServiceSetOption.SOME({2}),
        )
        self.assertEqual(
            ServiceSetOption.SOME({1, 2, 3}).except_of(ServiceSetOption.SOME({4, 5, 6}), case_all),
            ServiceSetOption.SOME({1, 2, 3}),
        )


class SetOptionLenTest(unittest.TestCase):
    def test_eq(self):
        self.assertTrue(SetOptionLen.INFINITY() == SetOptionLen.INFINITY())
        self.assertFalse(SetOptionLen.INFINITY() == SetOptionLen.INTEGER(555555555))
        self.assertTrue(SetOptionLen.INTEGER(555555555) == SetOptionLen.INTEGER(555555555))
        self.assertFalse(SetOptionLen.INTEGER(555555555) == SetOptionLen.INTEGER(222222))

    def test_neq(self):
        self.assertFalse(SetOptionLen.INFINITY() != SetOptionLen.INFINITY())
        self.assertTrue(SetOptionLen.INFINITY() != SetOptionLen.INTEGER(555555555))
        self.assertFalse(SetOptionLen.INTEGER(555555555) != SetOptionLen.INTEGER(555555555))
        self.assertTrue(SetOptionLen.INTEGER(555555555) != SetOptionLen.INTEGER(222222))

    def test_ge(self):
        self.assertTrue(SetOptionLen.INFINITY() >= SetOptionLen.INTEGER(555))
        self.assertTrue(SetOptionLen.INTEGER(666) >= SetOptionLen.INTEGER(555))
        self.assertTrue(SetOptionLen.INTEGER(666) >= SetOptionLen.INTEGER(666))
        self.assertFalse(SetOptionLen.INTEGER(666) >= SetOptionLen.INFINITY())

    def test_le(self):
        self.assertFalse(SetOptionLen.INFINITY() <= SetOptionLen.INTEGER(555))
        self.assertFalse(SetOptionLen.INTEGER(666) <= SetOptionLen.INTEGER(555))
        self.assertTrue(SetOptionLen.INTEGER(666) <= SetOptionLen.INTEGER(666))
        self.assertTrue(SetOptionLen.INTEGER(666) <= SetOptionLen.INFINITY())

    def test_lt(self):
        self.assertFalse(SetOptionLen.INFINITY() < SetOptionLen.INTEGER(555))
        self.assertFalse(SetOptionLen.INTEGER(666) < SetOptionLen.INTEGER(555))
        self.assertFalse(SetOptionLen.INTEGER(666) < SetOptionLen.INTEGER(666))
        self.assertTrue(SetOptionLen.INTEGER(555) < SetOptionLen.INTEGER(666))
        self.assertTrue(SetOptionLen.INTEGER(666) <= SetOptionLen.INFINITY())

    def test_gt(self):
        self.assertTrue(SetOptionLen.INFINITY() > SetOptionLen.INTEGER(555))
        self.assertTrue(SetOptionLen.INTEGER(666) > SetOptionLen.INTEGER(555))
        self.assertFalse(SetOptionLen.INTEGER(666) > SetOptionLen.INTEGER(666))
        self.assertFalse(SetOptionLen.INTEGER(666) > SetOptionLen.INFINITY())


class ServiceSetOptionTypeTest(unittest.TestCase):
    def test_types(self):
        self.assertIs(ServiceHandle, ServiceSetOption.SOME({1, 2}).inner_type())
        self.assertIs(
            ServiceAndVulnerability, ServiceVulnerabilitySetOption.SOME({1, 2}).inner_type()
        )


if __name__ == "__main__":
    unittest.main()
