import unittest

"""
from pagan.shared.primitives.actions import UsageScheme, ActionDescription
from pagan.shared.stores import ActionStore
from pagan.shared.exceptions import (
    StoreException,
)
from pagan.shared.primitives.tokens import TokenType, CombinedToken


class ActionStoreTest(unittest.TestCase):
    usage1 = UsageScheme(
        CombinedToken(TokenType.SESSION | TokenType.PRIV_ADMIN_DOMAIN),
        CombinedToken(TokenType.CODE_EXECUTION | TokenType.DATA_READ | TokenType.DATA_READ),
        -1,
    )
    usage2 = UsageScheme(
        CombinedToken(TokenType.SESSION | TokenType.PRIV_USER_LOCAL),
        CombinedToken(TokenType.DATA_READ),
        -1,
    )
    usage3 = UsageScheme(
        CombinedToken(
            TokenType.SESSION | TokenType.CREDENTIAL_ADMIN_LOCAL | TokenType.CREDENTIAL_2FA_TOKEN
        ),
        CombinedToken(TokenType.PRIV_ADMIN_LOCAL),
        -1,
    )

    action1 = ActionDescription(name="action1", uses=[usage2])
    action2 = ActionDescription(name="action2", uses=[usage1, usage2])
    action3 = ActionDescription(name="action3", uses=[usage3])

    def test_query_empty(self):
        store = ActionStore()
        self.assertEqual(store.oids, set())
        self.assertRaises(StoreException, store.get, oid=0)

    def test_add(self):
        store = ActionStore()
        store.add(self.action1)
        store.add(self.action2)
        store.add(self.action3)

        self.assertEqual(self.action1, store.get(0))
        self.assertEqual(self.action2, store.get(1))
        self.assertEqual(self.action3, store.get(2))

        self.assertRaises(StoreException, store.get, oid=3)

    def test_query_usable(self):
        store = ActionStore()
        store.add(self.action1)
        store.add(self.action2)
        store.add(self.action3)

        self.assertIn(
            (0, self.usage2),
            store.get_applicable([TokenType.SESSION, TokenType.PRIV_USER_LOCAL]),
        )
        self.assertIn(
            (1, self.usage2),
            store.get_applicable([TokenType.SESSION, TokenType.PRIV_USER_LOCAL]),
        )
        self.assertEqual(
            len(store.get_applicable([TokenType.SESSION, TokenType.PRIV_USER_LOCAL])),
            2,
        )
        self.assertIn(
            (1, self.usage1),
            store.get_applicable([TokenType.SESSION, TokenType.PRIV_ADMIN_DOMAIN]),
        )
        self.assertEqual(
            len(store.get_applicable([TokenType.SESSION, TokenType.PRIV_ADMIN_DOMAIN])),
            1,
        )
        self.assertIn(
            (2, self.usage3),
            store.get_applicable(
                [
                    TokenType.SESSION,
                    TokenType.CREDENTIAL_ADMIN_LOCAL,
                    TokenType.CREDENTIAL_2FA_TOKEN,
                ]
            ),
        )
        self.assertEqual(
            1,
            len(
                store.get_applicable(
                    [
                        TokenType.SESSION,
                        TokenType.CREDENTIAL_ADMIN_LOCAL,
                        TokenType.CREDENTIAL_2FA_TOKEN,
                    ]
                )
            ),
        )
        self.assertEqual(len(store.get_applicable([TokenType.SESSION])), 0)


if __name__ == "__main__":
    unittest.main()
"""
